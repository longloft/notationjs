﻿ko.bindingHandlers.uploadFile = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var handleFileSelect = function (evt) {
            var files = evt.target.files; // FileList object

            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {

                // Only process image files.
                //if (!f.type.match('image.*')) {
                //    continue;
                //}

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function (theFile) {
                    return function (e) {
                        valueAccessor()(e.target.result);
                    };
                })(f);

                // Read in the image file as a data URL.
                reader.readAsText(f, "UTF-8");
                //reader.readAsDataURL(f);
            }
        };

        element.addEventListener('change', handleFileSelect, false);
    }
};