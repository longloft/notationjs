﻿/// <reference path="../Notes/NotesEngine.js" />
/// <reference path="../Notes/NotesEngine.js" />
/// <reference path="NotationEngine.js" />
/// <reference path="NotationParser.js" />
/// <reference path="../Notes/NotesEngine.js" />
/// <reference path="../Notation/NotationEngine.js" />
/// 
function onloaded() {
    //setTimeout(function () {
    //loadIntensifiedAgility();
    loadAccidentals();
    //loadMultiPieceFile();
    //loadChamberFile();
    //loadJordansAngels();
    //}, 1000);
}

var ne = null;
var noteEngine = null;

var notes = [];

function nextPage() {

    notes[ne.getPageNumber()] = noteEngine.getImage();

    var currentPageNumber = ne.getPageNumber();
    var totalNumberPages = ne.getTotalNumberPages();
    if ((currentPageNumber + 1) <= totalNumberPages) {
        currentPageNumber += 1;
        ne.goToPage(currentPageNumber);
    }

    if (notes[currentPageNumber])
        noteEngine.drawImage(notes[currentPageNumber]);
}

function previousPage() {
    notes[ne.getPageNumber()] = noteEngine.getImage();

    var currentPageNumber = ne.getPageNumber();
    var totalNumberPages = ne.getTotalNumberPages();
    if ((currentPageNumber - 1) > 0) {
        currentPageNumber -= 1;
        ne.goToPage(currentPageNumber);
    }

    if (notes[currentPageNumber])
        noteEngine.drawImage(notes[currentPageNumber]);
}

function loadFile(fileUrl) {
    var canvas = document.getElementById("notationengine");
    var notesenginecanvas = document.getElementById("annotationsengine");

    var xhReq = new XMLHttpRequest();
    xhReq.open("GET", fileUrl + "?v=" + Math.floor(Math.random() * 99999999), false);
    xhReq.send(null);
    var serverResponse = xhReq.responseText;

    var parser = new NotationParser();
    var doc = parser.parseXmlString(serverResponse);

    if (!ne) {
        ne = new NotationEngine(canvas);
        ne.onLoad = function() {
            noteEngine.adjustCavasWidthHeight({ pageWidth: canvas.width, pageHeight: canvas.height });
            notesenginecanvas.style.width = canvas.style.width;
            notesenginecanvas.style.height = canvas.style.height;
        }
    }


    if (!noteEngine)
        noteEngine = new AnnotationsEngine(notesenginecanvas);
    noteEngine.mode = "text";

    // ne.goToPage(1);
    ne.loadDocument(doc, 1);
}

function toggleDraw() {
    if (noteEngine.enableDrawing)
        disableDraw();
    else
        enableDraw();
}

function enableDraw() {
    noteEngine.enableDrawing = true;
    var toggleButton = document.getElementById("toggleDrawButton");
    toggleDrawButton.innerText = "Disable Drawing";
}

function disableDraw() {
    noteEngine.enableDrawing = false;
    var toggleButton = document.getElementById("toggleDrawButton");
    toggleDrawButton.innerText = "Enable Drawing";
}

function setTextMode() {
    noteEngine.mode = "text";
    enableDraw();
}

function seteraser() {
    noteEngine.mode = "eraser";
    //noteEngine.color = "rgba(255, 255, 255, 0)";
    enableDraw();
}

function setred() {
    noteEngine.mode = "pen";
    noteEngine.color = "red";
    enableDraw();
}

function setblack() {
    noteEngine.mode = "pen";
    noteEngine.color = "black";
    enableDraw();
}

function setStamp() {
    noteEngine.mode = "stamp";
    enableDraw();
}

function changeLine() {
    noteEngine.lineWidth = document.getElementById("lineWidthInput").value;
}

function changeOpacity() {
    noteEngine.opacity = parseFloat(document.getElementById("opacityInput").value);
}

function loadChamberFile() {
    loadFile("/Content/MyChamberPiece.xml");
}

function loadTestFile() {
    loadFile("/Content/TestMusic.xml");
}

function loadMultiPieceFile() {
    loadFile("/Content/MultiPartPieces.xml");
}

function loadLargeFile() {
    loadFile("/Content/LargeFile.xml");
}

function loadIntensifiedAgility() {
    loadFile("/Content/Intensified_Agility.xml");
}

function loadJordansAngels() {
    loadFile("/Content/JoransAngels.xml");
}

function loadAccidentals() {
    loadFile("/Content/Accidentals.xml");
}



function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        //if (!f.type.match('image.*')) {
        //    continue;
        //}

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
                var parser = new NotationParser();
                var doc = null;
                try {
                    doc = parser.parseXmlString(e.target.result);
                } catch (e) {
                    alert("Unable to parse file data as music xml.");
                }

                try {
                    ne.loadDocument(doc, 1);
                } catch (e) {
                    alert("Unable to load file data as music xml.");
                }

                // Render thumbnail.
                //var span = document.createElement('span');
                //span.innerHTML = ['<img class="thumb" src="', e.target.result,
                //                  '" title="', escape(theFile.name), '"/>'].join('');
                //document.getElementById('list').insertBefore(span, null);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsText(f, "UTF-8");
        //reader.readAsDataURL(f);
    }
}

if (document.getElementById('files'))
    document.getElementById('files').addEventListener('change', handleFileSelect, false);