﻿/// <reference path="NotationEngine.js" />
/// <reference path="NotationParser.js" />

function HomePageModel(canvas) {
    var self = this;

    var ne = new NotationEngine(document.getElementById('engineContainer'));

    ne.mode = NotationEngineMode.autoFillToCavasWidthHeight;

    self.pages = ko.observableArray([
        { title: "Ecosaise", url: "/content/MusicXml/Ecosaise.xml", musUrl: "/content/Finale/p4.musx" },
        { title: "Theme", url: "/content/MusicXml/Theme.xml", musUrl: "/content/Finale/p4.musx" },
        // { title: "01 Overture - Acc", url: "/content/MusicXml/01 Overture - Acc.xml", musUrl: "/content/Finale/p4.musx" },
        { title: "Christmas Canon", url: "/content/MusicXml/Christmas Canon.xml", musUrl: "/content/Finale/p4.musx" },
        { title: "P4", url: "/content/MusicXml/p4.xml", musUrl: "/content/Finale/p4.musx" },
        { title: "Barlines", url: "/content/MusicXml/Barlines.xml", musUrl: "/content/Finale/p4.musx" },
        { title: "Articulations", url: "/content/MusicXml/Articulations.xml", musUrl: "/content/Finale/Accidentals.musx" },
        { title: "Accidentals", url: "/content/MusicXml/Accidentals.xml", musUrl: "/content/Finale/Accidentals.musx" },
        { title: "Orchestra Piece", url: "/content/MusicXml/OrchestaPiece.xml" },
        { title: "Choral Piece", url: "/content/MusicXml/ChoralPiece.xml" },
        { title: "Jordan’s Angels", url: "/content/MusicXml/JoransAngels.xml" },
        { title: "String Piece", url: "/content/MusicXml/StringComposition.xml" },
        { title: "Chamber Piece", url: "/content/MusicXml/MyChamberPiece.xml" },
        { title: "Large File", url: "/content/MusicXml/LargeFile.xml" },
        { title: "Greensleeves", url: "/content/MusicXml/GreenSleeves_Harp.xml" },
        { title: "Jingle Bells", url: "/content/MusicXml/JingleBells.xml" },
        { title: "Providence", url: "/content/MusicXml/1318406-1.xml", musUrl: "/content/Finale/1318406-1.MUS" },
        { title: "Hannukah Song", url: "/content/MusicXml/Hannukah Song.xml", musUrl: "/content/Finale/1318406-1.MUS" },
        { title: "MatonamiaCaraF", url: "/content/MusicXml/MatonamiaCaraF.xml", musUrl: "/content/Finale/1318406-1.MUS" },
        { title: "HousetopHoedown", url: "/content/MusicXml/HousetopHoedown.xml", musUrl: "/content/Finale/1318406-1.MUS" },
        { title: "Who is sylvia Amajwords09", url: "/content/MusicXml/WhoissylviaAmajwords09.xml", musUrl: "/content/Finale/1318406-1.MUS" },
        { title: "We Three Kings", url: "/content/MusicXml/WeThreeKings.xml", musUrl: "/content/Finale/1318406-1.MUS" }
    ]);
    self.selectedPage = ko.observable(self.pages()[1]);

    self.selectedPage.subscribe(function(newPage) {
        if (newPage)
            loadFile(newPage.url);
    });

    self.isLoading = ko.observable(false);

    var loadFile = function(fileUrl) {
        self.isLoading(true);

        $.ajax({
            type: "GET",
            url: fileUrl + "?v=" + Math.floor(Math.random() * 99999999),
            dataType: "text",
            success: function(xml) {
                self.isLoading(false);
                ne.loadDocument(xml, 1);
            }
        });

    };

    self.nextPage = function() {
        var currentPageNumber = ne.getPageNumber();
        var totalNumberPages = ne.getTotalNumberPages();
        if ((currentPageNumber + 1) <= totalNumberPages) {
            currentPageNumber += 1;
            ne.goToPage(currentPageNumber);
        }
    };

    self.previousPage = function() {
        var currentPageNumber = ne.getPageNumber();
        var totalNumberPages = ne.getTotalNumberPages();
        if ((currentPageNumber - 1) > 0) {
            currentPageNumber -= 1;
            ne.goToPage(currentPageNumber);
        }
    };

    self.downloadMus = function() {
        var page = self.selectedPage();
        if (page && page.musUrl) {
            window.location.href = page.musUrl;
        }
    };

    self.uploadFile = function(xmlString) {

        try {
            ne.loadDocument(xmlString, 1);
        } catch (e) {
            alert("Unable to load file data as music xml.");
        }
    };

    loadFile(self.selectedPage().url);
}