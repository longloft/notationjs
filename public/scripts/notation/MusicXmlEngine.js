﻿/// <reference path="CanvasUtils.js" />
/// <reference path="MaestroFontCharCodes.js" />
/// <reference path="FontChecker.js" />


function MusicXmlEngine(canvas, options) {

    var self = this;
    var document = null;
    var canvas = canvas;
    var ctx = canvas.getContext('2d');

    var canvasUtils = new CanvasUtils();
    var fontChecker = new FontChecker();

    var currentPage = 1;
    var totalNumberPages = 0;
    var tenthsPixal = 0;

    self.fontCharCodes = MaestroFontCharCodes;

    //page layout data
    var pageHeight = 0;
    var pageWidth = 0;
    var pageLeftMargin = 0;
    var pageRightMargin = 0;
    var pageTopMargin = 0;
    var pageBottomMargin = 0;
    var pageTopSystemDistance = 0;

    fontChecker.onLoad = function () {
        if (document)
            self.loadDocument(document);
    }

    self.autoAdjustCanvasWidthHeight = true;

    self.getPageLayoutData = function ()
    {
        return {
            pageHeight: pageHeight,
            pageWidth: pageWidth,
            pageLeftMargin: pageLeftMargin,
            pageRightMargin: pageRightMargin,
            pageTopMargin: pageTopMargin,
            pageBottomMargin: pageBottomMargin,
            pageTopSystemDistance: pageTopSystemDistance
        };
    }

    self.getTotalNumberPages = function () {
        return totalNumberPages;
    }

    self.getPageNumber = function () {
        return currentPage;
    }

    self.goToPage = function (newPageNumber) {
        currentPage = newPageNumber;

        if (document)
            self.loadDocument(document);
    }

    var convertTenthsToPixals = function (tenths) {
        return parseInt(tenths * tenthsPixal);
    }

    self.loadDocument = function (doc) {
        document = doc;

        //we need to make sure that the font is loaded
        if (!fontChecker.hasEmbeddedMaestroFont())
        {
            fontChecker.embedMaestroFont();
            return;
        }
        
        if (canvas.getContext) {

            //clear whatever was previously drawn
            self.clearCanvas();

            var credits = doc["score-partwise"][1].credit;
            var part = doc["score-partwise"][1].part;
            var measures = part.measure;
            var attributes = part.measure[0].attributes;
            var topDistance = 0;
            var leftMargin = 70;

            var noteFont = "normal 20.5pt MaestroNotes";
            
            //set note font for sizing
            ctx.font = noteFont;

            var noteHeadWidth = ctx.measureText(self.fontCharCodes.noteHead).width
            var noteHeadHeight = canvasUtils.getNoteHeadHeight(self.fontCharCodes.noteHead, noteFont);
            noteHeadHeight = noteHeadWidth - 1;
            var measureOffset = 0;
            var staffHeight = (noteHeadHeight * 4);
            var numberStaves = (attributes && attributes.staves) ? parseInt(attributes.staves["#text"]) : 1;
            tenthsPixal = noteHeadHeight / 10;

            //get page width/heigh
            var defaults = doc["score-partwise"][1].defaults;
            
            //get layout data
            self.parsePageLayout(defaults);

            //adjust width height of canvas
            if (self.autoAdjustCanvasWidthHeight) {
                self.adjustCavasWidthHeight();
            }

            //set title and credits
            self.drawCredits(credits, pageHeight);

            //make sure note font is set
            ctx.font = noteFont;

            
            leftMargin = pageLeftMargin;

            var currrentPageNumber = 1;

            for (var i = 0, len = measures.length; i < len; i++) {
                var newLine = false;

                if (measures[i].print && measures[i].print["@attributes"] && measures[i].print["@attributes"]["new-page"]) {
                    currrentPageNumber += 1;

                    //reset top distance
                    topDistance = pageTopMargin + pageTopSystemDistance;
                    newLine = true;
                }

                if (currrentPageNumber == currentPage) {

                    var rightMargin = 0;
                    var width = convertTenthsToPixals(parseInt(measures[i]["@attributes"].width));
                    var systemDistance = 0
                    if (measures[i].print && measures[i].print["system-layout"]) {
                        leftMargin = (measures[i].print["system-layout"]["system-margins"] && measures[i].print["system-layout"]["system-margins"]["left-margin"]) ? convertTenthsToPixals(parseInt(measures[i].print["system-layout"]["system-margins"]["left-margin"]["#text"])) + pageLeftMargin : leftMargin;
                        rightMargin = (measures[i].print["system-layout"]["system-margins"] && measures[i].print["system-layout"]["system-margins"]["right-margin"]) ? convertTenthsToPixals(parseInt(measures[i].print["system-layout"]["system-margins"]["right-margin"]["#text"])) : 0;
                        topDistance = (measures[i].print["system-layout"]["top-system-distance"]) ? convertTenthsToPixals(parseInt(measures[i].print["system-layout"]["top-system-distance"]["#text"])) + pageTopMargin : topDistance;

                        systemDistance = (measures[i].print["system-layout"]["system-distance"]) ? convertTenthsToPixals(parseInt(measures[i].print["system-layout"]["system-distance"]["#text"])) : 0;
                    }

                    //next line
                    
                    if (systemDistance > 0) {
                        topDistance += systemDistance + staffHeight;
                        leftMargin = pageLeftMargin;
                        measureOffset = pageLeftMargin;
                        newLine = true;
                    }


                    var originalLeftMargin = leftMargin;

                    //draw staff for measure
                    var bottomMeasureY = self.drawStaffLines(width, leftMargin, topDistance, noteHeadHeight, numberStaves, staffHeight, systemDistance);

                    leftMargin += width;

                    //create clef
                    self.drawMeasureAttributes(measures[i].attributes, originalLeftMargin, topDistance, noteHeadHeight, newLine);
                    

                    //create notes
                    if (measures[i].note) {
                        var notes = measures[i].note;

                        var c5Y = (topDistance + (noteHeadHeight * 2));
                        var c4Y = ((topDistance + (noteHeadHeight * 5)) + (noteHeadHeight * 0.5));

                        if (typeof (notes.length) == "undefined")
                            notes = [notes];


                        var beamBeginPointsArray = [];
                        var previousBeamFlagNotePoint = null;

                        for (var t = 0; t < notes.length; t++) {
                            var note = notes[t];
                            var previousNote = notes[(t - 1)];
                            var defaultX = (note["@attributes"]) ? convertTenthsToPixals(parseInt(note["@attributes"]["default-x"])) : 0;
                            var previousNoteDefaultX = (previousNote && previousNote["@attributes"]) ? convertTenthsToPixals(parseInt(previousNote["@attributes"]["default-x"])) : 0;

                            var previousNoteShowsFlagBeam = ((defaultX == previousNoteDefaultX) && (defaultX != 0));

                            var offByHalf = false;

                            //get pitch char
                            var noteData = self.getPitchTypeChar(note, previousNoteShowsFlagBeam);

                            //pitch char
                            var char = noteData.char;

                            //do we need to print flag
                            var needsFlag = noteData.needsFlag;

                            if (noteData.needsCustomStem)
                                offByHalf = true;

                            var myY = 0;
                            var stepid = self.noteIds[noteData.pitchStep];
                            if (noteData.pitchOctave >= 5) {
                                myY = c5Y - ((stepid - 1) * (noteHeadHeight * 0.5)) - (staffHeight * (noteData.pitchOctave - 5));
                            }
                            else {
                                myY = c4Y - ((stepid - 1) * (noteHeadHeight * 0.5)) + (staffHeight * (4 - noteData.pitchOctave));
                            }

                            if (offByHalf) {
                                myY -= (noteHeadHeight * 0.5);
                            }

                            ctx.fillText(char, (defaultX + originalLeftMargin), myY);

                            if (!previousNoteShowsFlagBeam) {
                                previousBeamFlagNotePoint = { x: (defaultX + originalLeftMargin), y: myY }
                            }

                            var stemX = 0;
                            var defaultStemY = 0;
                            //create stems if needed
                            if (noteData.needsCustomStem) {
                                //get default y
                                stemX = (!noteData.isStemDown) ? ((defaultX + originalLeftMargin) + (noteHeadWidth - 0.5)) : ((defaultX + originalLeftMargin) + tenthsPixal);
                                stemY = 0;
                                if (previousNoteShowsFlagBeam) {
                                    stemY = (previousBeamFlagNotePoint) ? previousBeamFlagNotePoint.y : 0;
                                }
                                else {
                                    defaultStemY = (note.stem && note.stem["@attributes"]) ? convertTenthsToPixals(parseInt(note.stem["@attributes"]["default-y"], 10)) : 0;
                                    stemY = (topDistance - defaultStemY);
                                }

                                ctx.beginPath();
                                ctx.moveTo(stemX, myY);
                                ctx.lineTo(stemX, stemY);
                                ctx.stroke();
                            }

                            if (needsFlag && !previousNoteShowsFlagBeam) {
                                var flagChar = noteData.flagChar;
                                
                                if (stemX > 0) {
                                    var flagY = (noteData.isStemDown) ? (topDistance - defaultStemY - noteHeadHeight) : (topDistance - defaultStemY + noteHeadHeight);
                                    ctx.fillText(flagChar, stemX, flagY)
                                }
                            }

                            if (noteData.isBeamBeginArray.length > 0) {
                                for (var b = 0; b < noteData.isBeamBeginArray.length; b++) {
                                    if (noteData.isBeamBeginArray[b])
                                        beamBeginPointsArray[b] = { x: stemX, y: (topDistance - defaultStemY) };
                                }
                            }

                            for (var b = 0; b < noteData.isBeamEndArray.length; b++) {
                                if (noteData.isBeamEndArray[b]) {
                                    ctx.save();

                                    ctx.beginPath();
                                    ctx.fillStyle = 'black';

                                    if (noteData.isStemDown) {
                                        ctx.moveTo(beamBeginPointsArray[b].x, (beamBeginPointsArray[b].y - (6 * b)));
                                        ctx.lineTo(stemX, ((topDistance - defaultStemY) - (6 * b)));
                                        ctx.lineTo(stemX, (((topDistance - defaultStemY) - 4) - (6 * b)));
                                        ctx.lineTo(beamBeginPointsArray[b].x, ((beamBeginPointsArray[b].y - 4)) - (6 * b));
                                        ctx.closePath();
                                    }
                                    else {
                                        ctx.moveTo(beamBeginPointsArray[b].x, (beamBeginPointsArray[b].y + (6 * b)));
                                        ctx.lineTo(stemX, ((topDistance - defaultStemY) + (6 * b)));
                                        ctx.lineTo(stemX, (((topDistance - defaultStemY) + 4) + (6 * b)));
                                        ctx.lineTo(beamBeginPointsArray[b].x, ((beamBeginPointsArray[b].y + 4)) + (6 * b));
                                        ctx.closePath();
                                    }
                                    ctx.fill();
                                    ctx.restore();
                                    noteData.isBeamEndArray[b] = false;
                                    //beamBeginPoints[b] = null;
                                }
                            }

                            beamPreviousNoteX = (defaultX + originalLeftMargin);

                            if (noteData.isFlat) {
                                ctx.fillText(self.fontCharCodes.flat, ((defaultX + originalLeftMargin) - 10), (myY));
                            }
                            else if (noteData.isSharp) {
                                ctx.fillText(self.fontCharCodes.sharp, ((defaultX + originalLeftMargin) - 10), (myY));
                            }
                        }
                    }

                    //draw barline
                    self.drawBarLine(measures[i].barline, leftMargin, topDistance, noteHeadHeight);

                    measureOffset = leftMargin;
                }
            }

            //save total number bages
            totalNumberPages = currrentPageNumber;
        }
    };

    //draw measure attributes
    self.drawMeasureAttributes = function (attr, originalLeftMargin, topDistance, noteHeadHeight, newLine)
    {
        var attLeftMargin = originalLeftMargin;

        if (attr)
        {
            var clefs = attr.clef;

            if (typeof (clefs.length) == "undefined")
                clefs = [clefs];

            for (var i = 0; i < clefs.length; i++) {

                var clef = clefs[i];

                if (clef && clef.sign) {
                    var sign = clef.sign["#text"];
                    if (sign == "G") {
                        ctx.fillText(self.fontCharCodes.trebleClef, (attLeftMargin + 6), (topDistance + (noteHeadHeight * 3)));
                        attLeftMargin += ctx.measureText(self.fontCharCodes.trebleClef).width + 6;
                    }
                }
                else {
                    ctx.fillText(self.fontCharCodes.trebleClef, (attLeftMargin + 6), (topDistance + (noteHeadHeight * 3)));
                    attLeftMargin += ctx.measureText(self.fontCharCodes.trebleClef).width + 6;
                }

                if (attr.time) {
                    var beats = attr.time.beats["#text"];
                    var beatType = attr.time["beat-type"]["#text"];
                    var beatsHeight = canvasUtils.textHeight(beats, "normal 20.5pt MaestroNotes");
                    ctx.fillText(beats, (attLeftMargin + 6), (topDistance + (noteHeadHeight * 1)));
                    ctx.fillText(beatType, (attLeftMargin + 6), (topDistance + (noteHeadHeight * 3)));
                    attLeftMargin += ctx.measureText(beatType).width + 6;
                }
            }
           
        }
        else if (newLine)
        {
            ctx.fillText(self.fontCharCodes.trebleClef, (attLeftMargin + 6), (topDistance + (noteHeadHeight * 3)));
        }
    }

    //get the char for pitch type
    self.getPitchTypeChar = function (note, previousNoteShowsFlagBeam)
    {
        var pitchStep = (note.pitch) ? note.pitch.step["#text"] : "C";
        var pitchOctave = (note.pitch) ? parseInt(note.pitch.octave["#text"]) : 5;
        var pitchType = (note.type) ? note.type["#text"] : "";
        var isRest = (note.rest) ? true : false;
        var isStemDown = (note.stem && note.stem["#text"] == "down");
        var isFlat = (note.accidental && note.accidental["#text"] == "flat");
        var isSharp = (note.accidental && note.accidental["#text"] == "sharp");
        var needsCustomStem = ((note.stem && note.stem["@attributes"]) || previousNoteShowsFlagBeam);

        //get beam data
        var isBeamBeginArray = [];
        var isBeamEndArray = [];
        if (note.beam) {
            //see if only one beam node
            if (typeof (note.beam.length) == "undefined") {
                isBeamBeginArray[0] = (note.beam["#text"] == "begin") ? true : false;
                isBeamEndArray[0] = (note.beam["#text"] == "end") ? true : false;
            }
            else {
                for (var b = 0; b < note.beam.length; b++) {
                    isBeamBeginArray[b] = (note.beam[b]["#text"] == "begin") ? true : false;
                    isBeamEndArray[b] = (note.beam[b]["#text"] == "end") ? true : false;
                }
            }
        }

        //default char to notehead
        var char = self.fontCharCodes.noteHead;
        var needsFlag = false;
        var flagChar = null;

        switch (pitchType) {
            case "quarter":
                if (isRest)
                    char = self.fontCharCodes.quarterNoteRest;
                else {
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.quarterNoteStemDown : self.fontCharCodes.quarterNoteStemUp;
                }

                break;
            case "eighth":
                if (isRest)
                    char = self.fontCharCodes.eighthNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.eighthNoteStemDown : self.fontCharCodes.eighthNoteStemUp;

                if (!note.beam)
                {
                    flagChar = (isStemDown) ? self.fontCharCodes.eighthNoteFlagDown : self.fontCharCodes.eighthNoteFlagUp;
                    needsFlag = true;
                }
                    
                break;
            case "16th":
                if (isRest)
                    char = self.fontCharCodes.sixteenthNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.sixteenthNoteStemDown : self.fontCharCodes.sixteenthNoteStemUp;
                if (!note.beam)
                {
                    needsFlag = true;
                    flagChar = (isStemDown) ? self.fontCharCodes.sixteenthNoteFlagDown : self.fontCharCodes.sixteenthNoteFlagUp;
                }
                    
                break;
            case "half":
                if (isRest)
                    char = self.fontCharCodes.halfNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.halfNoteStemDown : self.fontCharCodes.halfNoteStemUp;
                break;
            case "whole":
                if (isRest)
                    char = self.fontCharCodes.wholeNoteRest;
                else
                    char = self.fontCharCodes.wholeNote;

                offByHalf = true;

                break;
            default:
                char = self.fontCharCodes.wholeNoteRest;
                break;
        }

        return {
            char: char,
            needsFlag: needsFlag,
            flagChar: flagChar,
            pitchStep : pitchStep,
            pitchOctave: pitchOctave,
            pitchType:pitchType,
            isRest:isRest,
            isStemDown:isStemDown,
            isFlat:isFlat,
            isSharp: isSharp,
            needsCustomStem: needsCustomStem,
            isBeamBeginArray: isBeamBeginArray,
            isBeamEndArray: isBeamEndArray
        };
    }

    //draw the credits and titles on screen
    self.drawCredits = function (credits, pageHeight)
    {
        if (credits) {
            var originalTextBaseline = ctx.textBaseline;
            var originalTextAlign = ctx.textAlign;

            for (var i = 0, len = credits.length; i < len; i++) {

                var credit = credits[i];

                if (credit["@attributes"].page == currentPage) {

                    var creditType = (credit["credit-type"]) ? credit["credit-type"]["#text"] : "";
                    var creditWord = credit["credit-words"]["#text"];
                    var creditX = convertTenthsToPixals(parseInt(credit["credit-words"]["@attributes"]["default-x"]));
                    var creditY = convertTenthsToPixals(parseInt(credit["credit-words"]["@attributes"]["default-y"]));
                    var creditFontSize = credit["credit-words"]["@attributes"]["font-size"];
                    var creditJustify = credit["credit-words"]["@attributes"]["justify"];
                    var creditVAlign = credit["credit-words"]["@attributes"]["valign"];

                    ctx.font = 'normal ' + creditFontSize + 'pt Times New Roman';

                    if (creditVAlign) {
                        ctx.textBaseline = creditVAlign;
                    }

                    var textWidth = ctx.measureText(creditWord).width;

                    var textX = creditX

                    switch (creditJustify) {
                        case "center":
                        case "right":
                            ctx.textAlign = creditJustify;
                            break;
                        default:
                            ctx.textAlign = originalTextAlign;
                            break;
                    }

                    ctx.fillText(creditWord, textX, (pageHeight - creditY));
                }
            }

            ctx.textAlign = originalTextAlign;
            ctx.textBaseline = originalTextBaseline;
        }
    }

    //draws the barline by type
    self.drawBarLine = function (barline, leftMargin, topDistance, noteHeadHeight) {
        if (barline) {
            //figure out line types
            ctx.beginPath();
            ctx.moveTo(leftMargin - 5, topDistance);
            ctx.lineTo(leftMargin - 5, topDistance + (noteHeadHeight * 4));
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(leftMargin, topDistance);
            ctx.lineTo(leftMargin, topDistance + (noteHeadHeight * 4));
            ctx.stroke();

            ctx.beginPath();
            ctx.rect(leftMargin - 2, topDistance + (noteHeadHeight * 4), 2, (topDistance - (topDistance + (noteHeadHeight * 4))));
            ctx.fillStyle = 'black';
            ctx.fill();
            ctx.stroke();
        }
        else {
            ctx.beginPath();
            ctx.moveTo(leftMargin, topDistance);
            ctx.lineTo(leftMargin, topDistance + (noteHeadHeight * 4));
            ctx.stroke();
        }
    };

    //get the page layout data
    self.parsePageLayout = function (defaults)
    {
        if (!defaults)
            return;

        pageWidth = (defaults["page-layout"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-width"]["#text"])) : 0;
        pageHeight = (defaults["page-layout"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-height"]["#text"])) : 0;
        pageLeftMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["left-margin"]["#text"])) : 0;
        pageRightMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["right-margin"]["#text"])) : 0;
        pageTopMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["top-margin"]["#text"])) : 0;
        pageBottomMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["bottom-margin"]["#text"])) : 0;
        pageTopSystemDistance = (defaults["system-layout"] && defaults["system-layout"]["top-system-distance"]) ? convertTenthsToPixals(parseInt(defaults["system-layout"]["top-system-distance"]["#text"])) : 0;
    }

    //remove all drawing from canvas
    self.clearCanvas = function ()
    {
        // Store the current transformation matrix
        ctx.save();

        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Restore the transform
        ctx.restore();
    }

    //resize the canvas to the musicxml page specs
    self.adjustCavasWidthHeight = function ()
    {
        var retina = window.devicePixelRatio > 1;
        var dim = 1;

        if (retina)
            dim = 2;

        if (pageWidth > 0) {
            canvas.width = pageWidth * dim;
            canvas.style.width = pageWidth + "px";
        }

        if (pageHeight > 0) {
            canvas.height = pageHeight * dim;
            canvas.style.height = pageHeight + "px";
        }

        if(dim > 1)
            ctx.scale(2, 2);
    }

    //draw staff lines for measure.
    self.drawStaffLines = function (measureWidth, leftMargin, topDistance, noteHeadHeight, numberStaves, staffHeight, systemDistance)
    {
        var topStaffDistance = topDistance;
        var firstStaff = true;
        for (var i = 0; i < numberStaves; i++) {
            if (!firstStaff)
            {
                topStaffDistance += (staffHeight + systemDistance);
            }
            
            firstStaff = true;

            for (var s = 0; s < 5; s++) {
                ctx.beginPath();
                ctx.moveTo(leftMargin, topStaffDistance + (noteHeadHeight * s));
                ctx.lineTo(leftMargin + measureWidth, topStaffDistance + (noteHeadHeight * s));
                ctx.stroke();
            }
        }
        
        return topStaffDistance + staffHeight;
    }

    //used for adjusting the y axis
    self.noteIds = { C: 1, D: 2, E: 3, F: 4, G: 5, A: 6, B: 7 };

}