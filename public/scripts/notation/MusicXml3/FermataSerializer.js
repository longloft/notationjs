﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.FermataSerializer = function (piece) {
    "user strict";

    var self = this;

    self.deserialize = function (fermata) {
        var f = new NotationJS.Fermata();

        if (fermata["@attributes"]["default-x"] != undefined)
            f.defaultX = piece.convertTenthsToPixals(parseInt(fermata["@attributes"]["default-x"], 10));
        if (fermata["@attributes"]["default-y"] != undefined)
            f.defaultY = piece.convertTenthsToPixals(parseInt(fermata["@attributes"]["default-y"], 10));
        if (fermata["@attributes"]["type"] != undefined)
            f.type = fermata["@attributes"]["type"];

        return f;
    };
};