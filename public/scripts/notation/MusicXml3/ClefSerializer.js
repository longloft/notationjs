﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.ClefSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (clef) {
        var c = new NotationJS.Clef();

        if (clef.sign != undefined && clef.sign["#text"] != undefined)
            c.sign = clef.sign["#text"];
        if (clef.line != undefined && clef.line["#text"] != undefined)
            c.line = clef.line["#text"];
        if (clef["clef-octave-change"] != undefined && clef["clef-octave-change"]["#text"] != undefined)
            c.octaveChange = parseInt(clef["clef-octave-change"]["#text"], 10);

        if (clef["@attributes"] != undefined && clef["@attributes"].number != undefined) {
            c.number = parseInt(clef["@attributes"].number, 10);
        }

        return c;
    };
};