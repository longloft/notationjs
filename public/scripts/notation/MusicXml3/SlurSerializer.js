﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.SlurSerializer = function (piece) {
    "user strict";

    var self = this;

    self.deserialize = function (slur, slurModel) {
        var e = slurModel || new NotationJS.Slur();
        e.piece = piece;

        if (slur["@attributes"] != undefined) {

            var attributes = slur["@attributes"];

            if (attributes.number != undefined)
                e.number = attributes.number;
            if (attributes.placement != undefined)
                e.placement = attributes.placement;
            if (attributes["default-y"] != undefined)
                e.defaultY = e.piece.convertTenthsToPixals(parseInt(attributes["default-y"], 10));
            if (attributes["default-x"] != undefined)
                e.defaultX = e.piece.convertTenthsToPixals(parseInt(attributes["default-x"], 10));
            if (attributes["bezier-x"] != undefined)
                e.bezierX = e.piece.convertTenthsToPixals(parseInt(attributes["bezier-x"], 10));
            if (attributes["bezier-y"] != undefined)
                e.bezierY = e.piece.convertTenthsToPixals(parseInt(attributes["bezier-y"], 10));
        }

        return e;
    };

    self.deserializeEndSlur = function (slur, slurModel) {
        
        if (slur["@attributes"] != undefined) {

            var attributes = slur["@attributes"];

            if (attributes["default-x"] != undefined)
                slurModel.endDefaultX = slurModel.piece.convertTenthsToPixals(parseInt(attributes["default-x"], 10));
            if (attributes["default-y"] != undefined)
                slurModel.endDefaultY = slurModel.piece.convertTenthsToPixals(parseInt(attributes["default-y"], 10));

            if (attributes["bezier-x"] != undefined)
                slurModel.endBezierX = slurModel.piece.convertTenthsToPixals(parseInt(attributes["bezier-x"], 10));
            if (attributes["bezier-y"] != undefined)
                slurModel.endBezierY = slurModel.piece.convertTenthsToPixals(parseInt(attributes["bezier-y"], 10));

        }

        return slurModel;
    };

    self.processSlurs = function (slurs) {
        for (var i = 0; i < slurs.length; i++) {
            var slur = slurs[i];

            var startNote = slur.startNote;
            var endNote = slur.endNote;
            var piece = startNote.piece;

            if (startNote != undefined && endNote != undefined && startNote.measure.y == endNote.measure.y) {

                var downward = (slur.placement == "below");

                if (startNote.chord != null && !downward) {
                    startNote = startNote.chord.getTopNote();
                }
                if (endNote.chord != null && !downward) {
                    endNote = endNote.chord.getTopNote();
                }

                var padding = piece.noteHeadHeight;
                var x0 = startNote.x + (piece.noteHeadWidth * 0.5);
                var y0 = startNote.y;
                var x1 = endNote.x + (piece.noteHeadWidth * 0.5);
                var y1 = endNote.y;
                var bow = piece.noteHeadHeight * 3;

                if (startNote.stemDirection == "up" && !downward) {
                    y0 = startNote.stemY;
                }

                if (endNote.stemDirection == "up" && !downward) {
                    y1 = endNote.stemY;
                }

                //if (startNote.y > startNote.measure.staffMiddleYs[(startNote.staff - 1)])
                //    downward = true;



                //if (startNote.measure.page != endNote.measure.page || (endNote.measure.newLine && !startNote.measure.newLine)) {
                //    x1 = startNote.measure.x + startNote.measure.width;
                //    y1 = y0;
                //}

                y0 = (downward) ? y0 + padding : y0 - padding;
                y1 = (downward) ? y1 + padding : y1 - padding;

                var sign = (downward ? 1 : -1);

                var thickness = piece.convertTenthsToPixals((sign * 2));

                var deltaX = x1 - x0;
                var deltaY = y1 - y0;

                if (deltaX == 0)
                    deltaX = 1;
                var ratio = 0.25;

                var maxBow = deltaX * 0.4;

                if (bow > maxBow && deltaY < 2 * 10) {

                    bow = maxBow;
                }

                bow *= sign;

                var slope = deltaY / deltaX;

                var cp1X = x0 + deltaX * ratio;
                var cp1Y = y0 + (cp1X - x0) * slope + bow;

                var cp2X = x1 - deltaX * ratio;
                var cp2Y = y0 + (cp2X - x0) * slope + bow;

                var startX0 = x0;
                var startY0 = y0;
                var startX1 = x1;
                var startY1 = y1;

                if (slur.defaultX != null)
                    x0 = startNote.x + slur.defaultX;
                if (slur.defaultY != null)
                    y0 = startNote.measure.staffTopYs[(startNote.staff - 1)] - slur.defaultY;
                if (slur.bezierX != null)
                    cp1X = startNote.x + slur.bezierX;
                if (slur.bezierY != null)
                    cp1Y = y0 - slur.bezierY;

                if (slur.endDefaultX != null)
                    x1 = endNote.x + slur.endDefaultX;
                if (slur.endDefaultY != null)
                    y1 = endNote.measure.staffTopYs[(endNote.staff - 1)] - slur.endDefaultY;

                if (slur.endBezierX != null)
                    cp2X = endNote.x + slur.endBezierX;
                if (slur.endBezierY != null)
                    cp2Y = y1 - +slur.endBezierY;

                var curve = new NotationJS.Curve();

                curve.x1 = x1;
                curve.y1 = y1;
                curve.cp1x1 = cp1X;
                curve.cp1y1 = cp1Y;
                curve.cp2x1 = cp2X;
                curve.cp2y1 = cp2Y;

                cp1Y -= thickness;
                cp2Y -= thickness;

                curve.x0 = x0;
                curve.y0 = y0;
                curve.cp1x0 = cp1X;
                curve.cp1y0 = cp1Y;
                curve.cp2x0 = cp2X;
                curve.cp2y0 = cp2Y;

                slur.curves.push(curve);

            }
        }
    };
};