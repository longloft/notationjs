﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.NoteSerializer = function (piece, part, measure) {
    "user strict";

    var self = this;

    self.deserialize = function (note) {
        var n = new NotationJS.Note();
        n.piece = piece;
        n.part = part;
        n.measure = measure;

        if (note["@attributes"] != undefined) {
            n.defaultX = n.piece.convertTenthsToPixals(parseInt(note["@attributes"]["default-x"], 10));
        }

        if (note.stem != undefined && note.stem["#text"] != undefined) {
            n.stemDirection = note.stem["#text"];
        }

        if (note.stem != undefined && note.stem["@attributes"] != undefined && note.stem["@attributes"]["default-y"] != undefined) {
            n.stemDefaultY = n.piece.convertTenthsToPixals(parseInt(note.stem["@attributes"]["default-y"], 10));
        }

        if (note.type != undefined && note.type["#text"] != undefined)
            n.type = note.type["#text"];

        if (note.duration != undefined && note.duration["#text"] != undefined)
            n.duration = parseInt(note.duration["#text"], 10);

        if (note.pitch != undefined) {
            n.pitch = new NotationJS.MusicXml3.PitchSerializer().deserialize(note.pitch);
        }

        if (note.accidental != undefined) {
            if (note.accidental["#text"] != undefined)
                n.accidental = note.accidental["#text"];

            n.accidentalInParentheses = (note.accidental["@attributes"] != undefined && note.accidental["@attributes"].parentheses == "yes");
        }

        if (note.staff != undefined && note.staff["#text"] != undefined) {
            n.staff = parseInt(note.staff["#text"], 10);
        }

        if (note.rest != undefined) {
            n.isRest = true;
            if (note.rest["display-step"] != undefined && note.rest["display-step"]["#text"] != undefined) {
                n.restStep = note.rest["display-step"]["#text"];
            }
            if (note.rest["display-octave"] != undefined && note.rest["display-octave"]["#text"] != undefined) {
                n.restOctave = parseInt(note.rest["display-octave"]["#text"], 10);
            }
        }

        if (note.notations != undefined && note.notations.articulations != undefined) {
            n.articulations = new NotationJS.MusicXml3.ArticulationsSerializer(n.piece).deserialize(note.notations.articulations);
        }

        if (note.notations != undefined && note.notations.fermata != undefined)
        {
            n.fermata = new NotationJS.MusicXml3.FermataSerializer(n.piece).deserialize(note.notations.fermata);
            n.fermata.note = n;
        }

        if (note.tie != undefined) {
            n.isTied = true;
            n.tiedType = (note.tie["@attributes"] != undefined) ? note.tie["@attributes"].type : null;
        }

        n.isMeasureRest = (note.rest != undefined && note.rest["@attributes"] != undefined && note.rest["@attributes"]["measure"] == "yes");
        n.partOfChord = (note.chord != undefined);
        n.isDot = (note.dot != undefined);
        n.beam = (note.beam != undefined);
        n.isGrace = (note.grace != undefined);

        return n;
    };

    self.parse = function (note) {

        //don't do anything if part of multimeasure rest
        if (note.isPartOfMultiMeasureRest)
            return;

        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        note.charCode = getNoteHeadCharCode(note);

        if (note.isMeasureRest) {
            note.x = ((note.measure.x) + (note.measure.width * 0.5)) - (ctx.measureText(note.charCode).width * 0.5) + (note.measure.paddingLeft * 0.5);
        }

        for (var i = 0; i < note.articulations.length; i++) {
            new NotationJS.MusicXml3.ArticulationsSerializer(piece).parse(note.articulations[i], note);
        }
    };

    var getNoteHeadCharCode = function (note) {
        var isStemDown = (note.stemDirection == "down");
        var needsCustomStem = (note.stemDefaultY != null || note.partOfChord);

        var char = note.piece.fontCharCodes.noteHead;

        switch (note.type) {
            case "quarter":
                if (note.isRest)
                    char = note.piece.fontCharCodes.quarterNoteRest;
                else {
                    if (!needsCustomStem)
                        char = (isStemDown) ? note.piece.fontCharCodes.quarterNoteStemDown : note.piece.fontCharCodes.quarterNoteStemUp;
                }

                break;
            case "eighth":
                if (note.isRest)
                    char = note.piece.fontCharCodes.eighthNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? note.piece.fontCharCodes.eighthNoteStemDown : note.piece.fontCharCodes.eighthNoteStemUp;

                if (!note.beam) {
                    flagChar = (isStemDown) ? note.piece.fontCharCodes.eighthNoteFlagDown : note.piece.fontCharCodes.eighthNoteFlagUp;
                    needsFlag = true;
                }

                break;
            case "16th":
                if (note.isRest)
                    char = note.piece.fontCharCodes.sixteenthNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? note.piece.fontCharCodes.sixteenthNoteStemDown : note.piece.fontCharCodes.sixteenthNoteStemUp;
                if (!note.beam) {
                    needsFlag = true;
                    flagChar = (isStemDown) ? note.piece.fontCharCodes.sixteenthNoteFlagDown : note.piece.fontCharCodes.sixteenthNoteFlagUp;
                }

                break;

            case "32nd":
                if (note.isRest)
                    char = note.piece.fontCharCodes.thirtySecondNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? note.piece.fontCharCodes.sixteenthNoteStemDown : note.piece.fontCharCodes.sixteenthNoteStemUp;
                if (!note.beam) {
                    needsFlag = true;
                    flagChar = (isStemDown) ? note.piece.fontCharCodes.sixteenthNoteFlagDown : note.piece.fontCharCodes.sixteenthNoteFlagUp;
                }

                break;
            case "half":
                if (note.isRest)
                    char = note.piece.fontCharCodes.halfNoteRest;

                else {
                    char = note.piece.fontCharCodes.noteHeadOpen;
                    if (!needsCustomStem)
                        char = (isStemDown) ? note.piece.fontCharCodes.halfNoteStemDown : note.piece.fontCharCodes.halfNoteStemUp;
                }

                break;
            case "whole":
                if (note.isRest)
                    char = note.piece.fontCharCodes.wholeNoteRest;
                else
                    char = note.piece.fontCharCodes.wholeNote;

                offByHalf = true;

                break;
            default:
                char = note.piece.fontCharCodes.wholeNoteRest;
                break;
        }

        return char;
    };
};