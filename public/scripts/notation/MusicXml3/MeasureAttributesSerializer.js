﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.MeasureAttributesSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (attributes) {
        var ma = new NotationJS.MeasureAttributes();

        if (attributes.clef != undefined) {
            var clefSerializer = new NotationJS.MusicXml3.ClefSerializer();

            if (!Array.isArray(attributes.clef)) {
                var clef = clefSerializer.deserialize(attributes.clef);
                ma.clef[(clef.number - 1)] = clef;
            }
            else if (Array.isArray(attributes.clef)) {
                for (var i = 0; i < attributes.clef.length; i++) {
                    var clef = clefSerializer.deserialize(attributes.clef[i]);
                    ma.clef[(clef.number - 1)] = clef;
                }
            }
        }

        if (attributes.key != undefined)
            ma.key = new NotationJS.MusicXml3.KeySerializer().deserialize(attributes.key);
        if (attributes.time != undefined)
            ma.time = new NotationJS.MusicXml3.TimeSerializer().deserialize(attributes.time);
        if (attributes.divisions != undefined && attributes.divisions["#text"] != undefined)
            ma.divisions = attributes.divisions["#text"];

        return ma;
    };
};