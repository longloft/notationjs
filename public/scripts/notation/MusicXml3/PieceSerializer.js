﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.PieceSerializer = function () {
    "user strict";

    var self = this;

    self.credits = "";
    self.partsRaw = [];
    self.defaults = null;
    self.partList = null;

    self.deserialize = function (doc) {

        var piece = new NotationJS.Piece();

        //setup measurements based off of defaults
        self.setupMeasurements(piece, doc);


        self.credits = doc["score-partwise"][1].credit;
        self.partsRaw = doc["score-partwise"][1].part;
        self.defaults = doc["score-partwise"][1].defaults;
        self.partList = doc["score-partwise"][1]["part-list"];

        if (typeof (self.partsRaw.length) == "undefined") {
            self.partsRaw = [self.partsRaw];
        }

        piece.musicFontPoint = self.getMusicFontSettings(self.defaults);
        piece.wordFontPoint = self.getWordFontSettings(self.defaults);

        self.parseLayout(self.defaults, piece);

        self.getGroups(self.partList, piece);

        self.parsePages(piece);
        self.parseCreditList(piece);

        return piece;
    };

    //sets up all necessary measurements
    self.setupMeasurements = function (piece, doc) {

        var defaults = doc["score-partwise"][1].defaults;

        //set music font settings
        var musicFontPoint = self.getMusicFontSettings(defaults);
        var wordFontPoint = self.getWordFontSettings(defaults);

        //musicFontPoint.fontSize = 21;
        var noteFont = "normal " + musicFontPoint.fontSize + "pt MaestroNotes";

        //set note font for sizing
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        ctx.font = noteFont;

        var noteStaffPoint = {};

        noteStaffPoint.noteHeadWidth = ctx.measureText(piece.fontCharCodes.noteHead).width;
        // noteStaffPoint.noteHeadHeight = canvasUtils.getNoteHeadHeight(self.fontCharCodes.noteHead, noteFont);
        noteStaffPoint.noteHeadHeight = noteStaffPoint.noteHeadWidth * 0.8;//noteStaffPoint.noteHeadWidth - 1;
        noteStaffPoint.staffHeight = (noteStaffPoint.noteHeadHeight * 4);

        noteStaffPoint.tenthsPixal = noteStaffPoint.noteHeadHeight / 10;

        piece.noteHeadWidth = noteStaffPoint.noteHeadWidth;
        piece.noteHeadHeight = noteStaffPoint.noteHeadHeight;
        piece.staffHeight = noteStaffPoint.staffHeight;
        piece.tenthsPixal = noteStaffPoint.tenthsPixal;

        return noteStaffPoint;
    };


    self.parsePages = function (piece) {
        var returnParts = [];
        var pageNumber = 1;

        var page = new NotationJS.Page();
        page.number = pageNumber;
        self.parsePageLayout(page, piece);
        page.credits = self.getPageCredits(page);
        page.piece = piece;

        piece.pages.push(page);

        for (var i = 0; i < self.partsRaw.length; i++) {
            var part = self.partsRaw[i];
            var returnPart = null;

            for (var y = 0; y < piece.parts.length; y++) {
                var p = piece.parts[y];
                if (p.id == part["@attributes"].id) {
                    returnPart = p;
                    break;
                }
            }

            var measures = part.measure;

            if (typeof (measures) == "undefined")
                measures = [measures];

            pageNumber = 1;

            //reset to first page
            if (piece.pages.length > 0)
                page = piece.pages[0];

            //push the last array of measure to the page.
            var pagePart = new NotationJS.PagePart();
            pagePart.page = page;
            pagePart.part = returnPart;
            page.parts.push(pagePart);

            //var pageMeasures = [];
            var currentMeasureAttributes = null;
            var currentMeasureStaffDistances = [];
            var currentBeams = [];
            var currentSlurs = [];
            var currentEndSlurs = [];


            for (var x = 0; x < measures.length; x++) {
                var measure = measures[x];

                if (measure.print && measure.print["@attributes"] && measure.print["@attributes"]["new-page"]) {
                    pageNumber += 1;
                }

                if (measure.print && measure.print["staff-layout"]) {
                    returnPart.staffDistance = (measure.print["staff-layout"] && measure.print["staff-layout"]["staff-distance"]) ? piece.convertTenthsToPixals(parseInt(measure.print["staff-layout"]["staff-distance"]["#text"])) : 0;
                }

                if (measure.attributes && measure.attributes != undefined && measure.attributes.staves != undefined && measure.attributes.staves["#text"] != undefined) {
                    returnPart.numberStaves = parseInt(measure.attributes.staves["#text"], 10);
                }


                if (pageNumber != page.number) {

                    //find page
                    var foundPage = false;
                    for (var pp = 0; pp < piece.pages.length; pp++) {
                        if (piece.pages[pp].number == pageNumber) {
                            page = piece.pages[pp];
                            foundPage = true;
                            break;
                        }
                    }

                    if (!foundPage) {
                        page = new NotationJS.Page();
                        page.number = pageNumber;
                        page.credits = self.getPageCredits(page);
                        self.parsePageLayout(page, piece);
                        //page.parts = self.getCurrentPageParts();
                        page.piece = piece;
                        piece.pages.push(page);
                    }

                    var pagePart = new NotationJS.PagePart();
                    pagePart.page = page;
                    pagePart.part = returnPart;
                    page.parts.push(pagePart);
                }

                var m = new NotationJS.MusicXml3.MeasureSerializer(piece, page, returnPart).deserialize(measure);

                pagePart.measures.push(m);
                returnPart.measures.push(m);

                //copy note reference to part and pagepart
                for (var n = 0; n < m.notes.length; n++) {
                    returnPart.notes.push(m.notes[n]);
                    pagePart.notes.push(m.notes[n]);
                }

                //parse beams and slurs
                if (measure.note != undefined) {
                    if (!(measure.note instanceof Array))
                        measure.note = [measure.note];

                    for (var no = 0; no < measure.note.length; no++) {
                        var n = measure.note[no];
                        var note = m.notes[no];

                        if (n.beam != undefined) {
                            if (!(n.beam instanceof Array))
                                n.beam = [n.beam];

                            for (var be = 0; be < n.beam.length; be++) {
                                var b = n.beam[be];
                                var beamNumber = b["@attributes"].number;

                                var beam = null;

                                if (currentBeams[beamNumber] != undefined) {
                                    beam = currentBeams[beamNumber];
                                }
                                else {
                                    beam = new NotationJS.Beam();
                                    beam.piece = piece;
                                    beam.page = pagePart.page;
                                    beam.number = parseInt(beamNumber, 10);
                                    note.beams.push(beam);
                                    pagePart.beams.push(beam);
                                    returnPart.beams.push(beam);
                                    currentBeams[beamNumber] = beam;
                                }

                                if (b["#text"] == "begin") {
                                    beam.startNote = note;
                                }
                                else if (b["#text"] == "end") {
                                    beam.endNote = note;
                                    delete currentBeams[beamNumber];
                                } else if (b["#text"] == "backward hook" || b["#text"] == "forward hook") {
                                    beam.startNote = note;
                                    beam.endNote = note;
                                    beam.type = b["#text"];
                                    beam.isHook = true;

                                    //find reference beam for hook

                                    for (var rb = pagePart.beams.length - 1; rb > 0; rb--) {
                                        //backward hook is always with end note
                                        if (
                                            (b["#text"] == "backward hook" && pagePart.beams[rb].endNote == note && pagePart.beams[rb] != beam) ||
                                            (b["#text"] == "forward hook" && pagePart.beams[rb].startNote == note && pagePart.beams[rb] != beam)
                                            ) {
                                            beam.hookReferenceBeam = pagePart.beams[rb];
                                            break;
                                        }
                                    }

                                    delete currentBeams[beamNumber];
                                }
                            }

                        }

                        //parse slur information on note
                        var slurSerializer = new NotationJS.MusicXml3.SlurSerializer(piece);
                        if (n.notations != undefined && n.notations.slur != undefined) {
                            if (!(n.notations.slur instanceof Array))
                                n.notations.slur = [n.notations.slur];
                            for (var ni = 0; ni < n.notations.slur.length; ni++) {
                                var s = n.notations.slur[ni];

                                var slurNumber = s["@attributes"].number;
                                var slurType = s["@attributes"].type;
                                var slur = null;
                                if (slurType == "start") {
                                    //check if the stop came before the start
                                    if (currentEndSlurs[slurNumber] != undefined) {
                                        slur = currentEndSlurs[slurNumber];
                                        slur = slurSerializer.deserialize(s, slur);
                                        delete currentEndSlurs[slurNumber];
                                    } else {
                                        //create new slur
                                        slur = slurSerializer.deserialize(s);
                                        currentSlurs[slurNumber] = slur;
                                    }
                                    slur.startNote = note;
                                    pagePart.slurs.push(slur);
                                    returnPart.slurs.push(slur);
                                } else if (slurType == "continue") {
                                    slur = currentSlurs[slurNumber];

                                    slurSerializer.deserializeEndSlur(s, slur);

                                } else if (slurType == "stop") {
                                    slur = currentSlurs[slurNumber];
                                    //if not found, stop came before start in xml.
                                    if (slur != undefined) {
                                        slurSerializer.deserializeEndSlur(s, slur);
                                        delete currentSlurs[slurNumber];
                                    }
                                    else {
                                        slur = new NotationJS.Slur();
                                        slur.piece = piece;
                                        currentEndSlurs[slurNumber] = slur;
                                        slurSerializer.deserializeEndSlur(s, slur);
                                    }
                                    slur.endNote = note;

                                }

                                note.isSlurred = true;
                                note.slur = slur;
                            }
                        }
                    }
                }

                //make sure staff distance is persisted for all measures
                if (m.staffDistances.length > 0)
                    currentMeasureStaffDistances = m.staffDistances;

                m.staffDistances = currentMeasureStaffDistances;

                //check for endings
                if (measure.barline != undefined) {
                    var checkEnding = function (measureBarline) {
                        if (measureBarline.ending != undefined) {
                            var endingNumber = measureBarline.ending["@attributes"].number;
                            var endingType = measureBarline.ending["@attributes"].type;

                            var endingSerializer = new NotationJS.MusicXml3.EndingSerializer(piece);
                            var ending = (returnPart.endings[endingNumber] != null) ? returnPart.endings[endingNumber] : endingSerializer.deserialize(measureBarline.ending);
                            ending.part = returnPart;

                            if (endingType == "start") {
                                ending.startMeasure = m;
                            }
                            else {
                                ending.endMeasure = m;
                            }

                            ending.isDiscontinued = (endingType == "discontinue");

                            pagePart.endings[ending.number] = ending;
                            returnPart.endings[ending.number] = ending;
                        }
                    };

                    if (measure.barline instanceof Array) {
                        for (var mbe = 0; mbe < measure.barline.length; mbe++) {
                            checkEnding(measure.barline[mbe]);
                        }
                    }
                    else {
                        checkEnding(measure.barline);
                    }
                }

                //persist attributes
                if (m.attributes != null && m.attributes != undefined) {
                    //merge if previous attributes
                    if (currentMeasureAttributes != null) {
                        if (m.attributes.key == null) {
                            m.attributes.key = currentMeasureAttributes.key;
                        }
                        else {
                            m.newKey = true;
                            m.previousKey = currentMeasureAttributes.key;
                        }

                        if (m.attributes.time == null) {
                            m.attributes.time = currentMeasureAttributes.time;
                        }
                        else {
                            m.newTime = true;
                        }

                        if (m.attributes.divisions == null) {
                            m.attributes.divisions = currentMeasureAttributes.divisions;
                        }
                        else {
                            m.newDivision = true;
                        }

                        //merge clef
                        if (m.attributes.clef.length > 0) {
                            for (var z = 0; z < currentMeasureAttributes.clef.length; z++) {
                                var foundClef = false;
                                for (var f = 0; f < m.attributes.clef.length; f++) {
                                    if (currentMeasureAttributes.clef[z] != undefined && m.attributes.clef[z] != undefined && currentMeasureAttributes.clef[z].number == m.attributes.clef[f].number) {
                                        foundClef = true;
                                        break;
                                    }
                                }

                                if (!foundClef) {
                                    var c = currentMeasureAttributes.clef[z];
                                    m.attributes.clef[(c.number - 1)] = c;
                                }
                            }

                        } else {
                            m.attributes.clef = currentMeasureAttributes.clef;
                        }

                    }
                    currentMeasureAttributes = m.attributes;
                }
                else {
                    m.attributes = currentMeasureAttributes;
                }
            }

            returnParts.push(returnPart);
        }

        //parse note ties
        for (var partIndex = 0; partIndex < returnParts.length; partIndex++) {
            var p = returnParts[partIndex];

            var startedTies = [];

            for (var noteIndex = 0; noteIndex < p.notes.length; noteIndex++) {
                var note = p.notes[noteIndex];
                var previousNote = ((noteIndex - 1) >= 0) ? p.notes[(noteIndex - 1)] : null;

                if (note.isTied && note.tiedType == "start") {
                    startedTies.push(note);
                }

                if (note.isTied && note.tiedType == null) {
                    var found = false;
                    for (var stIndex = 0; stIndex < startedTies.length; stIndex++) {
                        var startTieNote = startedTies[stIndex];

                        if (startTieNote != undefined && note.pitch.step == startTieNote.pitch.step && note.pitch.octave == startTieNote.pitch.octave) {
                            startTieNote.tiedToNote = note;
                            delete startedTies[stIndex];
                            found = true;
                        }
                    }

                    if (found)
                        startedTies.push(note);
                }

                if (note.isTied && note.tiedType == "stop") {
                    for (var stIndex = 0; stIndex < startedTies.length; stIndex++) {
                        var startTieNote = startedTies[stIndex];

                        if (startTieNote != undefined && note.pitch != undefined && startTieNote.pitch != undefined && note.pitch.step == startTieNote.pitch.step && note.pitch.octave == startTieNote.pitch.octave) {
                            startTieNote.tiedToNote = note;
                            delete startedTies[stIndex];
                        }
                    }
                }

                if (previousNote != null && previousNote.isTied && note.isTied && (previousNote.measure.page != note.measure.page || (note.measure.newLine && !previousNote.measure.newLine))) {
                    note.tieBack = true;
                }
            }
        }

        piece.totalNumberPages = pageNumber;

        var pageSerializer = new NotationJS.MusicXml3.PageSerializer();
        for (var ppp = 0; ppp < piece.pages.length; ppp++) {
            pageSerializer.deserialize(piece.pages[ppp]);
        }

        return returnParts;
    };

    //parse out all current page credits
    self.getPageCredits = function (page) {
        var returnCredits = [];
        for (var i = 0; i < self.credits.length; i++) {
            var credit = self.credits[i];
            if (credit && credit["@attributes"] && credit["@attributes"]["page"] == page.number) {
                returnCredits.push(credit);
            }
        }

        return returnCredits;
    };

    self.parseLayout = function (defaults, piece) {
        if (!defaults)
            return;

        if (defaults["page-layout"] && defaults["page-layout"]["page-margins"] instanceof Array) {
            defaults["page-layout"]["page-margins"] = defaults["page-layout"]["page-margins"][0];
        }

        var pageDetails = {};

        piece.pageWidth = (defaults["page-layout"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-width"]["#text"])) : 0;
        piece.pageHeight = (defaults["page-layout"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-height"]["#text"])) : 0;
        piece.pageLeftMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"] && defaults["page-layout"]["page-margins"]["left-margin"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["left-margin"]["#text"])) : 0;
        piece.pageRightMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"] && defaults["page-layout"]["page-margins"]["right-margin"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["right-margin"]["#text"])) : 0;
        piece.pageTopMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["top-margin"]["#text"])) : 0;
        piece.pageBottomMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["bottom-margin"]["#text"])) : 0;
        piece.pageTopSystemDistance = (defaults["system-layout"] && defaults["system-layout"]["top-system-distance"]) ? piece.convertTenthsToPixals(parseInt(defaults["system-layout"]["top-system-distance"]["#text"])) : 0;
        piece.pageSystemDistance = (defaults["system-layout"] && defaults["system-layout"]["system-distance"]) ? piece.convertTenthsToPixals(parseInt(defaults["system-layout"]["system-distance"]["#text"])) : 0;
        piece.pageStaffDistance = (defaults["staff-layout"] && defaults["staff-layout"]["staff-distance"]) ? piece.convertTenthsToPixals(parseInt(defaults["staff-layout"]["staff-distance"]["#text"])) : 0;

        //set default line width
        piece.heavyBarlineWidth = piece.convertTenthsToPixals(10);
        piece.lightBarlineWidth = 1;
        piece.beamWidth = piece.convertTenthsToPixals(6);

        //find and set line widths
        if (defaults.appearance != undefined && defaults.appearance["line-width"] != undefined) {
            for (var u = 0; u < defaults.appearance["line-width"].length; u++) {
                var lineWidth = defaults.appearance["line-width"][u];
                switch (lineWidth["@attributes"].type) {
                    case "heavy barline":
                        piece.heavyBarlineWidth = piece.convertTenthsToPixals(parseFloat(lineWidth["#text"], 10));
                        break;
                    case "light barline":
                        piece.lightBarlineWidth = piece.convertTenthsToPixals(parseFloat(lineWidth["#text"], 10));
                        break;
                    case "beam":
                        piece.beamWidth = piece.convertTenthsToPixals(parseFloat(lineWidth["#text"], 10));
                        break;
                    default:
                }
            }
        }

        return pageDetails;
    };

    //get the page layout data
    self.parsePageLayout = function (page, piece) {
        var defaults = self.defaults;

        if (!defaults)
            return;

        if (defaults["page-layout"] && defaults["page-layout"]["page-margins"] instanceof Array) {
            defaults["page-layout"]["page-margins"] = defaults["page-layout"]["page-margins"][0];
        }

        page.width = (defaults["page-layout"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-width"]["#text"])) : 0;
        page.height = (defaults["page-layout"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-height"]["#text"])) : 0;
        page.leftMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"] && defaults["page-layout"]["page-margins"]["left-margin"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["left-margin"]["#text"])) : 0;
        page.rightMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"] && defaults["page-layout"]["page-margins"]["right-margin"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["right-margin"]["#text"])) : 0;
        page.topMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["top-margin"]["#text"])) : 0;
        page.bottomMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? piece.convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["bottom-margin"]["#text"])) : 0;
        page.topSystemDistance = (defaults["system-layout"] && defaults["system-layout"]["top-system-distance"]) ? piece.convertTenthsToPixals(parseInt(defaults["system-layout"]["top-system-distance"]["#text"])) : 0;
        page.systemDistance = (defaults["system-layout"] && defaults["system-layout"]["system-distance"]) ? piece.convertTenthsToPixals(parseInt(defaults["system-layout"]["system-distance"]["#text"])) : 0;
        page.staffDistance = (defaults["staff-layout"] && defaults["staff-layout"]["staff-distance"]) ? piece.convertTenthsToPixals(parseInt(defaults["staff-layout"]["staff-distance"]["#text"])) : 0;

        page.systemLeftMargin = (defaults["system-layout"] && defaults["system-layout"]["system-margins"] && defaults["system-layout"]["system-margins"]["left-margin"]) ? piece.convertTenthsToPixals(parseInt(defaults["system-layout"]["system-margins"]["left-margin"]["#text"])) : 0;

    };


    self.getGroups = function (partList, piece) {
        var currentGroups = [];

        for (var i = 0; i < partList.length; i++) {
            var o = partList[i];
            if (o.type != undefined && o.type == "part-group" && o["@attributes"] != undefined && o["@attributes"].type == "start") {
                var group = new NotationJS.Group();
                group.name = (o["group-name"] != undefined) ? o["group-name"]["#text"] : "";
                group.number = o["@attributes"].number;
                group.abbreviation = o["group-abbreviation"];
                group.symbol = o["group-symbol"]["#text"];
                group.defaultX = (o["group-symbol"] != undefined && o["group-symbol"]["@attributes"] != undefined) ? piece.convertTenthsToPixals(parseFloat(o["group-symbol"]["@attributes"]["default-x"], 10)) : 0;
                group.barline = (o["group-barline"]["#text"] == "yes");

                piece.groups.push(group);
                currentGroups.push(group);
            }
            else if (o.type != undefined && o.type == "part-group" && o["@attributes"] != undefined && o["@attributes"].type == "stop") {
                var number = o["@attributes"].number;
                //find and remove
                for (var z = 0; z < currentGroups.length; z++) {
                    var g = currentGroups[z];

                    if (g.number == number) {
                        currentGroups.splice(z, 1);
                        break;
                    }
                }

            }
            else if (o.type != undefined && o.type == "score-part") {
                var part = new NotationJS.Part();
                part.name = o["part-name"]["#text"];
                part.id = o["@attributes"].id;
                part.abbreviation = (o["part-abbreviation"] != undefined && o["part-abbreviation"]["#text"] != undefined) ? o["part-abbreviation"]["#text"] : "";
                part.printName = !(o["part-name"] != undefined && o["part-name"]["@attributes"] != undefined && o["part-name"]["@attributes"]["print-object"] == "no");

                part.groups = currentGroups.slice();
                piece.parts.push(part);
            }
        }
    };


    self.parseCreditList = function (piece) {
        var creditsSerializer = new NotationJS.MusicXml3.CreditsSerializer();

        for (var i = 0; i < piece.pages.length; i++) {
            creditsSerializer.deserialize(self.credits, piece.pages[i]);
        }
    };

    //get the font settings for the music font
    self.getMusicFontSettings = function (defaults) {

        var fontSettings = {};
        if (defaults["music-font"] && defaults["music-font"]["@attributes"]) {
            //musicFontPoint.fontFamily = (defaults["music-font"]["@attributes"]["font-family"]) ? defaults["music-font"]["@attributes"]["font-family"];
            fontSettings.fontSize = (defaults["music-font"]["@attributes"]["font-size"]) ? defaults["music-font"]["@attributes"]["font-size"] : "20.5";
        }

        return fontSettings;
    };

    //get the font settings for labels
    self.getWordFontSettings = function (defaults) {
        var fontSettings = {};
        if (defaults["word-font"] && defaults["word-font"]["@attributes"]) {
            //musicFontPoint.fontFamily = (defaults["music-font"]["@attributes"]["font-family"]) ? defaults["music-font"]["@attributes"]["font-family"];
            fontSettings.fontSize = (defaults["word-font"]["@attributes"]["font-size"]) ? defaults["word-font"]["@attributes"]["font-size"] : "10.5";
            fontSettings.fontFamily = (defaults["word-font"]["@attributes"]["font-family"]) ? defaults["word-font"]["@attributes"]["font-family"] : "Times New Roman";
        }

        return fontSettings;
    };
};