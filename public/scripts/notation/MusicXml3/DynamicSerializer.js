﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.DynamicSerializer = function (piece) {
    "user strict";

    var self = this;

    self.deserialize = function (direction) {
        var d = new NotationJS.Dynamic();
        d.piece = piece;

        if (direction == undefined || direction["direction-type"] == undefined || direction["direction-type"].dynamics == undefined) {
            return d;
        }

        if (direction["@attributes"] != undefined && direction["@attributes"].placement != undefined) {
            d.placement = direction["@attributes"].placement;
        }

        var dynamics = direction["direction-type"].dynamics;

        for (var key in dynamics) {
            if (key != "@attributes" && key != "#text") {
                d.mark = key;
                break;
            }
        }

        if (dynamics["@attributes"] != undefined && dynamics["@attributes"]["default-x"] != undefined) {
            d.defaultX = d.piece.convertTenthsToPixals(parseInt(dynamics["@attributes"]["default-x"], 10));
        }

        if (dynamics["@attributes"] != undefined && dynamics["@attributes"]["default-y"] != undefined) {
            d.defaultY = d.piece.convertTenthsToPixals(parseInt(dynamics["@attributes"]["default-y"], 10));
        }

        if (dynamics["@attributes"] != undefined && dynamics["@attributes"]["halign"] != undefined) {
            d.halign = dynamics["@attributes"]["halign"];
        }

        if (direction.staff != undefined && direction.staff["#text"] != undefined) {
            d.staff = parseInt(direction.staff["#text"], 10);
        }

        if (direction.sound != undefined && direction.sound["#text"] != undefined) {
            d.soundDynamic = parseInt(direction.sound["#text"], 10);
        }

        return d;
    };
};