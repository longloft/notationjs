﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.CreditsSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (credits, page) {

        var creditModels = [];

        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        var phrases = [];

        if (!(credits instanceof Array))
            credits = [credits];

        for (var i = 0; i < credits.length; i++) {

            var credit = credits[i];

            if (credit["@attributes"].page == page.number) {

                if (!(credit["credit-words"] instanceof Array)) {
                    credit["credit-words"] = [credit["credit-words"]];
                }

                var creditX = 0;
                var creditY = 0;
                var creditJustify = null;
                var creditVAlign = null;
                var creditHAlign = null;
                var creditFontStyle = null;

                var phrase = null;

                if (credit["credit-words"].length > 1) {
                    phrase = [];
                    phrases.push(phrase);
                }

                for (var y = 0; y < credit["credit-words"].length; y++) {

                    var creditWord = credit["credit-words"][y];

                    var creditType = (credit["credit-type"]) ? credit["credit-type"]["#text"] : "";
                    var creditWordText = creditWord["#text"];
                    var creditFontSize = creditWord["@attributes"]["font-size"];
                    creditJustify = (creditWord["@attributes"]["justify"]) ? creditWord["@attributes"]["justify"] : creditJustify;
                    creditVAlign = (creditWord["@attributes"]["valign"]) ? creditWord["@attributes"]["valign"] : creditVAlign;
                    creditHAlign = (creditWord["@attributes"]["halign"]) ? creditWord["@attributes"]["halign"] : creditHAlign;
                    var creditFont = (creditWord["@attributes"]["font-family"]) ? creditWord["@attributes"]["font-family"] : "Times New Roman";
                    var creditFontWeight = (creditWord["@attributes"]["font-weight"]) ? creditWord["@attributes"]["font-weight"] : "normal";
                    creditFontStyle = (creditWord["@attributes"]["font-style"]) ? creditWord["@attributes"]["font-style"] : creditFontStyle;

                    //line breaks not sported with canvas
                    //need to break apart by line break
                    var creditLines = creditWordText.split("\n");

                    if (typeof (creditWord["@attributes"]["default-x"]) != "undefined") {
                        creditX = page.piece.convertTenthsToPixals(parseInt(creditWord["@attributes"]["default-x"]));
                    }
                    if (typeof (creditWord["@attributes"]["default-y"]) != "undefined") {
                        creditY = page.piece.convertTenthsToPixals(parseInt(creditWord["@attributes"]["default-y"]));
                    }

                    var widestText = 0;
                    //get widest string length
                    for (var s = 0; s < creditLines.length; s++) {
                        ctx.font = creditFontWeight + ' ' + creditFontSize + 'pt ' + creditFont;

                        if (creditFontStyle != undefined)
                            ctx.font = creditFontStyle + " " + ctx.font;

                        var lineWidth = ctx.measureText(creditLines[s]).width;
                        if (lineWidth > widestText)
                            widestText = lineWidth;
                    }

                    var creditLineModels = [];

                    for (var cl = 0; cl < creditLines.length; cl++) {
                        var c = new NotationJS.Credit();

                        c.page = page;

                        c.text = creditLines[cl];
                        c.type = creditType;
                        c.x = creditX;
                        c.y = (page.height - creditY) + ((creditFontSize * 1.618) * cl);
                        c.valign = creditVAlign;
                        c.halign = (creditJustify != undefined && creditHAlign == undefined) ? creditJustify : creditHAlign;
                        c.fontSize = creditFontSize;
                        c.font = creditFont;
                        c.fontWeight = creditFontWeight;
                        c.justify = creditJustify;
                        c.fontStyle = creditFontStyle;

                        //reset context to get text width
                        ctx.font = creditFontWeight + ' ' + creditFontSize + 'pt ' + creditFont;

                        if (creditFontStyle != undefined)
                            ctx.font = creditFontStyle + " " + ctx.font;

                        c.width = ctx.measureText(c.text).width;

                        //adjust x for justify for halign or valign
                        if (creditJustify == "center" && creditHAlign == "right" && c.width < widestText)
                            c.x -= (widestText - c.width) * 0.5;
                        else if (creditJustify == "center" && creditHAlign == "left" && c.width < widestText)
                            c.x += (widestText - c.width) * 0.5;

                        creditModels.push(c);
                        creditLineModels.push(c);

                        if (phrase != null && creditLines.length == 1)
                            phrase.push(c);

                    }

                    //fix valign bottom y positions
                    if (creditVAlign == "bottom" && creditLineModels.length > 1)
                    {
                        var firstModel = creditLineModels[0];
                        var lastModel = creditLineModels[creditLineModels.length - 1];
                        var offset = lastModel.y - firstModel.y;
                        for (var clm = 0; clm < creditLineModels.length; clm++) {
                            var model = creditLineModels[clm];
                            model.y -= offset;
                        }
                    }

                    creditY = creditY - ((creditFontSize * 1.618) * (creditLines.length - 1));
                }

                //fix credit words on same line
                for (var cw = 0; cw < phrases.length; cw++) {
                    var phrase = phrases[cw];

                    if (phrase.length > 0) {
                        var combinedWidth = 0;
                        var creditX = phrase[0].x;
                        var creditY = phrase[0].y;
                        var creditHalign = phrase[0].halign;

                        for (var j = 0; j < phrase.length; j++) {
                            var cred = phrase[j];
                            combinedWidth += cred.width;
                        }

                        if (creditHalign == "center") {
                            var leftX = creditX - (combinedWidth * 0.5);
                            var totaledWidth = 0;
                            for (var k = 0; k < phrase.length; k++) {
                                var cred = phrase[k];

                                cred.x = totaledWidth + leftX + (cred.width * 0.5);
                                totaledWidth += cred.width;
                            }
                        }
                    }

                }

            }
        }

        page.credits = creditModels;
    };
};