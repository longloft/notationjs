﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.PitchSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (pitch) {
        var p = new NotationJS.Pitch();

        if (pitch.octave != undefined && pitch.octave["#text"] != undefined)
            p.octave = parseInt(pitch.octave["#text"], 10);
        if (pitch.alter != undefined && pitch.alter["#text"] != undefined)
            p.alter = parseInt(pitch.alter["#text"], 10);
        if (pitch.step != undefined && pitch.step["#text"] != undefined)
            p.step = pitch.step["#text"];

        return p;
    };
};