﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.ArticulationsSerializer = function (piece) {
    "user strict";

    var self = this;

    self.deserialize = function (articulations) {

        var returnList = [];

        for (var prop in articulations) {
            if (prop != "#text") {
                var art = articulations[prop];

                var articulation = new NotationJS.Articulation();
                articulation.name = prop;

                if (art["@attributes"] != undefined) {
                    if (art["@attributes"]["default-x"] != undefined)
                        articulation.defaultX = piece.convertTenthsToPixals(parseInt(art["@attributes"]["default-x"], 10));
                    if (art["@attributes"]["default-y"] != undefined)
                        articulation.defaultY = piece.convertTenthsToPixals(parseInt(art["@attributes"]["default-y"], 10));

                    if (art["@attributes"]["placement"] != undefined)
                        articulation.placement = art["@attributes"]["placement"];

                    if (art["@attributes"]["type"] != undefined)
                        articulation.type = art["@attributes"]["type"];
                }

                returnList.push(articulation);
            }
        }

        return returnList;
    };

    self.parse = function (articulation, note) {
        if (note.chord != null && note.chord.stemNote)
            note = note.chord.stemNote;

        //self.pieceRenderer.setMusicFont();

        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        var code = null;
        var codeHeight = 0;
        var padding = note.piece.noteHeadHeight;
        switch (articulation.name) {
            case "staccato":
                code = note.piece.fontCharCodes.staccato;
                codeHeight = note.piece.convertTenthsToPixals(1);
                break;
            case "strong-accent":
                code = (articulation.placement == "below") ? note.piece.fontCharCodes.strongAccentInverted : note.piece.fontCharCodes.strongAccent;
                codeHeight = note.piece.convertTenthsToPixals(9.5);
                break;
            case "accent":
                code = note.piece.fontCharCodes.accent;
                codeHeight = note.piece.convertTenthsToPixals(11.5);
                break;
        }

        if (code == null)
            return;

        var artX = note.x + (note.piece.noteHeadWidth * 0.5) - (ctx.measureText(code).width * 0.5);
        var artY = (articulation.placement == "below") ? (note.y + codeHeight + padding) : note.y - note.piece.noteHeadHeight;
        articulation.code = code;

        //get staff top and bottom y
        var measure = note.measure;

        //others need to stay above and below staff
        if (articulation.name != "staccato") {
            var topY = measure.staffTopYs[note.staff - 1] - (note.piece.noteHeadHeight * 0.5);
            var bottomY = measure.staffBottomYs[note.staff - 1] + (note.piece.noteHeadHeight * 0.5) + codeHeight;

            if (artY > topY && articulation.placement == "above") {
                artY = topY;
            }

            if (artY < bottomY && articulation.placement == "below") {
                artY = bottomY;
            }
        }

        //adjust for stem
        if (articulation.placement == "above" && note.stemDirection == "up") {
            artY = note.stemY - (note.piece.noteHeadHeight * 0.5);
        }

        // fit staccato in staff
        if (articulation.name == "staccato") {
            if ((artY == measure.staffTopYs[note.staff - 1] ||
                artY == measure.staffMiddleYs[note.staff - 1] ||
                artY == measure.staffBottomYs[note.staff - 1] ||
                artY == measure.staffTopMiddleYs[note.staff - 1] ||
                artY == measure.staffBottomMiddleYs[note.staff - 1]) ||
                (artY == (measure.staffTopYs[note.staff - 1] + codeHeight) ||
                artY == (measure.staffMiddleYs[note.staff - 1] + codeHeight) ||
                artY == (measure.staffBottomYs[note.staff - 1] + codeHeight) ||
                artY == (measure.staffTopMiddleYs[note.staff - 1] + codeHeight) ||
                artY == (measure.staffBottomMiddleYs[note.staff - 1] + codeHeight))
                ) {
                if (articulation.placement == "above")
                    artY -= note.piece.noteHeadHeight * 0.5;
                else
                    artY += note.piece.noteHeadHeight * 0.5;
            }
        }

        articulation.x = artX;
        articulation.y = artY;
    };

    return self;
};