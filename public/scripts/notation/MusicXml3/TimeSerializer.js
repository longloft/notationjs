﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.TimeSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (time) {
        var t = new NotationJS.Time();

        if (time.beats != undefined && time.beats["#text"] != undefined)
            t.beats = parseInt(time.beats["#text"]);
        if (time["beat-type"] != undefined && time["beat-type"]["#text"] != undefined)
            t.type = parseInt(time["beat-type"]["#text"]);

        return t;
    };
};