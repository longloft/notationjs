﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.KeySerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (key) {
        var k = new NotationJS.Key();

        if (key.fifths != undefined && key.fifths["#text"] != undefined)
            k.fifths = parseInt(key.fifths["#text"], 10);
        if (key.mode != undefined && key.mode["#text"] != undefined)
            k.mode = key.mode["#text"];

        return k;
    };
};