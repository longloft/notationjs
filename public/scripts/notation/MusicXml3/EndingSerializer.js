﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.EndingSerializer = function (piece) {
    "user strict";

    var self = this;

    self.deserialize = function (ending) {
        var e = new NotationJS.Ending();
        e.piece = piece;
       
        if (ending["@attributes"] != undefined) {

            var attributes = ending["@attributes"];

            if (attributes.number != undefined)
                e.number = attributes.number;
            if (attributes["default-y"] != undefined)
                e.defaultY = e.piece.convertTenthsToPixals(parseInt(attributes["default-y"], 10));
            if (attributes["default-x"] != undefined)
                e.defaultX = e.piece.convertTenthsToPixals(parseInt(attributes["default-x"], 10));


            if (attributes["end-length"] != undefined)
                e.jogLength = e.piece.convertTenthsToPixals(parseInt(attributes["end-length"], 10));

            if (attributes["font-size"] != undefined)
                e.fontSize = attributes["font-size"];

            if (attributes["print-object"] != undefined && attributes["print-object"] == "no") {
                e.drawEnding = false;
            }
        }

        return e;
    };
};