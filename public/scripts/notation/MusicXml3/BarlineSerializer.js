﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.BarlineSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (barline) {
        var b = new NotationJS.Barline();
        if (barline != undefined && barline["bar-style"]) {
            b.style = barline["bar-style"]["#text"];
        }

        if (barline != undefined && barline["@attributes"] != undefined && barline["@attributes"].location != undefined) {
            b.location = barline["@attributes"].location;
        }

        if (barline != undefined && barline.repeat != undefined) {
            b.isRepeat = true;
            b.repeatDirection = barline.repeat["@attributes"].direction;
            b.repeatWinged = barline.repeat["@attributes"].winged;
        }

        return b;
    };
};