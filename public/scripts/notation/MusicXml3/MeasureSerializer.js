﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.MeasureSerializer = function (piece, page, part) {
    "user strict";

    var self = this;

    self.deserialize = function (measure) {
        var m = new NotationJS.Measure();
        m.piece = piece;
        m.page = page;
        m.part = part;

        if (measure["@attributes"] != undefined) {
            m.width = m.piece.convertTenthsToPixals(parseInt(measure["@attributes"].width));
            m.number = parseInt(measure["@attributes"].number, 10);
        }

        if (measure.print && measure.print["system-layout"]) {
            m.leftMargin = (measure.print["system-layout"]["system-margins"] && measure.print["system-layout"]["system-margins"]["left-margin"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-margins"]["left-margin"]["#text"])) : 0;
            m.rightMargin = (measure.print["system-layout"]["system-margins"] && measure.print["system-layout"]["system-margins"]["right-margin"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-margins"]["right-margin"]["#text"])) : 0;
            m.topDistance = (measure.print["system-layout"]["top-system-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["top-system-distance"]["#text"])) : 0;
        }

        if (measure.print && measure.print["system-layout"]) {
            m.systemDistance = (measure.print["system-layout"]["system-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-distance"]["#text"])) : 0;
        }

        if (measure.print) {
            if (measure.print["@attributes"])
                m.newLine = (measure.print["@attributes"]["new-system"] != undefined);
            if (measure.print["system-layout"])
                m.measureSystemDistance = (measure.print["system-layout"]["system-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-distance"]["#text"])) : m.systemDistance;
            if (measure.print["staff-layout"]) {
                var staffDistance = (measure.print["staff-layout"] && measure.print["staff-layout"]["staff-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["staff-layout"]["staff-distance"]["#text"])) : 0;
                var staffNumber = (measure.print["staff-layout"] && measure.print["staff-layout"]["@attributes"] && measure.print["staff-layout"]["@attributes"].number != undefined) ? parseInt(measure.print["staff-layout"]["@attributes"].number, 10) : 1;
                m.staffDistances[staffNumber] = staffDistance;
            }

            if (measure.print["page-layout"]) {
                m.page.width = (measure.print["page-layout"]["page-width"] != undefined && measure.print["page-layout"]["page-width"]["#text"] != undefined) ? m.piece.convertTenthsToPixals(parseInt(measure.print["page-layout"]["page-width"]["#text"])) : m.page.width;
                m.page.height = (measure.print["page-layout"]["page-height"] != undefined && measure.print["page-layout"]["page-height"]["#text"] != undefined) ? m.piece.convertTenthsToPixals(parseInt(measure.print["page-layout"]["page-height"]["#text"])) : m.page.height;
                m.page.leftMargin = (measure.print["page-layout"]["page-margins"] != undefined && measure.print["page-layout"]["page-margins"]["left-margin"]["#text"] != undefined) ? m.piece.convertTenthsToPixals(parseInt(measure.print["page-layout"]["page-margins"]["left-margin"]["#text"])) : m.page.leftMargin;
                m.page.rightMargin = (measure.print["page-layout"]["page-margins"] != undefined && measure.print["page-layout"]["page-margins"]["right-margin"]["#text"] != undefined) ? m.piece.convertTenthsToPixals(parseInt(measure.print["page-layout"]["page-margins"]["right-margin"]["#text"])) : m.page.rightMargin;
                m.page.topMargin = (measure.print["page-layout"]["page-margins"] != undefined && measure.print["page-layout"]["page-margins"]["top-margin"]["#text"] != undefined) ? m.piece.convertTenthsToPixals(parseInt(measure.print["page-layout"]["page-margins"]["top-margin"]["#text"])) : m.page.topMargin;
                m.page.bottomMargin = (measure.print["page-layout"]["page-margins"] != undefined && measure.print["page-layout"]["page-margins"]["bottom-margin"]["#text"] != undefined) ? m.piece.convertTenthsToPixals(parseInt(measure.print["page-layout"]["page-margins"]["bottom-margin"]["#text"])) : m.page.bottomMargin;


                m.topSystemDistance = (measure.print["system-layout"] && measure.print["system-layout"]["top-system-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["top-system-distance"]["#text"])) : 0;
                m.page.systemDistance = (measure.print["system-layout"] && measure.print["system-layout"]["system-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-distance"]["#text"])) : m.page.systemDistance;
                m.page.staffDistance = (measure.print["staff-layout"] && measure.print["staff-layout"]["staff-distance"]) ? m.piece.convertTenthsToPixals(parseInt(measure.print["staff-layout"]["staff-distance"]["#text"])) : m.page.staffDistance;
            }
        }

        if (measure.attributes != undefined) {
            m.attributes = new NotationJS.MusicXml3.MeasureAttributesSerializer().deserialize(measure.attributes);

            if (measure["attributes"]["measure-style"] != undefined && measure["attributes"]["measure-style"]["multiple-rest"] != undefined && measure["attributes"]["measure-style"]["multiple-rest"]["#text"] != undefined) {
                m.numberMeasuresRest = parseInt(measure["attributes"]["measure-style"]["multiple-rest"]["#text"], 10);
                m.isMultiMeasureRest = true;
            }
        }

        if (measure.barline != undefined) {
            var barlineSerializer = new NotationJS.MusicXml3.BarlineSerializer();
            if (measure.barline instanceof Array) {
                for (var mb = 0; mb < measure.barline.length; mb++) {
                    m.barline = barlineSerializer.deserialize(measure.barline[mb]);
                }
            }
            else {
                m.barline = barlineSerializer.deserialize(measure.barline);
            }

        }

        if (measure.note != undefined) {
            if (measure.note.constructor !== Array)
                measure.note = [measure.note];

            for (var i = 0; i < measure.note.length; i++) {
                var note = measure.note[i];
                var n = new NotationJS.MusicXml3.NoteSerializer(m.piece, m.part, m).deserialize(note);
                m.notes.push(n);
            }

            //figure out chords
            var currentChord = null;
            for (var j = 0; j < m.notes.length; j++) {
                var note = m.notes[j];
                var nextNote = ((j + 1) < m.notes.length) ? m.notes[(j + 1)] : null;

                if (nextNote != null && nextNote.partOfChord && !note.partOfChord) {
                    currentChord = new NotationJS.Chord();
                    currentChord.measure = m;
                    currentChord.initialNote = note;
                    currentChord.addNote(note);
                }

                if (note.partOfChord && currentChord != null) {
                    currentChord.addNote(note);
                }

                if ((nextNote == null || !nextNote.partOfChord) && currentChord != null) {
                    m.chords.push(currentChord);
                    currentChord = null;
                }
            }
        }


        if (measure.direction != undefined) {

            if (!(measure.direction instanceof Array)) {
                measure.direction = [measure.direction];
            }

            for (var md = 0; md < measure.direction.length; md++) {
                var direction = measure.direction[md];

                if (direction["direction-type"] != undefined && direction["direction-type"].dynamics != undefined) {
                    var dynamicSerializer = new NotationJS.MusicXml3.DynamicSerializer(m.piece);

                    var dynamic = dynamicSerializer.deserialize(direction);
                    dynamic.measure = m;

                    m.dynamics.push(dynamic);
                }
            }
        }

        return m;
    };
};