﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

NotationJS.MusicXml3.PageSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (page) {

        //page.parts = parts;
        var parts = page.parts;

        var numberMeasures = parts[0].measures.length;
        var partMeasureX = page.leftMargin + page.systemLeftMargin;
        var partMeasureY = page.topSystemDistance + page.topMargin;
        var partMeasureBottomY = 0;
        console.log("partMeasureX=" + partMeasureX);

        var pageLeftMargin = partMeasureX;

        //persist the staff distance for whole page
        var staffDistance = page.staffDistance;

        var printBrackets = true;

        for (var m = 0; m < numberMeasures; m++) {
            var measure = parts[0].measures[m];
            var measureSystemDistance = page.systemDistance;
            //var firstMeasureOfPiece = (measure["@attributes"] && measure["@attributes"].number == "1");
            firstMeasureOfPiece = measure.number;
            var newLine = measure.newLine;
            if (measure.leftMargin != undefined)
                pageLeftMargin = page.leftMargin + measure.leftMargin;

            if (measure.measureSystemDistance > 0)
                measureSystemDistance = measure.measureSystemDistance;

            if (newLine) {
                //want to print the brackets for the new line
                printBrackets = true;
                //reset to page margin
                partMeasureX = pageLeftMargin;

                //add to bottom y of last measure
                partMeasureY = (partMeasureBottomY + measureSystemDistance);
            }

            var measureY = partMeasureY;
            var firstPart = true;
            var partMeasureDrawDetails = [];

            for (var i = 0; i < parts.length; i++) {
                var part = parts[i];
                var p = page.parts[i].part;
                var measureModel = part.measures[m];
                var partMeasure = part.measures[m];

                if (page.measures[measureModel.number] == undefined) {
                    page.measures[measureModel.number] = [];
                }

                page.measures[measureModel.number].push(measureModel);

                if (measureModel.topDistance != 0) {
                    measureModel.y = partMeasureY = measureY = measureModel.topDistance + page.topMargin;
                    measureModel.x = partMeasureX = measureModel.leftMargin + page.leftMargin;
                } else {
                    measureModel.y = measureY;
                    measureModel.x = partMeasureX;
                }
                if (measureModel.staffDistances[1] != undefined && measureModel.staffDistances[1] > 0)
                    staffDistance = measureModel.staffDistances[1];
                else
                    staffDistance = page.staffDistance;

                //get the staff distance print override in xml for part on next measure
                //so we have to calculate the y before we print
                if (!firstPart)
                    measureY = partMeasureBottomY + staffDistance;

                measureModel.y = measureY;

                firstPart = false;

                var firstStaff = true;
                var topStaffDistance = measureY;
                var staffBottomY = [];
                var staffMiddleY = [];
                var staffTopMiddleY = [];
                var staffBottomMiddleY = [];
                var staffTopY = [];
                for (var z = 0; z < p.numberStaves; z++) {
                    if (!firstStaff) {
                        topStaffDistance += (page.piece.staffHeight + (measureModel.staffDistances[(z + 1)] || page.staffDistance));
                    }
                    firstStaff = false;
                    measureModel.bottomY = (topStaffDistance + page.piece.staffHeight);
                    staffBottomY.push(measureModel.bottomY);

                    var topY = topStaffDistance;
                    var bottomY = measureModel.bottomY;
                    var middleY = (topStaffDistance + ((measureModel.bottomY - topStaffDistance) * 0.5));
                    var topMiddleY = topY + ((middleY - topY) * 0.5);
                    var bottomMiddleY = middleY + ((bottomY - middleY) * 0.5);

                    staffTopY.push(topY);
                    staffMiddleY.push(middleY);
                    staffTopMiddleY.push(topMiddleY);
                    staffBottomMiddleY.push(bottomMiddleY);
                }

                measureModel.staffTopYs = staffTopY;
                measureModel.staffBottomYs = staffBottomY;
                measureModel.staffMiddleYs = staffMiddleY;
                measureModel.staffTopMiddleYs = staffTopMiddleY;
                measureModel.staffBottomMiddleYs = staffBottomMiddleY;

                //have measure adjust note positions
                measureModel.adjustNotePositions();

                partMeasureBottomY = measureModel.bottomY;

                partMeasureX = measureModel.x;
                measureY = measureModel.y;
            }

            partMeasureX += measureModel.width;
        }

        var measureGroupSerializer = new NotationJS.MusicXml3.MeasureGroupSerializer();
        for (var h = 0; h < page.measures.length; h++) {
            if (page.measures[h] != undefined) {
                page.measureGroups.push(measureGroupSerializer.deserialize(page.measures[h]));

                for (var mn = 0; mn < page.measures[h].length; mn++) {
                    var measure = page.measures[h][mn];

                    for (var ni = 0; ni < measure.notes.length; ni++) {
                        new NotationJS.MusicXml3.NoteSerializer(page.piece).parse(measure.notes[ni]);
                    }
                }
            }
        }

        var slurSerializer = new NotationJS.MusicXml3.SlurSerializer(page.piece);
        for (var pi = 0; pi < parts.length; pi++) {
            slurSerializer.processSlurs(parts[pi].slurs);

        }
    };
};