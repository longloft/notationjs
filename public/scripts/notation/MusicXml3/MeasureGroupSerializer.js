﻿var NotationJS = NotationJS || {};
NotationJS.MusicXml3 = NotationJS.MusicXml3 || {};

//groups together measure for barline drawing.
NotationJS.MusicXml3.MeasureGroupSerializer = function () {
    "user strict";

    var self = this;

    self.deserialize = function (measures) {
        var mg = new NotationJS.MeasureGroup();

        var topMeasure = null;
        var previousTopMeasure = null;
        var currentGroups = [];
        var currentGroupMeasures = [];
        var previousMeasure = null;

        mg.measures = measures;

        //loop part measures for measure
        for (var x = 0; x < measures.length; x++) {
            //part measure
            var measure = measures[x];
            var nextMeasure = ((x + 1) < measures.length) ? measures[(x + 1)] : null;

            if (mg.measure == null)
                mg.measure = measure;

            //see if the measure is in a group
            if (measure.part.groups != undefined && measure.part.groups.length > 0) {
                var groups = measure.part.groups;

                for (var t = currentGroups.length - 1; t >= 0; t--) {
                    var g = currentGroups[t];
                    var index = groups.indexOf(g);
                    if (index == -1) {
                        currentGroups.splice(t, 1);
                    }

                    if (currentGroups.length <= 0) {
                        previousTopMeasure = topMeasure;
                        topMeasure = null;
                    }
                }

                var containsCurrentGroup = false;
                var containsNewGroup = true;
                for (var j = 0; j < groups.length; j++) {
                    var group = groups[j];

                    if (currentGroups.indexOf(group) > -1) {
                        containsCurrentGroup = true;
                    }
                    else {
                        if (group.barline) {
                            if (currentGroups.length <= 0) {
                                topMeasure = measure;
                            }
                            currentGroups.push(group);
                            containsNewGroup = true;
                        }
                    }
                }

                //draw previous grouping
                if (!containsCurrentGroup && previousTopMeasure != null) {
                    var gSplice1 = currentGroupMeasures.slice(0);
                    mg.barlineMeasureGroupings.push(gSplice1);
                    currentGroupMeasures = [];
                }

                //draw if group of one
                if (!containsCurrentGroup && topMeasure == null) {
                    mg.barlineMeasureGroupings.push([measure]);
                    currentGroupMeasures = [];
                }

                //draw if last part
                if (nextMeasure == null && topMeasure != null) {
                    var gSplice2 = currentGroupMeasures.slice(0);
                    gSplice2.push(measure);
                    mg.barlineMeasureGroupings.push(gSplice2);
                    currentGroupMeasures = [];
                }

                //null top measure if no more groups
                if (currentGroups.length <= 0) {
                    topMeasure = null;
                    currentGroupMeasures = [];
                }
                currentGroupMeasures.push(measure);
                previousMeasure = measure;
            }
            else {
                //draw previous groupings
                if (currentGroupMeasures.length > 0) {
                    var gSplice1 = currentGroupMeasures.slice(0);
                    mg.barlineMeasureGroupings.push(gSplice1);
                    currentGroupMeasures = [];
                }

                //print barline for single part
                mg.barlineMeasureGroupings.push([measure]);

            }
        }

        return mg;
    };
};