﻿function NotationParser() {
    
    var self = this;


    self.parseXmlString = function (xmlString) {

        var parseXml;

        if (typeof window.DOMParser != "undefined") {
            parseXml = function (xmlStr) {
                return (new window.DOMParser()).parseFromString(xmlStr, "text/xml");
            };
        } else if (typeof window.ActiveXObject != "undefined" &&
            new window.ActiveXObject("Microsoft.XMLDOM")) {
            parseXml = function (xmlStr) {
                var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = "false";
                xmlDoc.loadXML(xmlStr);
                return xmlDoc;
            };
        } else {
            throw new Error("No XML parser found");
        }

        var xml = parseXml(xmlString);
        var notationDoc = self.xmlToJson(xml);

        reparsePartList(notationDoc, xml);
        
        return notationDoc;
    };

    //fix groupings
    var reparsePartList = function (notationDoc, xml) {
        if (notationDoc["score-partwise"] != null &&
            notationDoc["score-partwise"][1] != null &&
            notationDoc["score-partwise"][1]["part-list"] != null) {

            var partList = [];
            var partListXml = xml.getElementsByTagName("part-list")[0];//xml.childNodes[1].childNodes[1].childNodes;

            for (var i = 0; i < partListXml.childNodes.length; i++) {
                var item = partListXml.childNodes.item(i);
                if (item.nodeType == 1) {
                    var json = self.xmlToJson(item);
                    json.type = item.nodeName;
                    partList.push(json);
                }
            }

            notationDoc["score-partwise"][1]["part-list"] = partList;
        }
    };

    self.xmlToJson = function (xml) {

        // Create the return object
        var obj = {};

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
                obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) { // text
            obj = xml.nodeValue;
        }

        // do children
        if (xml.hasChildNodes()) {
            for (var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof (obj[nodeName]) == "undefined") {
                    obj[nodeName] = self.xmlToJson(item);
                } else {
                    if (typeof (obj[nodeName].push) == "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(self.xmlToJson(item));
                }
            }
        }
        return obj;
    };

    
}