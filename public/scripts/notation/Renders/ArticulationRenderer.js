﻿var NotationJS = NotationJS || {};

NotationJS.ArticulationRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (articulation, note) {

        if (articulation == undefined || articulation.code == null)
            return;

        ctx.fillText(articulation.code, articulation.x, articulation.y);

    };
};