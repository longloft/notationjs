﻿var NotationJS = NotationJS || {};

NotationJS.FermataRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (fermata) {
        
        var note = fermata.note;

        if (note.chord != null)
            note = note.chord.stemNote;

        var startX = note.x + fermata.defaultX;
        var startY = note.measure.staffTopYs[(note.staff - 1)] - fermata.defaultY;
       
        //draw text
        self.pieceRenderer.setMusicFont();

        ctx.fillText(fermata.note.piece.fontCharCodes.fermata, startX, startY);

    };
};