﻿var NotationJS = NotationJS || {};

NotationJS.MeasureAttributesRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.piece = null;
    self.part = null;

    self.draw = function (measures, firstMeasure) {
        var measure = measures[0];

        if (measure.newLine || measure.newTime || measure.newKey || firstMeasure) {
            var drawTime = ((measure.number == 0 && firstMeasure) || (measure.number == 1 && firstMeasure) || measure.newTime);
            var maxKeyX = 0;

            if (measure.newLine || measure.newKey || firstMeasure) {
                for (var i = 0; i < measures.length; i++) {

                    var keyY = measures[i].x;

                    if (measure.number == 1 || firstMeasure || measure.newLine)
                        keyY = self.drawMeasure(measures[i]);

                    if (firstMeasure || measure.newLine || measure.newKey)
                        keyY = self.drawKey(measures[i], keyY);

                    if (keyY > maxKeyX)
                        maxKeyX = keyY;
                }
            }

            if (maxKeyX > 0) {
                measure.paddingLeft = (maxKeyX - measure.x) + self.piece.convertTenthsToPixals(10);
                for (var p = 0; p < measures.length; p++) {
                    measures[p].paddingLeft = measure.paddingLeft;
                }
            }

            if (drawTime) {
                if (maxKeyX <= 0) {
                    maxKeyX = measure.x;
                }

                for (var j = 0; j < measures.length; j++) {
                    self.drawTime(measures[j], maxKeyX);
                }
            }
        }
    };

    self.drawMeasure = function (measure) {

        var noteHeadHeight = self.piece.noteHeadHeight;
        var keyFinalX = 0;

        for (var i = 0; i < measure.part.numberStaves; i++) {
            var clef = measure.attributes.clef[i];
            var clefX = measure.x + self.piece.convertTenthsToPixals(12);
            var centerStaffY = measure.staffBottomYs[i] - ((measure.staffBottomYs[i] - measure.staffTopYs[i]) * 0.5);
            var topY = measure.staffTopYs[i];

            var signWidth = 0;
            switch (clef.sign) {
                case "G":
                    var trebleClefFont = self.piece.fontCharCodes.trebleClef;
                    if (clef.octaveChange == -1)
                        trebleClefFont = self.piece.fontCharCodes.trebleClefOctaveLower;
                    if (clef.octaveChange == 1)
                        trebleClefFont = self.piece.fontCharCodes.trebleClefOctaveHigher;

                    ctx.fillText(trebleClefFont, clefX, (topY + (noteHeadHeight * 3)));
                    signWidth = ctx.measureText(self.piece.fontCharCodes.trebleClef).width;
                    break;
                case "F":
                    var bassClef = self.piece.fontCharCodes.bassClef;
                    if (clef.octaveChange == -1)
                        bassClef = self.piece.fontCharCodes.bassClefOctaveLower;
                    if (clef.octaveChange == 1)
                        bassClef = self.piece.fontCharCodes.bassClefOctaveHigher;
                    ctx.fillText(bassClef, clefX, (topY + noteHeadHeight));
                    signWidth = ctx.measureText(self.piece.fontCharCodes.bassClef).width;
                    break;
                case "C":
                    ctx.fillText(self.piece.fontCharCodes.altoClef, clefX, centerStaffY);
                    signWidth = ctx.measureText(self.piece.fontCharCodes.altoClef).width;
                    break;
            }

            keyFinalX = clefX + signWidth;
        }

        return keyFinalX;
    };

    self.drawKey = function (measure, keyY) {
        var noteHeadHeight = self.piece.noteHeadHeight;
        var keyFinalX = keyY;
        var paddingBetweenAccidentals = self.piece.convertTenthsToPixals(2);

        for (var i = 0; i < measure.part.numberStaves; i++) {

            //draw key *************************************************
            var keyFifths = measure.attributes.key.fifths;
            var centerStaffY = measure.staffBottomYs[i] - ((measure.staffBottomYs[i] - measure.staffTopYs[i]) * 0.5);
            var clef = measure.attributes.clef[i];

            var topFlatY = centerStaffY;
            var topSharpY = measure.staffTopYs[i];

            var isSharpKey = (keyFifths > 0);

            keyFinalX = keyY;


            //draw cautionary naturals 
            if (measure.previousKey != undefined && measure.previousKey.fifths != measure.attributes.key.fifths && measure.previousKey.fifths != 0) {
                var topX = keyFinalX;
                var topY = (measure.previousKey.fifths > 0) ? topSharpY : topFlatY;
                var numberNaturals = (measure.previousKey.fifths < 0) ? measure.previousKey.fifths * -1 : measure.previousKey.fifths;
                var naturalWidth = ctx.measureText(self.piece.fontCharCodes.natural).width;


                if (measure.previousKey.fifths > 0) {
                    if (clef.sign == "F")
                        topY += noteHeadHeight;
                    else if (clef.sign == "C")
                        topY += (noteHeadHeight * 0.5);
                    for (var j = 0; j < numberNaturals; j++) {
                        var currentKeySharpIndex = j;
                        if (currentKeySharpIndex >= measure.attributes.key.fifths) {
                            var naturalY = topY;
                            switch (j) {
                                case 1:
                                    naturalY += (noteHeadHeight * 1.5);
                                    break;
                                case 2:
                                    naturalY -= (noteHeadHeight * 0.5);
                                    break;
                                case 3:
                                    naturalY += noteHeadHeight;
                                    break;
                                case 4:
                                    naturalY += (noteHeadHeight * 2.5);
                                    break;
                                case 5:
                                    naturalY += (noteHeadHeight * 0.5);
                                    break;
                                case 6:
                                    naturalY += (noteHeadHeight * 2);
                                    break;
                            }

                            keyFinalX += naturalWidth + paddingBetweenAccidentals;
                            ctx.fillText(self.piece.fontCharCodes.natural, keyFinalX, naturalY);
                        }
                    }
                } else {
                    if (clef.sign == "F")
                        topY += noteHeadHeight;
                    else if (clef.sign == "C")
                        topY += (noteHeadHeight * 0.5);
                    for (var k = 0; k < numberNaturals; k++) {
                        var naturalY = topY;
                        var currentKeyFlatIndex = (k * -1) - 1;
                        if (currentKeyFlatIndex < measure.attributes.key.fifths) {
                            if (k % 2 != 0) {
                                naturalY -= (noteHeadHeight * 1.5) + ((noteHeadHeight * 0.5) * 0.5);
                                naturalY += ((noteHeadHeight * 0.5) * (k * 0.5));
                            } else {
                                naturalY += ((noteHeadHeight * 0.5) * (k * 0.5));
                            }

                            keyFinalX += naturalWidth + paddingBetweenAccidentals;

                            ctx.fillText(self.piece.fontCharCodes.natural, keyFinalX, naturalY);
                        }

                    }
                }

            }

            //sharp key
            if (keyFifths > 0) {
                isSharpKey = true;
                var numberOfSharps = keyFifths;
                var keyX = keyFinalX;
                var keyWidth = ctx.measureText(self.piece.fontCharCodes.sharp).width;

                if (clef.sign == "F")
                    topSharpY += noteHeadHeight;
                else if (clef.sign == "C")
                    topSharpY += (noteHeadHeight * 0.5);

                for (var j = 0; j < numberOfSharps; j++) {
                    var sharpY = topSharpY;

                    switch (j) {
                        case 1:
                            sharpY += (noteHeadHeight * 1.5);
                            break;
                        case 2:
                            sharpY -= (noteHeadHeight * 0.5);
                            break;
                        case 3:
                            sharpY += noteHeadHeight;
                            break;
                        case 4:
                            sharpY += (noteHeadHeight * 2.5);
                            break;
                        case 5:
                            sharpY += (noteHeadHeight * 0.5);
                            break;
                        case 6:
                            sharpY += (noteHeadHeight * 2);
                            break;
                    }

                    keyFinalX += keyWidth + paddingBetweenAccidentals;

                    ctx.fillText(self.piece.fontCharCodes.sharp, keyFinalX, sharpY);
                }
            }
                //flat key
            else if (keyFifths < 0) {
                var numberOfFlats = keyFifths * -1;
                var keyX = keyFinalX;
                var keyWidth = ctx.measureText(self.piece.fontCharCodes.flat).width;

                if (clef.sign == "F")
                    topFlatY += noteHeadHeight;
                else if (clef.sign == "C")
                    topFlatY += (noteHeadHeight * 0.5);

                for (var j = 0; j < numberOfFlats; j++) {
                    var flatY = topFlatY;

                    if (j % 2 != 0) {
                        flatY -= (noteHeadHeight * 1.5) + ((noteHeadHeight * 0.5) * 0.5);
                        flatY += ((noteHeadHeight * 0.5) * (j * 0.5));
                    }
                    else {
                        flatY += ((noteHeadHeight * 0.5) * (j * 0.5));
                    }

                    keyFinalX += keyWidth + paddingBetweenAccidentals;
                    ctx.fillText(self.piece.fontCharCodes.flat, keyFinalX, flatY);
                }
            }
        }

        keyFinalX += self.piece.convertTenthsToPixals(10);
        measure.paddingLeft = (keyFinalX - measure.x) + self.piece.convertTenthsToPixals(10);
        return keyFinalX;
    };

    self.drawTime = function (measure, keyY) {
        var noteHeadHeight = self.piece.noteHeadHeight;
        var keyFinalX = keyY;

        for (var i = 0; i < measure.part.numberStaves; i++) {

            if (measure.attributes != null && measure.attributes.time != null) {
                var beats = measure.attributes.time.beats;
                var beatType = measure.attributes.time.type;
                var beatX = keyY + self.piece.convertTenthsToPixals(10);

                if (beats == 2 && beatType == 2) {
                    ctx.fillText(self.piece.fontCharCodes.cutTime, beatX, measure.staffMiddleYs[i]);
                    keyFinalX += ctx.measureText(self.piece.fontCharCodes.cutTime).width;
                }
                else {
                    ctx.fillText(beats, beatX, (measure.staffTopYs[i] + (noteHeadHeight * 1)));
                    ctx.fillText(beatType, beatX, (measure.staffTopYs[i] + (noteHeadHeight * 3)));
                    keyFinalX += ctx.measureText(beats).width;
                }
            }
        }

        measure.paddingLeft = (keyFinalX - measure.x) + self.piece.convertTenthsToPixals(10);
    };
};