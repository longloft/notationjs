﻿var NotationJS = NotationJS || {};

NotationJS.BracketsRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.piece = null;

    self.draw = function (piece, page, part) {

        self.piece = piece;

        var firstMeasure = true;

        for (var i = 0; i < page.measures.length; i++) {
            var partMeasuresForMeasure = page.measures[i];

            if (partMeasuresForMeasure != undefined) {
                //find any braces
                for (var x = 0; x < partMeasuresForMeasure.length; x++) {
                    var measure = partMeasuresForMeasure[x];

                    if (measure != undefined && (firstMeasure || measure.newLine)) {
                        //draw braces
                        if (measure.part.numberStaves > 1) {
                            piece.fontCharCodes.brace((measure.x - piece.convertTenthsToPixals(6)), measure.y, measure.bottomY, ctx, piece.convertTenthsToPixals);
                        }

                        //print names
                        if (measure.part.printName) {
                            var labelText = measure.part.abbreviation || measure.part.name;

                            if (measure.number == 1) {
                                labelText = measure.part.name;
                            }

                            var originalTextBaseline = ctx.textBaseline;
                            var originalTextAlign = ctx.textAlign;
                            ctx.textAlign = "right";
                            ctx.textBaseline = "middle";
                            ctx.font = 'normal ' + piece.wordFontPoint.fontSize + 'pt Times New Roman';
                            ctx.fillText(labelText, (measure.x - piece.convertTenthsToPixals(30)), (measure.y + ((measure.bottomY - measure.y) * 0.5)));
                            ctx.textAlign = originalTextAlign;
                            ctx.textBaseline = originalTextBaseline;
                        }
                    }
                }

                //see if we need to print brackets
                var measure = partMeasuresForMeasure[0];

                if (measure != undefined && (firstMeasure || measure.newLine)) {
                    self.drawBrackets(piece, page, part, partMeasuresForMeasure, measure);
                }

                firstMeasure = false;
            }

        }
    };

    self.drawBrackets = function (piece, page, part, partMeasuresForMeasure, measure) {
        self.piece = piece;

        var bracketRenderer = new NotationJS.BracketRenderer(canvas, ctx);
        bracketRenderer.piece = piece;

        var topGroupMeasures = [];

        for (var i = 0; i < partMeasuresForMeasure.length; i++) {
            var measure = partMeasuresForMeasure[i];
            var nextMeasure = ((i + 1) < partMeasuresForMeasure.length) ? partMeasuresForMeasure[(i + 1)] : null;

            if (measure != null && measure != undefined) {
                for (var x = 0; x < measure.part.groups.length; x++) {
                    var group = measure.part.groups[x];

                    if (topGroupMeasures[group.number] == undefined) {
                        topGroupMeasures[group.number] = { measure: measure, group: group };
                    }
                }
            }

            if (nextMeasure != null && nextMeasure != undefined) {
                for (var x = 0; x < topGroupMeasures.length; x++) {

                    //see if group is in next measure
                    if (topGroupMeasures[x] != undefined) {
                        var foundGroup = false;

                        for (var z = 0; z < nextMeasure.part.groups.length; z++) {
                            var group = nextMeasure.part.groups[z];
                            if (group != undefined && group == topGroupMeasures[x].group) {
                                foundGroup = true;
                                break;
                            }
                        }

                        if (!foundGroup) {
                            var group = topGroupMeasures[x].group;
                            var topMeasure = topGroupMeasures[x].measure;
                            var bottomMeasure = measure;

                            //print 
                            bracketRenderer.draw((topMeasure.x + group.defaultX), topMeasure.y, bottomMeasure.bottomY, group.symbol);

                            topGroupMeasures[x] = undefined;
                        }
                    }

                }
            }
            else {
                //print last grouping if need be
                for (var x = 0; x < topGroupMeasures.length; x++) {
                    if (topGroupMeasures[x] != undefined) {
                        var group = topGroupMeasures[x].group;
                        var topMeasure = topGroupMeasures[x].measure;
                        var bottomMeasure = measure;

                        //print 
                        bracketRenderer.draw((topMeasure.x + group.defaultX), topMeasure.y, bottomMeasure.bottomY, group.symbol);

                        topGroupMeasures[x] = undefined;
                    }
                }
            }
        }
    };
};