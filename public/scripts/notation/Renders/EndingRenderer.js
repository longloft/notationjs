﻿var NotationJS = NotationJS || {};

NotationJS.EndingRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (ending) {
        if (!ending.drawEnding)
            return;

        var startX = ending.startMeasure.x + ending.startMeasure.paddingLeft;

        //draw top line
        ctx.beginPath();
        ctx.moveTo(startX, (ending.startMeasure.y - ending.defaultY));
        ctx.lineTo(((ending.endMeasure.x + ending.endMeasure.width) - ending.piece.convertTenthsToPixals(5)), (ending.startMeasure.y - ending.defaultY));
        ctx.stroke();
        ctx.closePath();

        //draw job
        ctx.beginPath();
        ctx.moveTo(startX, (ending.startMeasure.y - ending.defaultY));
        ctx.lineTo(startX, ((ending.startMeasure.y - ending.defaultY) + ending.jogLength));
        ctx.stroke();
        ctx.closePath();

        if (!ending.isDiscontinued) {
            ctx.beginPath();
            ctx.moveTo(((ending.endMeasure.x + ending.endMeasure.width) - ending.piece.convertTenthsToPixals(5)), (ending.startMeasure.y - ending.defaultY));
            ctx.lineTo(((ending.endMeasure.x + ending.endMeasure.width) - ending.piece.convertTenthsToPixals(5)), ((ending.startMeasure.y - ending.defaultY) + ending.jogLength));
            ctx.stroke();
            ctx.closePath();
        }

        //draw text
        self.pieceRenderer.setWordFont(ending.fontSize);

        ctx.textBaseline = "top";
        var cornerPadding = ending.piece.convertTenthsToPixals(5);
        ctx.fillText(ending.number + ".", startX + cornerPadding, ((ending.startMeasure.y - ending.defaultY) + cornerPadding));

    };
};