﻿var NotationJS = NotationJS || {};

NotationJS.BarlineRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.piece = null;
    self.part = null;

    self.draw = function (measure) {

        //always draw right barline
        self.drawRightBarline((measure.x + measure.width), measure.y, measure.bottomY, (measure.barline != undefined && measure.barline.location != "left") ? measure.barline : null);

        if (measure.barline != undefined && measure.barline.location == "left") {
            self.drawLeftBarline(measure.x + measure.paddingLeft, measure.y, measure.bottomY, measure.barline);
        }

        if (measure.barline != undefined && measure.barline.isRepeat) {
            self.drawRepeatDots(measure);
        }
    };

    self.drawBarline = function (barx, barTopY, barBottomY, barline, measure, groupMeasures) {
        self.drawRightBarline((measure.x + measure.width), barTopY, barBottomY, (measure.barline != undefined && measure.barline.location != "left") ? measure.barline : null);

        if (measure.barline != undefined && measure.barline.location == "left") {
            self.drawLeftBarline(measure.x + measure.paddingLeft, barTopY, barBottomY, measure.barline);
        }

        if (measure.barline != undefined && measure.barline.isRepeat) {
            if (groupMeasures != undefined && groupMeasures.length > 0) {
                for (var i = 0; i < groupMeasures.length; i++) {
                    self.drawRepeatDots(groupMeasures[i]);
                }
            } else {
                self.drawRepeatDots(measure);
            }
        }
    };

    self.drawRightBarline = function (barx, barTopY, barBottomY, barline) {
        var type = "normal";

        if (barline != null && barline != undefined) {
            type = barline.style;
        }

        switch (type) {
            case "light-heavy":
                ctx.beginPath();
                ctx.moveTo(barx - (self.piece.heavyBarlineWidth * 2), barTopY);
                ctx.lineTo(barx - (self.piece.heavyBarlineWidth * 2), barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - self.piece.heavyBarlineWidth, barTopY, self.piece.heavyBarlineWidth, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy-light":
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - (self.piece.heavyBarlineWidth * 2), barTopY, self.piece.heavyBarlineWidth, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy-heavy":
                ctx.beginPath();
                ctx.rect(barx - 2, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - 8, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "light-light":
                ctx.beginPath();
                ctx.moveTo(barx - self.piece.heavyBarlineWidth, barTopY);
                ctx.lineTo(barx - self.piece.heavyBarlineWidth, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy":
                ctx.beginPath();
                ctx.rect(barx - self.piece.heavyBarlineWidth, barTopY, self.piece.heavyBarlineWidth, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            default:
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineCap = 'square';
                ctx.lineJoin = 'miter';
                ctx.stroke();
                ctx.closePath();
                break;
        }

    };

    self.drawLeftBarline = function (barx, barTopY, barBottomY, barline) {
        var type = "normal";

        if (barline != null && barline != undefined) {
            type = barline.style;
        }

        switch (type) {
            case "light-heavy":
                ctx.beginPath();
                ctx.moveTo(barx + (self.piece.heavyBarlineWidth * 2), barTopY);
                ctx.lineTo(barx + (self.piece.heavyBarlineWidth * 2), barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx, barTopY, barx + self.piece.heavyBarlineWidth, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy-light":
                ctx.beginPath();
                ctx.moveTo(barx + (self.piece.heavyBarlineWidth * 2), barTopY);
                ctx.lineTo(barx + (self.piece.heavyBarlineWidth * 2), barBottomY);
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx, barTopY, self.piece.heavyBarlineWidth, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy-heavy":
                ctx.beginPath();
                ctx.rect(barx + 2, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - 8, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "light-light":
                ctx.beginPath();
                ctx.moveTo(barx + self.piece.heavyBarlineWidth, barTopY);
                ctx.lineTo(barx + self.piece.heavyBarlineWidth, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy":
                ctx.beginPath();
                ctx.rect(barx + self.piece.heavyBarlineWidth, barTopY, self.piece.heavyBarlineWidth, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.lineCap = 'square';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            default:
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineCap = 'square';
                ctx.lineJoin = 'miter';
                ctx.stroke();
                ctx.closePath();
                break;
        }
    };

    self.drawRepeatDots = function (measure) {
        for (var i = 0; i < measure.part.numberStaves; i++) {
            var topY = measure.staffTopYs[i];
            var middleY = measure.staffMiddleYs[i];
            var bottomY = measure.staffBottomYs[i];
            var topMiddleY = measure.staffTopMiddleYs[i];
            var bottomMiddleY = measure.staffBottomMiddleYs[i];

            var repeatX = measure.x + measure.paddingLeft + (self.piece.heavyBarlineWidth * 4);

            if (measure.barline.location == "right") {
                repeatX = (measure.x + measure.paddingLeft + measure.width) - (self.piece.heavyBarlineWidth * 4);
            }

            var centerY = (topMiddleY + ((middleY - topMiddleY) * 0.5));
            ctx.beginPath();
            ctx.arc(repeatX, centerY, self.piece.convertTenthsToPixals(2), 0, 2 * Math.PI, false);
            ctx.fillStyle = 'black';
            ctx.fill();
            ctx.closePath();

            centerY = (middleY + ((bottomMiddleY - middleY) * 0.5));
            ctx.beginPath();
            ctx.arc(repeatX, centerY, self.piece.convertTenthsToPixals(2), 0, 2 * Math.PI, false);
            ctx.fillStyle = 'black';
            ctx.fill();
            ctx.closePath();
        }
    };
};