﻿var NotationJS = NotationJS || {};

NotationJS.TieRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.piece = null;

    self.draw = function (note) {

        var tiedNote = note.tiedToNote;

        if (note.tiedToNote != null) {
            self.drawTieBetweenNotes(note, tiedNote);
        }

        if (note.tieBack) {
            self.drawTieBack(note);
        }
    };

    self.drawTieBetweenNotes = function (note, tiedNote) {
        var padding = note.piece.noteHeadWidth * 0.5;
        var x0 = note.x + note.piece.noteHeadWidth + padding;
        var y0 = note.y;
        var x1 = tiedNote.x - padding;
        var y1 = tiedNote.y;
        var bow = note.piece.noteHeadHeight;
        var downward = false;

        if (note.y > note.measure.staffMiddleYs[(note.staff - 1)])
            downward = true;

        if (note.chord != null) {
            downward = (note.chord.getTopNote() != note);
        }

        if (note.measure.page != tiedNote.measure.page || (tiedNote.measure.newLine && !note.measure.newLine)) {
            x1 = note.measure.x + note.measure.width;
            y1 = y0;
        }

        var sign = (downward ? 1 : -1);

        var thickness = sign * 2;

        var deltaX = x1 - x0;
        var deltaY = y1 - y0;

        if (deltaX == 0)
            deltaX = 1;
        var ratio = 0.25;

        var maxBow = deltaX * 0.4;

        if (bow > maxBow && deltaY < 2 * 10) {

            bow = maxBow;
        }

        bow *= sign;

        var slope = deltaY / deltaX;

        var cp1X = x0 + deltaX * ratio;
        var cp1Y = y0 + (cp1X - x0) * slope + bow;

        var cp2X = x1 - deltaX * ratio;
        var cp2Y = y0 + (cp2X - x0) * slope + bow;

        ctx.beginPath();
        ctx.moveTo(x0, y0);
        ctx.bezierCurveTo(cp1X, cp1Y, cp2X, cp2Y, x1, y1);

        cp1Y += thickness;
        cp2Y += thickness;

        ctx.bezierCurveTo(cp2X, cp2Y, cp1X, cp1Y, x0, y0);
        ctx.fill();
        ctx.closePath();
    };

    self.drawTieBack = function (note) {
        var padding = note.piece.noteHeadWidth * 0.5;
       
        var x0 = note.measure.x + note.measure.paddingLeft - note.piece.convertTenthsToPixals(10);
        var y0 = note.y;
        var x1 = note.x - padding;
        var y1 = note.y;
        var bow = note.piece.noteHeadHeight;
        var downward = false;

        if (note.y > note.measure.staffMiddleYs[(note.staff - 1)])
            downward = true;

        if (note.chord != null) {
            downward = (note.chord.getTopNote() != note);
        }

        var sign = (downward ? 1 : -1);

        var thickness = sign * 2;

        var deltaX = x1 - x0;
        var deltaY = y1 - y0;

        if (deltaX == 0)
            deltaX = 1;
        var ratio = 0.25;

        var maxBow = deltaX * 0.4;

        if (bow > maxBow && deltaY < 2 * 10) {

            bow = maxBow;
        }

        bow *= sign;

        var slope = deltaY / deltaX;

        var cp1X = x0 + deltaX * ratio;
        var cp1Y = y0 + (cp1X - x0) * slope + bow;

        var cp2X = x1 - deltaX * ratio;
        var cp2Y = y0 + (cp2X - x0) * slope + bow;

        ctx.beginPath();
        ctx.moveTo(x0, y0);
        ctx.bezierCurveTo(cp1X, cp1Y, cp2X, cp2Y, x1, y1);

        cp1Y += thickness;
        cp2Y += thickness;

        ctx.bezierCurveTo(cp2X, cp2Y, cp1X, cp1Y, x0, y0);
        ctx.fill();
        ctx.closePath();
    };
};