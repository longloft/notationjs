﻿var NotationJS = NotationJS || {};

NotationJS.CreditsRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;
    self.pageRenderer = null;

    self.draw = function (credits) {

        for (var i = 0; i < credits.length; i++) {
            var credit = credits[i];

            ctx.font = credit.fontWeight + ' ' + credit.fontSize + 'pt ' + credit.font;

            if (credit.fontStyle != undefined)
                ctx.font = credit.fontStyle + " " + ctx.font;

            ctx.textAlign = "start";

            ctx.textBaseline = (credit.valign != undefined) ? credit.valign : "alphabetic";
            ctx.textAlign = (credit.halign != undefined) ? credit.halign : ctx.textAlign;

            //ctx.beginPath();
            //ctx.moveTo(credit.x, credit.y);
            //ctx.lineTo(credit.x, credit.y + 20);
            //ctx.stroke();
            //ctx.closePath();

            //ctx.beginPath();
            //ctx.moveTo(credit.x, credit.y);
            //ctx.lineTo(credit.width, credit.y);
            //ctx.stroke();
            //ctx.closePath();

            ctx.fillText(credit.text, credit.x, credit.y);
        }
    };
};