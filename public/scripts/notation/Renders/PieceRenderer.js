﻿var NotationJS = NotationJS || {};

NotationJS.PieceRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.piece = null;
    self.noteStaffPoint = null;
    self.fontCharCodes = null;

    self.draw = function (piece, page) {

        var cc = ctx.currentScale;

        //get line width converted.
        var lw = piece.convertTenthsToPixals(1);

        //set scaled line width
        ctx.lineWidth = lw;

        //adjust it for scaling so it looks better zoomed in.
        //if (cc != undefined && cc < 1)
        //{
        //    var newLineWidth = 1 + (cc / 1.5);
        //    ctx.lineWidth = (newLineWidth > 0.2) ? newLineWidth : 0.2;
        //    if (ctx.lineWidth > lw)
        //        ctx.lineWidth = lw;
        //    //console.log("lineWidth=" + ctx.lineWidth + " currentScale=" + cc + " lw=" + lw);
        //}

        self.piece = piece;

        var pageRenderer = new NotationJS.PageRenderer(canvas, ctx);
        pageRenderer.pieceRenderer = self;

        pageRenderer.draw(piece, page);
    };

    //sets the music font for the canvas
    self.setMusicFont = function (smallerSize) {
        var size = (smallerSize) ? (self.piece.musicFontPoint.fontSize - 2) : self.piece.musicFontPoint.fontSize;
        if (smallerSize)
        {
            size = (self.piece.musicFontPoint.fontSize - 8);
        }
        var noteFont = "normal " + size + "pt MaestroNotes";
        ctx.textBaseline = "alphabetic";
        //set note font for sizing
        ctx.textAlign = "start";
        ctx.font = noteFont;
    };

    //sets the music font for the canvas
    self.setWordFont = function (overrideSize) {

        var size = overrideSize || self.piece.wordFontPoint.fontSize;
        ctx.textBaseline = "alphabetic";
        ctx.textAlign = "start";
        ctx.font = 'normal ' + size + 'pt ' + self.piece.wordFontPoint.fontFamily;
    };
};