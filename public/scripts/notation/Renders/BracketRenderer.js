﻿var NotationJS = NotationJS || {};

NotationJS.BracketRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.piece = null;

    self.draw = function (barx, barTopY, barBottomY, type) {

        switch (type) {
            case "square":
                var indentX = self.piece.convertTenthsToPixals(10);
                ctx.beginPath();
                ctx.moveTo(barx + indentX, barTopY);
                ctx.lineTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineTo(barx + indentX, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();
                break;

            case "bracket":
                var thickness = self.piece.convertTenthsToPixals(4);
                var swigleInsetCurveWidth = self.piece.convertTenthsToPixals(14);
                var swigleInsetWidth = self.piece.convertTenthsToPixals(18);
                var leftX = barx - self.piece.convertTenthsToPixals(2);
                var rightX = barx + self.piece.convertTenthsToPixals(2)
                ctx.beginPath();
                ctx.moveTo(leftX, (barTopY - 2));
                ctx.lineTo(leftX, (barBottomY + 3));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barBottomY + 2), (barx + swigleInsetWidth), (barBottomY + 5));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barBottomY + 2), leftX, (barBottomY + 2));
                ctx.lineTo(rightX, (barBottomY + 2));
                ctx.lineTo(rightX, (barTopY - 2));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barTopY - 2), (barx + swigleInsetWidth), (barTopY - 5));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barTopY - 2), leftX, (barTopY - 3));
                ctx.lineTo(leftX, (barTopY - 2));
                ctx.fillStyle = '#000000';
                ctx.fill();
                ctx.closePath();
                break;

            default:
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();
                break;
        }

    };
};