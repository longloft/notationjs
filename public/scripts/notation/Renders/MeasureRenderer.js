﻿var NotationJS = NotationJS || {};

NotationJS.MeasureRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;
    self.pieceRenderer = null;

    self.draw = function (piece, page, part, measure) {

        var firstStaff = true;
        var topStaffDistance = measure.y;
        var bottomY = 0;
        var topYArray = [];
        var bottomYArray = [];
        for (var i = 0; i < part.numberStaves; i++) {
            if (!firstStaff) {
                topStaffDistance += (piece.staffHeight + (measure.staffDistances[(i + 1)] || page.staffDistance));
            }

            firstStaff = false;
            bottomY = self.drawStaffLines(measure.width, measure.x, topStaffDistance, piece.noteHeadHeight, piece.staffHeight);
            bottomYArray.push(bottomY);
            topYArray.push(topStaffDistance);
        }

        //draw dynamics
        var dynamicRenderer = new NotationJS.DynamicRenderer(canvas, ctx);
        dynamicRenderer.pieceRenderer = self.pieceRenderer;

        for (var j = 0; j < measure.dynamics.length; j++) {
            dynamicRenderer.draw(measure.dynamics[j]);
        }
    };

    self.drawStaffLines = function (measureWidth, leftMargin, topStaffDistance, noteHeadHeight, staffHeight) {

        for (var s = 0; s < 5; s++) {
            ctx.beginPath();
            ctx.moveTo(leftMargin, topStaffDistance + (noteHeadHeight * s));
            ctx.lineTo(leftMargin + measureWidth, topStaffDistance + (noteHeadHeight * s));
            ctx.stroke();
            ctx.closePath();
        }

        return topStaffDistance + staffHeight;
    };
};