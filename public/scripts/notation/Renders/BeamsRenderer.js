﻿var NotationJS = NotationJS || {};

NotationJS.BeamsRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (beams) {

        for (var b = 0; b < beams.length; b++) {
            var beam = beams[b];

            if (beam.startNote == null || beam.endNote == null) {
                break;
            }

            var hookBeam = null;
            //if is hook, then we want to use the reference beam for stem measurements.
            if (beam.isHook) {
                hookBeam = beam;
                beam = hookBeam.hookReferenceBeam;
            }

            if (beam == null)
                return;

            var stemRenderer = new NotationJS.StemRenderer();

            var startNoteCords = stemRenderer.getStemCordsForNote(
                (beam.startNote.chord && beam.startNote.chord.stemNote) ? beam.startNote.chord.stemNote : beam.startNote,
                (beam.startNote.chord && beam.startNote.chord.initialNote) ? beam.startNote.chord.initialNote.stemY : null);
            var endNoteCords = stemRenderer.getStemCordsForNote(
                (beam.endNote.chord && beam.endNote.chord.stemNote) ? beam.endNote.chord.stemNote : beam.endNote,
                (beam.endNote.chord && beam.endNote.chord.initialNote) ? beam.endNote.chord.initialNote.stemY : null);


            ctx.beginPath();
            ctx.fillStyle = 'black';
            var beamHeight = beam.piece.convertTenthsToPixals(6);
            var beamPadding = beam.piece.convertTenthsToPixals(8);


            if (hookBeam != null) {
                var beamIndex = hookBeam.number - 1;

                var slope = (endNoteCords.stemY - startNoteCords.stemY) / (endNoteCords.stemX - startNoteCords.stemX);
                var startX = (hookBeam.type == "backward hook") ? endNoteCords.stemX - beam.piece.convertTenthsToPixals(10) : startNoteCords.stemX;
                var startY = (hookBeam.type == "backward hook") ? (slope * (startX - endNoteCords.stemX)) + endNoteCords.stemY : startNoteCords.stemY;
                var endX = (hookBeam.type == "forward hook") ? startNoteCords.stemX + beam.piece.convertTenthsToPixals(10) : endNoteCords.stemX;
                var endY = (hookBeam.type == "forward hook") ? (slope * (endX - startNoteCords.stemX)) + startNoteCords.stemY : endNoteCords.stemY;

                if (beam.startNote.stemDirection == "down") {

                    ctx.moveTo(startX, (startY - (beamPadding * beamIndex)));
                    ctx.lineTo(endX, (endY - (beamPadding * beamIndex)));
                    ctx.lineTo(endX, ((endY - beamHeight) - (beamPadding * beamIndex)));
                    ctx.lineTo(startX, (startY - beamHeight) - (beamPadding * beamIndex));

                }
                else {
                    ctx.moveTo(startX, (startY + (beamPadding * beamIndex)));
                    ctx.lineTo(endX, ((endY) + (beamPadding * beamIndex)));
                    ctx.lineTo(endX, (((endY) + beamHeight) + (beamPadding * beamIndex)));
                    ctx.lineTo(startX, ((startY + beamHeight)) + (beamPadding * beamIndex));
                }
            }
            else {
                var beamIndex = beam.number - 1;

                if (beam.startNote.stemDirection == "down") {

                    ctx.moveTo(startNoteCords.stemX, (startNoteCords.stemY - (beamPadding * beamIndex)));
                    ctx.lineTo(endNoteCords.stemX, (endNoteCords.stemY - (beamPadding * beamIndex)));
                    ctx.lineTo(endNoteCords.stemX, ((endNoteCords.stemY - beamHeight) - (beamPadding * beamIndex)));
                    ctx.lineTo(startNoteCords.stemX, (startNoteCords.stemY - beamHeight) - (beamPadding * beamIndex));

                }
                else {
                    ctx.moveTo(startNoteCords.stemX, (startNoteCords.stemY + (beamPadding * beamIndex)));
                    ctx.lineTo(endNoteCords.stemX, ((endNoteCords.stemY) + (beamPadding * beamIndex)));
                    ctx.lineTo(endNoteCords.stemX, (((endNoteCords.stemY) + beamHeight) + (beamPadding * beamIndex)));
                    ctx.lineTo(startNoteCords.stemX, ((startNoteCords.stemY + beamHeight)) + (beamPadding * beamIndex));
                }
            }
            ctx.fill();
            ctx.closePath();
        }

    };
};