﻿var NotationJS = NotationJS || {};

NotationJS.NoteRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;
    self.pieceRenderer = null;

    self.draw = function (note) {

        //don't do anything if part of multimeasure rest
        if (note.isPartOfMultiMeasureRest)
            return;


        self.pieceRenderer.setMusicFont(note.isGrace);

        ctx.fillText(note.charCode, note.x, note.y);



        if (note.isDot)
            drawDotForNote(note.x, note.y, note);

        if (!note.isRest)
            drawLineThroughNote(note.x, note.y, note);

        if (note.accidental != undefined)
            drawAccidental(note.x, note.y, note);

        var needsCustomStem = (note.stemDefaultY != null && !note.partOfChord && note.chord == null);

        //draw stem if needed
        //create stems if needed
        if (needsCustomStem) {
            var stemRenderer = new NotationJS.StemRenderer(canvas, ctx);
            stemRenderer.pieceRenderer = self.pieceRenderer;
            stemRenderer.drawStemForNote(note);
        }

        if (note.fermata != null)
        {
            var fermataRenderer = new NotationJS.FermataRenderer(canvas, ctx);
            fermataRenderer.pieceRenderer = self.pieceRenderer;
            fermataRenderer.draw(note.fermata);
        }

        if (note.beams != null && note.beams.length > 0) {
            var beamsRenderer = new NotationJS.BeamsRenderer(canvas, ctx);
            beamsRenderer.pieceRenderer = self.pieceRenderer;
            beamsRenderer.draw(note.beams);
        }

        if (note.articulations != null && note.articulations.length > 0) {
            var artRenderer = new NotationJS.ArticulationRenderer(canvas, ctx);
            artRenderer.pieceRenderer = self.pieceRenderer;

            for (var na = 0; na < note.articulations.length; na++) {
                artRenderer.draw(note.articulations[na]);
            }
        }
    };

    var drawDotForNote = function (noteX, noteY, note) {
        //note, clef, noteHeadWidth, noteHeadHeight
        var dotX = noteX + (note.piece.noteHeadWidth + note.piece.convertTenthsToPixals(6));
        var dotY = noteY;
        var clef = note.measure.attributes.clef[(note.staff - 1)];
        if (!note.isRest) {
            switch (clef.sign) {
                //treble cleff
                case "G":
                    if ((note.pitch.octave == 5 && note.pitch.step == "F") ||
                        (note.pitch.octave == 5 && note.pitch.step == "D") ||
                        (note.pitch.octave == 4 && note.pitch.step == "B") ||
                        (note.pitch.octave == 4 && note.pitch.step == "G") ||
                        (note.pitch.octave == 4 && note.pitch.step == "E") ||
                        note.isRest
                        ) {
                        dotY -= (note.piecenoteHeadHeight * 0.5);
                    }
                    break;
                    //bass cleff
                case "F":
                    if ((note.pitch.octave == 2 && note.pitch.step == "B") ||
                        (note.pitch.octave == 2 && note.pitch.step == "G") ||
                        (note.pitch.octave == 3 && note.pitch.step == "F") ||
                        (note.pitch.octave == 3 && note.pitch.step == "D") ||
                        (note.pitch.octave == 3 && note.pitch.step == "A") ||
                        note.isRest
                        ) {
                        dotY -= (note.piecenoteHeadHeight * 0.5);
                    }
                    break;
            }
            ctx.fillText(note.piece.fontCharCodes.dot, dotX, dotY);
        }

        //TODO: dotted rests
    };

    var drawAccidental = function (noteX, noteY, note) {
        var char = null;
        var offset = 0;

        switch (note.accidental) {
            case "flat":
                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.flat : note.piece.fontCharCodes.flatWithParentheses;
                offset = (!note.accidentalInParentheses) ? note.piece.convertTenthsToPixals(10) : note.piece.convertTenthsToPixals(20);
                ctx.fillText(char, (noteX - offset), (noteY));
                break;
            case "sharp":
                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.sharp : note.piece.fontCharCodes.sharpWithParentheses;
                offset = (!note.accidentalInParentheses) ? note.piece.convertTenthsToPixals(10) : note.piece.convertTenthsToPixals(21);
                ctx.fillText(char, (noteX - offset), (noteY));
                break;
            case "natural":
                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.natural : note.piece.fontCharCodes.nateralWithParentheses;
                offset = (!note.accidentalInParentheses) ? note.piece.convertTenthsToPixals(10) : note.piece.convertTenthsToPixals(20);
                ctx.fillText(char, (noteX - offset), (noteY));
                break;
            case "double-sharp":
                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.doubleSharp : note.piece.fontCharCodes.doubleSharpWithParentheses;
                offset = note.piece.convertTenthsToPixals(12);
                ctx.fillText(char, (noteX - offset), (noteY));
                break;
            case "flat-flat":
                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.doubleFlat : note.piece.fontCharCodes.doubleFlatWithParentheses;
                offset = (!note.accidentalInParentheses) ? note.piece.convertTenthsToPixals(18) : note.piece.convertTenthsToPixals(24);
                ctx.fillText(char, (noteX - offset), (noteY));
                break;
            case "triple-sharp":

                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.doubleSharp : note.piece.fontCharCodes.doubleSharpWithParentheses;
                offset = (!note.accidentalInParentheses) ? note.piece.convertTenthsToPixals(12) : note.piece.convertTenthsToPixals(20);

                ctx.fillText(char, (noteX - offset), (noteY));

                var charWidth = ctx.measureText(char).width;

                ctx.fillText((!note.accidentalInParentheses) ? note.piece.fontCharCodes.sharp : note.piece.fontCharCodes.sharpWithParentheses, (noteX - (offset + charWidth + note.piece.convertTenthsToPixals(2))), (noteY));
                break;
            case "triple-flat":

                char = (!note.accidentalInParentheses) ? note.piece.fontCharCodes.doubleFlat : note.piece.fontCharCodes.doubleFlatWithParentheses;
                offset = (!note.accidentalInParentheses) ? note.piece.convertTenthsToPixals(18) : note.piece.convertTenthsToPixals(24);

                ctx.fillText(char, (noteX - offset), (noteY));

                var charWidth = ctx.measureText(char).width;

                ctx.fillText((!note.accidentalInParentheses) ? note.piece.fontCharCodes.flat : note.piece.fontCharCodes.flatWithParentheses, (noteX - (offset + charWidth + note.piece.convertTenthsToPixals(-5))), (noteY));
                break;
        }
    };

    var drawLineThroughNote = function (noteX, noteY, note) {
        if (note.staff > 1) {
            var t = true;
        }
        var staffTopY = note.measure.staffTopYs[(note.staff - 1)];
        var staffBottomY = note.measure.staffBottomYs[(note.staff - 1)];
        var numberLines = 0;
        var goingUp = true;
        var lineY = staffTopY;

        if (noteY < staffTopY) {
            var topHeight = (staffTopY - noteY);
            var numberLines = Math.floor(((staffTopY - noteY) / note.piece.noteHeadHeight) + 0.1);
        }
        else if (noteY > staffBottomY) {
            var numberLines = Math.floor(((noteY - staffBottomY) / note.piece.noteHeadHeight) + 0.1);
            goingUp = false;
            lineY = staffBottomY;
        }


        for (var i = 0; i < numberLines; i++) {
            if (goingUp)
                lineY -= note.piece.noteHeadHeight;
            else
                lineY += note.piece.noteHeadHeight;
            ctx.beginPath();
            ctx.moveTo((noteX - note.piece.convertTenthsToPixals(3)), lineY);
            ctx.lineTo((noteX + note.piece.noteHeadWidth + note.piece.convertTenthsToPixals(3)), lineY);
            ctx.lineCap = 'butt';
            ctx.stroke();
            ctx.closePath();
        }
    };

    
};