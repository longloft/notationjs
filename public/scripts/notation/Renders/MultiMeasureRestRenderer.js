﻿var NotationJS  = NotationJS || {};

NotationJS.MultiMeasureRestRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (measure) {

        var beamHeight = measure.piece.convertTenthsToPixals(6);
        var padding = measure.piece.convertTenthsToPixals(30);

        var part = measure.part;

        for (var i = 0; i < part.numberStaves; i++) {
            var centerY = measure.staffMiddleYs[i];
            var topMiddleLineY = measure.staffTopMiddleYs[i];
            var bottomMiddleLineY = measure.staffBottomMiddleYs[i];

            var xLeft = measure.x + measure.paddingLeft + padding;
            var xRight = measure.x + measure.width - padding;
            var yTop = centerY - (beamHeight * 0.5);
            var yBottom = centerY + (beamHeight * 0.5);

            //draw beam
            ctx.moveTo(xLeft, yTop);
            ctx.lineTo(xRight, yTop);
            ctx.lineTo(xRight, yBottom);
            ctx.lineTo(xLeft, yBottom);
            ctx.lineTo(xLeft, yTop);
            ctx.fill();
            ctx.closePath();

            //draw jogs
            ctx.beginPath();
            ctx.moveTo(xLeft, topMiddleLineY);
            ctx.lineTo(xLeft, bottomMiddleLineY);
            ctx.lineCap = 'butt';
            ctx.stroke();
            ctx.closePath();

            ctx.beginPath();
            ctx.moveTo(xRight, topMiddleLineY);
            ctx.lineTo(xRight, bottomMiddleLineY);
            ctx.lineCap = 'butt';
            ctx.stroke();
            ctx.closePath();

            //draw number rests
            self.pieceRenderer.setMusicFont();
            var fontY = measure.staffTopYs[i] - (measure.piece.noteHeadWidth * 1);
            var textWidth = ctx.measureText(measure.numberMeasuresRest).width;

            var fontX = xLeft + ((xRight - xLeft) * 0.5) - (textWidth * 0.5);

            ctx.fillText(measure.numberMeasuresRest, fontX, fontY);
        }

    };

};