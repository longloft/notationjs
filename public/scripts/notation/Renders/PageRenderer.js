﻿var NotationJS = NotationJS || {};

NotationJS.PageRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (piece, page) {

        var credRenderer = new NotationJS.CreditsRenderer(canvas, ctx);
        credRenderer.pieceRenderer = self.pieceRenderer;
        credRenderer.pageRenderer = self;

        credRenderer.draw(page.credits);

        //set music font
        self.pieceRenderer.setMusicFont();

        var firstMeasure = true;
        for (var x = 0; x < page.measures.length; x++) {
            var measures = page.measures[x];

            if (measures != undefined && measures.length > 0) {
                var measure = measures[0];
                var maRenderer = new NotationJS.MeasureAttributesRenderer(canvas, ctx);
                maRenderer.piece = piece;
                maRenderer.draw(measures, firstMeasure);
                firstMeasure = false;
            }
        }


        for (var i = 0; i < page.parts.length; i++) {
            var pagePart = page.parts[i];

            firstMeasure = true;
            var previousAttributes = null;
            var measureRenderer = new NotationJS.MeasureRenderer(canvas, ctx);
            var multiMeasureRestRenderer = new NotationJS.MultiMeasureRestRenderer(canvas, ctx);
            var noteRenderer = new NotationJS.NoteRenderer(canvas, ctx);
            var stemRenderer = new NotationJS.StemRenderer(canvas, ctx);
            measureRenderer.pieceRenderer = self.pieceRenderer;
            multiMeasureRestRenderer.pieceRenderer = self.pieceRenderer;
            noteRenderer.pieceRenderer = self.pieceRenderer;
            stemRenderer.pieceRenderer = self.pieceRenderer;

            for (var j = 0; j < pagePart.measures.length; j++) {
                var measure = pagePart.measures[j];

                if (measure.width > 0) {
                    measureRenderer.draw(piece, page, pagePart.part, measure);

                    if (!measure.isMultiMeasureRest) {
                        for (var n = 0; n < measure.notes.length; n++) {
                            noteRenderer.draw(measure.notes[n]);
                        }

                        for (var c = 0; c < measure.chords.length; c++) {
                            stemRenderer.drawStemForChord(measure.chords[c]);
                        }
                    }
                }

                if (measure.isMultiMeasureRest)
                    multiMeasureRestRenderer.draw(measure);

                previousAttributes = measure.attributes;
                firstMeasure = false;
            }

            if (pagePart.endings != null && pagePart.endings.length > 0) {
                for (var endingNumber in pagePart.endings) {
                    var ending = pagePart.endings[endingNumber];
                    var endingRenderer = new NotationJS.EndingRenderer(canvas, ctx);
                    endingRenderer.pieceRenderer = self.pieceRenderer;
                    endingRenderer.draw(ending);
                }
            }

            var tieRenderer = new NotationJS.TieRenderer(canvas, ctx);
            for (var n = 0; n < pagePart.notes.length; n++) {
                var note = pagePart.notes[n];
                if (note.isTied) {
                    tieRenderer.draw(note);
                }
            }

            var slurRenderer = new NotationJS.SlurRenderer(canvas, ctx);
            slurRenderer.piece = piece;
            for (var si = 0; si < pagePart.slurs.length; si++) {
                if (pagePart.slurs[si] != undefined) {
                    slurRenderer.draw(pagePart.slurs[si]);
                }
            }
        }



        var barlineRenderer = new NotationJS.MeasureBarlineRenderer(canvas, ctx);
        barlineRenderer.draw(piece, page, pagePart);

        var bracketsRenderer = new NotationJS.BracketsRenderer(canvas, ctx);
        bracketsRenderer.draw(piece, page, pagePart);
    };
};