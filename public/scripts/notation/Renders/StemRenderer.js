﻿var NotationJS = NotationJS || {};

NotationJS.StemRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;
    self.pieceRenderer = null;

    self.drawStemForNote = function (note, overrideStemY) {

        self.pieceRenderer.setMusicFont(note.isGrace);

        var cords = self.getStemCordsForNote(note, overrideStemY);

        ctx.beginPath();
        ctx.moveTo(cords.stemX, cords.noteY);
        ctx.lineTo(cords.stemX, cords.stemY);
        ctx.lineCap = 'butt';
        ctx.stroke();
        ctx.closePath();

        var flagChar = getFlagChar(note);

        if (flagChar != null) {

            if (cords.stemX > 0) {
                var flagY = (note.stemDirection == "down") ? (cords.stemY - note.piece.noteHeadHeight) : (cords.stemY + note.piece.noteHeadHeight);
                ctx.fillText(flagChar, cords.stemX, flagY);
            }
        }
    };

    self.getStemCordsForNote = function (note, overrideStemY) {
        var myX = note.measure.x + note.defaultX;
        var myY = note.y;

        if (note.isMeasureRest) {
            myX = ((note.measure.x) + (note.measure.width * 0.5)) - (ctx.measureText(char).width * 0.5) + (note.measure.paddingLeft * 0.5);
        }
        //get default y
        var stemX = myX;
        if (note.stemDirection == "up")
        {
            stemX = (!note.isGrace) ? myX + note.piece.noteHeadWidth : myX + (note.piece.noteHeadWidth * 0.6);
        }
        var stemY = overrideStemY || note.stemY;

        if (note.chord != undefined && note.chord.chordOverrideStemNoteY != undefined)
            myY = note.chord.chordOverrideStemNoteY;

        //nudge stem x to look better with note head
        if (note.stemDirection == "up") {
            myY -= note.piece.convertTenthsToPixals(2);
        }
        else {
            if (note.pitch != undefined && note.pitch.type != "half")
                stemX += note.piece.convertTenthsToPixals(0.5);
            myY += note.piece.convertTenthsToPixals(2.2);
        }

        return { noteX: myX, noteY: myY, stemX: stemX, stemY: stemY };
    };

    //figure out what is top note for chord stem.
    self.drawStemForChord = function (chord) {
        var initialNote = chord.initialNote;
        var measure = initialNote.measure;
        var needsCustomStem = (initialNote.stemDefaultY != null);

        if (needsCustomStem) {
            var defaultY = initialNote.stemDefaultY;

            var note = chord.stemNote;

            self.drawStemForNote(note, initialNote.stemY);
        }
    };

    var getFlagChar = function (note) {
        var isStemDown = (note.stemDirection == "down");
        var needsCustomStem = (note.stemDefaultY != null || note.partOfChord);

        //don't process if not initial note of chord with stem data
        if (note.chord != null && note.chord.initialNote != note)
            return null;

        var char = null;

        switch (note.type) {
            case "eighth":
                if (!note.beam) {
                    char = (isStemDown) ? note.piece.fontCharCodes.eighthNoteFlagDown : note.piece.fontCharCodes.eighthNoteFlagUp;
                }

                break;
            case "16th":
                if (!note.beam) {
                    char = (isStemDown) ? note.piece.fontCharCodes.sixteenthNoteFlagDown : note.piece.fontCharCodes.sixteenthNoteFlagUp;
                }

                break;

            case "32nd":
                if (!note.beam) {
                    char = (isStemDown) ? note.piece.fontCharCodes.sixteenthNoteFlagDown : note.piece.fontCharCodes.sixteenthNoteFlagUp;
                }

                break;

            default:
                char = null;
                break;
        }

        return char;
    };

};