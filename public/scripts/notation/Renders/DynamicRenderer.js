﻿var NotationJS = NotationJS || {};

NotationJS.DynamicRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.pieceRenderer = null;

    self.draw = function (dynamic) {

        if (dynamic.mark != undefined && dynamic.piece.fontCharCodes[dynamic.mark] != undefined) {

            var mark = dynamic.piece.fontCharCodes[dynamic.mark];
            var defaultX = dynamic.measure.x + dynamic.defaultX;
            var defaultY = (dynamic.defaultY * -1) + dynamic.measure.staffTopYs[(dynamic.staff - 1)];

            if (dynamic.halign == "center")
                defaultX -= (ctx.measureText(mark).width * 0.5);

            ctx.fillText(mark, defaultX, defaultY);
        }
    };
};