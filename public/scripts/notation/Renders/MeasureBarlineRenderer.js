﻿var NotationJS = NotationJS || {};

NotationJS.MeasureBarlineRenderer = function (canvas, ctx) {
    "user strict";

    var self = this;

    self.draw = function (piece, page, part) {

        var numberMeasures = page.measureGroups.length;
        var firstMeasure = true;
        var renderer = new NotationJS.BarlineRenderer(canvas, ctx);
        renderer.piece = piece;

        for (var i = 0; i < numberMeasures; i++) {
            var measureGroup = page.measureGroups[i];
            var masterMeasure = measureGroup.measure;

            for (var x = 0; x < measureGroup.barlineMeasureGroupings.length; x++) {
                var barlineMeasureGrouping = measureGroup.barlineMeasureGroupings[x];
                if (barlineMeasureGrouping.length > 0) {
                    var topMeasure = barlineMeasureGrouping[0];
                    var bottomMeasure = barlineMeasureGrouping[(barlineMeasureGrouping.length - 1)];

                    //draw previously finish grouping
                    renderer.drawBarline(topMeasure.x, topMeasure.y, bottomMeasure.bottomY, topMeasure.barline, topMeasure, measureGroup.measures);
                } else {
                    renderer.draw(measureGroup.measure);
                }
            }

            if ((firstMeasure || masterMeasure.newLine) && (masterMeasure.part.numberStaves > 1 || page.parts.length > 1)) {
                var topMeasure = measureGroup.measures[0];
                var bottomMeasure = measureGroup.measures[(measureGroup.measures.length - 1)];

                renderer.drawLeftBarline(topMeasure.x, topMeasure.y, bottomMeasure.bottomY, topMeasure);
            }
            firstMeasure = false;
        }
    };
};