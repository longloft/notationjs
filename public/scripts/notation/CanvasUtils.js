﻿function CanvasUtils() {
    var self = this;

    self.getNoteHeadHeight = function (text, font) {
       // alert(font);

        var fontDraw = document.createElement("canvas");

        var height = 100;
        var width = 100;

        // here we expect that font size will be less canvas geometry
        fontDraw.setAttribute("height", height);
        fontDraw.setAttribute("width", width);

        var ctx = fontDraw.getContext('2d');
        // black is default
        ctx.fillRect(10, 10, width, height);
        //ctx.textBaseline = 'top';
        ctx.fillStyle = 'white';
        ctx.font = font;
        ctx.fillText(text/*'Eg'*/, 0, 0);

        var pixels = ctx.getImageData(0, 0, width, height).data;

        // row numbers where we first find letter end where it ends 
        var start = -1;
        var end = -1;

        for (var row = 0; row < height; row++) {
            for (var column = 0; column < width; column++) {

                var index = (row * width + column) * 4;

                // if pixel is not white (background color)
                if (pixels[index] == 0) {
                    // we havent met white (font color) pixel
                    // on the row and the letters was detected
                    if (column == width - 1 && start != -1) {
                        end = row;
                        row = height;
                        break;
                    }
                    continue;
                }
                else {
                    // we find top of letter
                    if (start == -1) {
                        start = row;
                    }
                    // ..letters body
                    break;
                }

            }

        }
        /*
         document.body.appendChild(fontDraw);
         fontDraw.style.pixelLeft = 400;
         fontDraw.style.pixelTop = 400;
         fontDraw.style.position = "absolute";
        */

        var height = end - start;

        return height * 2;

    }

    self.textHeight = function (text, font) {

        var fontDraw = document.createElement("canvas");

        var height = 100;
        var width = 100;

        // here we expect that font size will be less canvas geometry
        fontDraw.setAttribute("height", height);
        fontDraw.setAttribute("width", width);

        var ctx = fontDraw.getContext('2d');
        // black is default
        ctx.fillRect(0, 0, width, height);
        ctx.textBaseline = 'top';
        ctx.fillStyle = 'white';
        ctx.font = font;
        ctx.fillText(text/*'Eg'*/, 0, 0);

        var pixels = ctx.getImageData(0, 0, width, height).data;

        // row numbers where we first find letter end where it ends 
        var start = -1;
        var end = -1;

        for (var row = 0; row < height; row++) {
            for (var column = 0; column < width; column++) {

                var index = (row * width + column) * 4;

                // if pixel is not white (background color)
                if (pixels[index] == 0) {
                    // we havent met white (font color) pixel
                    // on the row and the letters was detected
                    if (column == width - 1 && start != -1) {
                        end = row;
                        row = height;
                        break;
                    }
                    continue;
                }
                else {
                    // we find top of letter
                    if (start == -1) {
                        start = row;
                    }
                    // ..letters body
                    break;
                }

            }

        }
        /*
         document.body.appendChild(fontDraw);
         fontDraw.style.pixelLeft = 400;
         fontDraw.style.pixelTop = 400;
         fontDraw.style.position = "absolute";
        */



        return end - start;

    }
}