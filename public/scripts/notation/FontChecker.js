﻿/// <reference path="MaestroFontCharCodes.js" />
/// <reference path="CanvasUtils.js" />
function FontChecker() {

    var self = this;

    self.onLoad = null;
    self.onFail = null;
    self.hasEmbeddedFonts = false;

    var maestroFontChecker = new MaestroFontChecker();
    var maestroPercFontChecker = new MaestroPercussionFontChecker();

    var hasMaestroFontBeenEmbedded = function() {
        var canvasUtils = new CanvasUtils();
        var fontDraw = document.createElement("canvas");
        var ctx = fontDraw.getContext('2d');
        ctx.font = "normal 24pt MaestroNotes";

        var newWidth = ctx.measureText(MaestroFontCharCodes.noteHead).width;

        return (Math.round(newWidth) != 10);
    };

    var hasMaestroPercussionFontBeenEmbedded = function() {
        var canvasUtils = new CanvasUtils();
        var fontDraw = document.createElement("canvas");
        var ctx = fontDraw.getContext('2d');
        ctx.font = "normal 24pt MaestroPercNotes";

        var newWidth = ctx.measureText(MaestroPercussionFontCharCodes.natural).width;

        return (Math.round(newWidth) != 10);
    };

    var embeddedMaestroFont = false;

    self.embedMaestroFont = function() {

        var fontsEmbedded = 0;
        maestroFontChecker.onLoad = function() {
            //fontsEmbedded += 1; alert("regular");
            //if (fontsEmbedded == 2)
            //{
            self.hasEmbeddedFonts = true;
            self.onLoad();
            //}

        };

        maestroPercFontChecker.onLoad = function() {
            fontsEmbedded += 1;
            alert("perc");
            if (fontsEmbedded == 2) {
                self.hasEmbeddedFonts = true;
                self.onLoad();
            }
        };

        maestroFontChecker.embedFont();
        //maestroPercFontChecker.embedFont();
    };
}

function MaestroFontChecker() {
    var self = this;
    FontCheckerBase.call(self);
    self.fontName = "MaestroNotes";
    self.badWidth = 10;
    self.fontToCheckWidth = MaestroFontCharCodes.noteHead;
    self.css = "@font-face {" +
        "font-family: 'MaestroNotes';" +
        "    src: url('/fonts/maestro_.ttf') format('truetype');" +
        "    src: url('/fonts/maestro_.eot?#iefix') format('embedded-opentype')," +
        "         url('/fonts/maestro_.woff') format('woff')," +
        "         url('/fonts/maestro_.ttf') format('truetype');" +
        "    font-weight: normal;" +
        "    font-style: normal;" +
        "}";
}

function MaestroPercussionFontChecker() {
    var self = this;
    FontCheckerBase.call(self);
    self.fontName = "MaestroPercNotes";
    self.badWidth = 23;
    self.expectedWidth = 11;
    self.fontToCheckWidth = MaestroPercussionFontCharCodes.threeQuartersFlat;
    self.css = "@font-face {" +
        "font-family: 'MaestroPercNotes';" +
        "    src: url('/fonts/Perc.ttf') format('truetype');" +
        "    src: url('/fonts/Perc.eot?#iefix') format('embedded-opentype')," +
        "         url('/fonts/Perc.woff') format('woff')," +
        "         url('/fonts/Perc.ttf') format('truetype');" +
        "    font-weight: normal;" +
        "    font-style: normal;" +
        "}";
}

function FontCheckerBase() {
    var self = this;

    self.font = "";
    self.delay = 50;
    self.timeOut = 2500;
    self.expectedWidth = 10;
    self.fontToCheckWidth = MaestroFontCharCodes.noteHead;
    self.fontName = "MaestroNotes";
    self.onLoad = null;
    self.onFail = null;

    var checkDelay = 0;
    var checkTimeOut = 0;
    var badHeight = 0;

    self.fontEmbedded = false;
    self.css = ""; //override this

    self.embedFont = function() {
        if (self.fontEmbedded)
            return;
        var s = document.createElement('style');
        s.type = "text/css";
        var css = self.css;

        if (s.styleSheet) {
            s.styleSheet.cssText = css
        } else {
            s.appendChild(document.createTextNode(css));
        }

        document.getElementsByTagName('head')[0].appendChild(s);

        self.checkFont();
    };

    self.checkFont = function() {
        checkDelay = self.delay;
        checkTimeOut = self.timeOut;

        //force the ui to use the font
        var d = new Detector();
        var hasLoaded = d.detect("'" + self.fontName + "'");

        //get it's width through a canvas
        if (hasFontBeenEmbedded()) {

            if (self.onLoad)
                self.onLoad();
        } else {

            if (checkTimeOut < 0) {
                if (self.onFail)
                    self.onFail();
            } else {
                setTimeout(self.checkFont, self.delay);
                checkTimeOut = checkTimeOut - checkDelay;
            }
        }
    };

    var hasFontBeenEmbedded = function() {
        var canvasUtils = new CanvasUtils();
        var fontDraw = document.createElement("canvas");
        var ctx = fontDraw.getContext('2d');
        ctx.font = "normal 24pt " + self.fontName;

        var newWidth = ctx.measureText(self.fontToCheckWidth).width;
        //alert("newWidth" + newWidth +" expectedWidth" + self.expectedWidth);
        return (Math.round(newWidth) == self.expectedWidth);
    };
}


var Detector = function() {
    // a font will be compared against all the three default fonts.
    // and if it doesn't match all 3 then that font is not available.
    var baseFonts = ['monospace', 'sans-serif', 'serif'];

    //we use m or w because these two characters take up the maximum width.
    // And we use a LLi so that the same matching fonts can get separated
    var testString = "mmmmmmmmmmlli";

    //we test using 72px font size, we may use any size. I guess larger the better.
    var testSize = '72px';

    var h = document.getElementsByTagName("body")[0];

    // create a SPAN in the document to get the width of the text we use to test
    var s = document.createElement("span");
    s.style.fontSize = testSize;
    s.innerHTML = testString;
    var defaultWidth = {};
    var defaultHeight = {};
    for (var index in baseFonts) {
        //get the default width for the three base fonts
        s.style.fontFamily = baseFonts[index];
        h.appendChild(s);
        defaultWidth[baseFonts[index]] = s.offsetWidth; //width for the default font
        defaultHeight[baseFonts[index]] = s.offsetHeight; //height for the defualt font
        h.removeChild(s);
    }

    function detect(font) {
        var detected = false;
        for (var index in baseFonts) {
            s.style.fontFamily = font + ',' + baseFonts[index]; // name of the font along with the base font for fallback.
            h.appendChild(s);
            var matched = (s.offsetWidth != defaultWidth[baseFonts[index]] || s.offsetHeight != defaultHeight[baseFonts[index]]);
            h.removeChild(s);
            detected = detected || matched;
        }
        return detected;
    }

    this.detect = detect;
};