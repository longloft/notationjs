﻿function GestureController(canvas, ctx) {

    var self = this;
    trackTransforms(ctx);
    self.mode = NotationEngineMode.autoFillToCavasWidthHeight;
    self.container = null;
    self.page = null;

    self.onCanvasResize = null;

    self.enableZoomAndPan = true;

    var scale = 1;

    self.adjustCanvasSizeForPage = function (page) {
        self.page = page;

        //adjust width height of canvas
        if (self.mode == NotationEngineMode.autoAdjustCanvasWidthHeight) {
            self.adjustCavasWidthHeight(page);
        }
        if (self.mode == NotationEngineMode.autoFillToCavasWidthHeight) {
            self.scaleToFillToCavasWidthHeight(page);
        }
        if (self.mode == NotationEngineMode.autoFillToCavansWidthAdjustHeight)
            self.scaleToFillToCavasWidthAdjustHeight(page);
    };

    //resize the canvas to the musicxml page specs
    self.adjustCavasWidthHeight = function (page) {
        var piece = page.piece;

        var retina = window.devicePixelRatio > 1;
        var pageWidth = page.width; // (piece.pageWidth > 0) ? piece.pageWidth : canvas.width;
        var pageHeight = page.height; // (piece.pageHeight > 0) ? piece.pageHeight : canvas.height;
        var dim = 1;

        if (retina)
            dim = 2;

        var totalSquareFootage = (pageWidth * dim) * (pageHeight * dim);

        if (dim > 1 && totalSquareFootage >= (2200 * 2200)) {
            dim = 1;
        }

        canvas.width = page.width * dim;
        canvas.height = page.height * dim;

        if (dim > 1) {
            //set the style width and hight only for retina support
            //causing aliasing if changed for browsers
            canvas.style.width = page.width + "px";
            canvas.style.height = page.height + "px";
            ctx.scale(dim, dim);
        }
        scale = dim;
        ctx.currentScale = scale;
    };

    var originalHeight = null;
    var originalWidth = null;

    self.scaleToFillToCavasWidthHeight = function (page) {

        var canvasNode = self.container;

        if (originalWidth == null)
            originalWidth = canvasNode.clientWidth;
        if (originalHeight == null)
            originalHeight = canvasNode.clientHeight;

        var w = canvas.offsetWidth;
        var widthRadio = originalWidth / page.width;
        var heightRatio = originalHeight / page.height;

        scale = (widthRadio < heightRatio) ? widthRadio : heightRatio;

        var retina = window.devicePixelRatio > 1;

        if (retina) {
            scale *= 2;
            canvas.width = originalWidth * window.devicePixelRatio;
            canvas.height = originalHeight * window.devicePixelRatio;
            canvas.style.width = originalWidth + "px";
            canvas.style.height = originalHeight + "px";
        }
        else {
            canvas.width = originalWidth;
            canvas.height = originalHeight;
            canvas.style.width = originalWidth + "px";
            canvas.style.height = originalHeight + "px";
        }

        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.currentScale = scale;
        ctx.scale(scale, scale);
    };

    self.scaleToFillToCavasWidthAdjustHeight = function (page) {

        var canvasNode = self.container;

        if (originalWidth == null)
            originalWidth = canvasNode.clientWidth;
        if (originalHeight == null)
            originalHeight = canvasNode.clientHeight;

        var w = canvas.offsetWidth;
        scale = originalWidth / page.width;

        var adjustedHeight = page.height * scale;

        var retina = window.devicePixelRatio > 1;

        if (retina) {
            scale *= 2;
            canvas.width = originalWidth * window.devicePixelRatio;
            canvas.height = adjustedHeight * window.devicePixelRatio;
            canvas.style.width = originalWidth + "px";
            canvas.style.height = adjustedHeight + "px";
        }
        else {
            canvas.width = originalWidth;
            canvas.height = adjustedHeight;
            canvas.style.width = originalWidth + "px";
            canvas.style.height = adjustedHeight + "px";
        }

        ctx.setTransform(1, 0, 0, 1, 0, 0);

        ctx.currentScale = scale;
        ctx.scale(scale, scale);
    };

    var mylatesttap;
    canvas.addEventListener('touchstart', function (e) {
        if (!self.enableZoomAndPan) return;

        var lastWasPinch = isPinching;
        isPinching = false;

        var now = new Date().getTime();
        var timesince = now - mylatesttap;
        mylatesttap = new Date().getTime();
        if ((timesince < 200) && (timesince > 0) && !lastWasPinch && e.targetTouches.length == 1) {
            self.adjustCanvasSizeForPage(self.page);
            if (self.onCanvasResize)
                self.onCanvasResize(self.page);
            return;
        }


        self.lastX = e.targetTouches[0].pageX - canvas.getBoundingClientRect().left;
        self.lastY = e.targetTouches[0].pageY - canvas.getBoundingClientRect().top;
        self.lastX *= window.devicePixelRatio;
        self.lastY *= window.devicePixelRatio;
        self.dragStart = ctx.transformedPoint(self.lastX, self.lastY);
        self.lastZoomScale = null;
    }.bind(this));

    var isPinching = false;

    canvas.addEventListener('touchmove', function (e) {
        if (!self.enableZoomAndPan) return;
        e.preventDefault();
        
        if (e.targetTouches.length == 2) { //pinch
            var zoom = self.gesturePinchZoom(e);
            if(zoom)
            {
                isPinching = true;
                zoomScreen(zoom / (window.devicePixelRatio * 10));
            }
        }
        else if (e.targetTouches.length == 1 && !isPinching) {
            self.lastX = e.targetTouches[0].pageX - canvas.getBoundingClientRect().left;
            self.lastY = e.targetTouches[0].pageY - canvas.getBoundingClientRect().top;
            self.lastX *= window.devicePixelRatio;
            self.lastY *= window.devicePixelRatio;
            if (self.dragStart) {
                var pt = ctx.transformedPoint(self.lastX, self.lastY);
                ctx.translate(pt.x - self.dragStart.x, pt.y - self.dragStart.y);
                if (self.onCanvasResize)
                    self.onCanvasResize(self.page);
            }
        }
    }.bind(this));

    canvas.addEventListener('mousedown', function (evt) {
        if (!self.enableZoomAndPan) return;
        self.mdown = true;
        self.lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        self.lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
        self.lastX *= window.devicePixelRatio;
        self.lastY *= window.devicePixelRatio;
        self.dragStart = ctx.transformedPoint(self.lastX, self.lastY);
    }.bind(this));

    canvas.addEventListener('mouseup', function (e) {
        if (!self.enableZoomAndPan) return;
        self.mdown = false;
        self.dragStart = null;
    }.bind(this));

    canvas.addEventListener('mousemove', function (evt) {
        if (!self.enableZoomAndPan) return;
        self.lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        self.lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);

        self.lastX *= window.devicePixelRatio;
        self.lastY *= window.devicePixelRatio;
        if (self.dragStart) {
            var pt = ctx.transformedPoint(self.lastX, self.lastY);
            ctx.translate(pt.x - self.dragStart.x, pt.y - self.dragStart.y);
            if (self.onCanvasResize)
                self.onCanvasResize(self.page);
        }

    }.bind(this));

    canvas.addEventListener('dblclick', function () {
        self.adjustCanvasSizeForPage(self.page);
        if (self.onCanvasResize)
            self.onCanvasResize(self.page);
        if (window.getSelection)
            window.getSelection().removeAllRanges();
        else if (document.selection)
            document.selection.empty();
    });

    canvas.onmousewheel = function (evt) {
        if (!self.enableZoomAndPan) return;
        var delta = evt.wheelDelta ? evt.wheelDelta / 40 : evt.detail ? -evt.detail : 0;
        if (delta) zoomScreen(delta);
        return evt.preventDefault() && false;
    }

    var scaleFactor = 1.1;
    var zoomScreen = function (clicks) {
        self.lastX = self.lastX || 0;
        self.lastY = self.lastY || 0;

        var pt = ctx.transformedPoint(self.lastX, self.lastY);
        ctx.translate(pt.x, pt.y);
        var factor = Math.pow(scaleFactor, clicks);
        ctx.scale(factor, factor);

        ctx.currentScale += 1 - factor;
        //console.log(ctx.currentScale);
        ctx.translate(-pt.x, -pt.y);
        if (self.onCanvasResize)
            self.onCanvasResize(self.page);
    }

   
    self.gesturePinchZoom = function(event) {
        var zoom = false;

        if( event.targetTouches.length >= 2 ) {
            var p1 = event.targetTouches[0];
            var p2 = event.targetTouches[1];
            var zoomScale = Math.sqrt(Math.pow(p2.pageX - p1.pageX, 2) + Math.pow(p2.pageY - p1.pageY, 2)); //euclidian distance

            if( self.lastZoomScale ) {
                zoom = zoomScale - self.lastZoomScale;
            }

            self.lastZoomScale = zoomScale;

        }    
        return zoom;
    }





    function resizeCanvas() {
        originalWidth = null;
        originalHeight = null;

        if (self.mode == NotationEngineMode.autoFillToCavasWidthHeight || self.mode == NotationEngineMode.autoFillToCavansWidthAdjustHeight) {
            if (self.page != null)
            {
                self.adjustCanvasSizeForPage(self.page);

                if (self.onCanvasResize)
                    self.onCanvasResize(self.page);
            }
        }
    }

    window.addEventListener('resize', resizeCanvas, false);
}


//modes for adjusting music on screen
NotationEngineMode = {
    autoAdjustCanvasWidthHeight: "autoAdjustCanvasWidthHeight",
    autoFillToCavasWidthHeight: "autoFillToCavasWidthHeight",
    autoFillToCavansWidthAdjustHeight: "autoFillToCavansWidthAdjustHeight"
};

//http://phrogz.net/tmp/canvas_zoom_to_cursor.html
function trackTransforms(ctx) {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
    var xform = svg.createSVGMatrix();
    ctx.getTransform = function () { return xform; };

    var savedTransforms = [];
    var save = ctx.save;
    ctx.save = function () {
        savedTransforms.push(xform.translate(0, 0));
        return save.call(ctx);
    };
    var restore = ctx.restore;
    ctx.restore = function () {
        xform = savedTransforms.pop();
        return restore.call(ctx);
    };

    var scale = ctx.scale;
    ctx.scale = function (sx, sy) {
        xform = xform.scaleNonUniform(sx, sy);
        return scale.call(ctx, sx, sy);
    };
    var rotate = ctx.rotate;
    ctx.rotate = function (radians) {
        xform = xform.rotate(radians * 180 / Math.PI);
        return rotate.call(ctx, radians);
    };
    var translate = ctx.translate;
    ctx.translate = function (dx, dy) {
        xform = xform.translate(dx, dy);
        return translate.call(ctx, dx, dy);
    };
    var transform = ctx.transform;
    ctx.transform = function (a, b, c, d, e, f) {
        var m2 = svg.createSVGMatrix();
        m2.a = a; m2.b = b; m2.c = c; m2.d = d; m2.e = e; m2.f = f;
        xform = xform.multiply(m2);
        return transform.call(ctx, a, b, c, d, e, f);
    };
    var setTransform = ctx.setTransform;
    ctx.setTransform = function (a, b, c, d, e, f) {
        xform.a = a;
        xform.b = b;
        xform.c = c;
        xform.d = d;
        xform.e = e;
        xform.f = f;
        return setTransform.call(ctx, a, b, c, d, e, f);
    };
    var pt = svg.createSVGPoint();
    ctx.transformedPoint = function (x, y) {
        pt.x = x; pt.y = y;
        return pt.matrixTransform(xform.inverse());
    }
}