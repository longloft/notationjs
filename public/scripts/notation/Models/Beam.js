﻿var NotationJS = NotationJS || {};
NotationJS.Beam = function () {
    "user strict";

    var self = this;

    self.number = 1;

    self.type = "regular";
    //usually for sixteenth notes
    self.isHook = false;
    //beam for hook to reference for angle drawing
    self.hookReferenceBeam = null;

    self.piece = null;
    self.page = null;
    self.startNote = null;
    self.endNote = null;

};