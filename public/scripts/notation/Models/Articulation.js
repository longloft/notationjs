﻿var NotationJS = NotationJS || {};

NotationJS.Articulation = function () {
    "user strict";

    var self = this;

    self.x = 0;
    self.y = 0;
    //char code for rendering
    self.code = null;
    self.type = null;
    self.defaultX = 0;
    self.defaultY = 0;
    self.placement = null; //above or below
    self.name = null;

};