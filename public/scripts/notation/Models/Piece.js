﻿var NotationJS = NotationJS || {};

NotationJS.Piece = function () {
    "user strict";

    var self = this;

    self.currentPageNumber = 1;
    self.totalNumberPages = 1;

    self.credits = "";
    self.partsRaw = [];
    self.parts = [];
    self.pages = [];
    self.defaults = [];
    self.partList = [];
    self.groups = [];

    self.pageWidth = 0;
    self.pageHeight = 0;
    self.pageLeftMargin = 0;
    self.pageRightMargin = 0;
    self.pageTopMargin = 0;
    self.pageBottomMargin = 0;
    self.pageTopSystemDistance = 0;
    self.pageSystemDistance = 0;
    self.pageStaffDistance = 0;

    self.musicFontPoint = "";
    self.wordFontPoint = "";

    self.lightBarlineWidth = 0;
    self.heavyBarlineWidth = 0;
    self.beamWidth = 0;

    self.fontCharCodes = MaestroFontCharCodes;

    self.noteHeadWidth = 0;
    self.noteHeadHeight = 0;
    self.noteHeadHeight = 0;
    self.staffHeight = 0;

    self.tenthsPixal = 0;

    self.convertTenthsToPixals = function (tenths) {
        //return parseInt(tenths * noteStaffPoint.tenthsPixal);
        return tenths * self.tenthsPixal;
    }

    self.getCurrentPage = function () {
        return self.pages[(self.currentPageNumber - 1)];
    }

}