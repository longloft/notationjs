﻿var NotationJS = NotationJS || {};

NotationJS.Barline = function () {
    "user strict";

    var self = this;

    self.location = "";
    self.style = "";
    self.isRepeat = false;
    self.repeatWinged = null;
    self.repeatDirection = null;

    self.measure = null;

};