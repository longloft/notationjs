﻿var NotationJS = NotationJS || {};

NotationJS.Chord = function () {
    "user strict";

    var self = this;

    self.notes = [];
    self.initialNote = null;
    self.stemNote = null;

    //the y coord for the top most, or bottom most note in chord to use for stem rendering.
    self.chordOverrideStemNoteY = null;

    self.measure = null;

    self.addNote = function (note) {
        note.chord = self;
        self.notes.push(note);
    };

    self.parseNotes = function () {
        var isStemUp = (self.initialNote.stemDirection == "up");
        var stemY = self.initialNote.stemY;
        self.stemNote = self.initialNote;

        //figure out when note it top or bottom most in chord for stem rendering
        for (var i = 0; i < self.notes.length; i++) {
            var note = self.notes[i];
            if ((isStemUp && note.y > self.stemNote.y) || (!isStemUp && note.y < self.stemNote.y)) {
                self.chordOverrideStemNoteY = note.y;
                self.stemNote = note;
            }
        }
    };

    self.getTopNote = function() {
        var hightestNote = self.notes[0];
        for (var i = 0; i < self.notes.length; i++) {
            if (self.notes[i].y < hightestNote.y) {
                hightestNote = self.notes[i];
            }
        }
        return hightestNote;
    };
};