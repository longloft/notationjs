﻿var NotationJS = NotationJS || {};

NotationJS.Measure = function () {
    "user strict";

    var self = this;

    self.notes = [];
    self.chords = [];
    self.dynamics = [];

    self.part = null;
    self.page = null;
    self.piece = null;

    self.width = 0;
    self.x = 0;
    self.y = 0;
    self.bottomY = 0;

    self.leftMargin = null;
    self.rightMargin = 0;
    self.topDistance = 0;
    self.staffDistances = [];

    self.overrideLeftMargin = null;

    self.paddingLeft = 0;

    self.newLine = false;
    self.measureSystemDistance = 0;
    self.topSystemDistance = 0;

    self.isMultiMeasureRest = false;
    self.numberMeasuresRest = 0;

    self.staffTopYs = []; //all the top y for all measures (used for multi staff part)
    self.staffMiddleYs = []; //all the top y for all measures (used for multi staff part)
    self.staffBottomYs = []; //all the bottom y coords for all measures (used for multi staff part)
    self.staffTopMiddleYs = []; //all the top middle line y coords for all measures (used for multi staff part)
    self.staffBottomMiddleYs = []; //all the bottom middle line y coords for all measures (used for multi staff part)
    self.c0Ys = []; //all the c0 y coords for all measures (used for multi staff part)

    self.barline = null;

    self.attributes = null;

    self.previousKey = null;

    self.newKey = false;
    self.newTime = false;
    self.newClef = false;
    self.newDivision = false;

    self.systemDistance = 0;
    self.number = 1;
    self.newSystem = false;

    //called after measure x y positions are calculated.
    self.adjustNotePositions = function () {

        for (var s = 0; s < self.part.numberStaves; s++) {

            var bottomY = self.staffBottomYs[s];
            var topY = self.staffTopYs[s];
            var middleY = self.staffMiddleYs[s];
            var clef = self.attributes.clef[s];
            var octaveHeight = ((self.piece.noteHeadHeight * 0.5) * 7);

            switch (clef.sign) {
                //treble
                case "G":
                    self.c0Ys[s] = (((topY + (self.piece.noteHeadHeight * 4.5)) + (self.piece.noteHeadHeight * 0.5)) + (octaveHeight * 4));
                    break;
                    //bass
                case "F":
                    self.c0Ys[s] = ((topY - self.piece.noteHeadHeight) + (octaveHeight * 4));
                    break;
                    //Alto
                case "C":
                    self.c0Ys[s] = (middleY + (octaveHeight * 4));
                    break;
                default:

            }
        }

        for (var i = 0; i < self.notes.length; i++) {
            self.notes[i].adjustPositions();
        }

        for (var i = 0; i < self.chords.length; i++) {
            self.chords[i].parseNotes();
        }
    };
};