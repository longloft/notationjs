﻿var NotationJS = NotationJS || {};

NotationJS.Note = function () {
    "user strict";

    var self = this;

    self.piece = null;
    self.part = null;
    self.measure = null;

    self.x = null;
    self.y = null;
    self.charCode = null;

    self.defaultX = 0;
    self.pitch = null;
    self.isDot = false;
    self.duration = 0;
    self.type = "";
    self.staff = 1;
    self.partOfChord = false;
    self.chord = null;
    self.isRest = false;
    self.isMeasureRest = false;

    self.isGrace = false;

    self.isTied = false;
    self.tiedToNote = null;
    self.tiedType = null;
    self.tieBack = false;

    self.isSlurred = false;
    self.slur = null;

    self.fermata = null;

    self.beam = false;

    self.accidental = null;
    self.accidentalInParentheses = false;

    self.articulations = [];

    self.dynamic = null;

    self.restStep = null;
    self.restOctave = null;
    self.isPartOfMultiMeasureRest = false;

    self.stemDirection = "up";  //up or down
    self.stemDefaultY = null;
    self.stemY = null;

    self.beams = [];

    self.noteIdsAscending = { C: 1, D: 2, E: 3, F: 4, G: 5, A: 6, B: 7 };
    self.noteIdsDescending = { C: 7, D: 6, E: 5, F: 4, G: 3, A: 2, B: 1 };

    //called after measure has x y positions calculated.
    self.adjustPositions = function () {

        var clef = self.measure.attributes.clef[(self.staff - 1)];
        var c0Y = self.measure.c0Ys[(self.staff - 1)];
        var octaveHeight = ((self.piece.noteHeadHeight * 0.5) * 7);
        var staffBottomY = self.measure.staffBottomYs[(self.staff - 1)];
        var staffTopY = self.measure.staffTopYs[(self.staff - 1)];

        var centerStaffY = staffBottomY - (self.piece.staffHeight * 0.5);

        if (!self.isRest && self.pitch != undefined) {
            self.y = (c0Y - (octaveHeight * self.pitch.octave)) - ((self.piece.noteHeadHeight * 0.5) * (self.noteIdsAscending[self.pitch.step] - 1));
        }
        else if (self.isRest && self.restOctave != null && self.restStep != null) {
            self.y = (c0Y - (octaveHeight * self.restOctave)) - ((self.piece.noteHeadHeight * 0.5) * (self.noteIdsAscending[self.restStep] - 1));
        }
        else {
            self.y = centerStaffY;
        }

        if (clef.octaveChange != 0 && !self.isRest) {
            self.y += octaveHeight * clef.octaveChange;
        }

        //adjust stem
        self.stemY = staffTopY - self.stemDefaultY;

        self.x = self.measure.x + self.defaultX;

    };
};