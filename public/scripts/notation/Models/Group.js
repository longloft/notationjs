﻿var NotationJS = NotationJS || {};

NotationJS.Group = function () {
    "user strict";

    var self = this;

    self.notes = [];

    self.name = 0;
    self.number = 0;
    self.abbreviation = "";
    self.symbol = ""
    self.defaultX = 0;
    self.barline = false;
};