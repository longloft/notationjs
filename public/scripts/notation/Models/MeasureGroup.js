﻿var NotationJS = NotationJS || {};
//groups the measure barline groupings
NotationJS.MeasureGroup = function () {
    "user strict";

    var self = this;

    self.measures = [];
    self.measure = null;
    self.barlineMeasureGroupings = [];

};