﻿var NotationJS = NotationJS || {};

NotationJS.Credit = function () {
    "user strict";

    var self = this;

    self.page = null;

    self.type = "";
    self.text = "";

    self.x = 0;
    self.y = 0;
    self.width = 0;
    self.valign = null;
    self.halign = null;
    self.fontSize = null;
    self.font = null;
    self.fontWeight = null;
    self.justify = null;
    self.fontStyle = null;

};