﻿var NotationJS = NotationJS || {};

NotationJS.Dynamic = function () {
    "user strict";

    var self = this;

    self.piece = null;
    self.measure = null;

    self.defaultX = null;
    self.defaultY = null;
    self.mark = null;
    self.halign = null;
    self.staff = 1;
    self.placement = "below";
    self.soundDynamic = 0;

};