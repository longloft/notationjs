﻿var NotationJS = NotationJS || {};
//contains all part data that purtains to a specific page
NotationJS.PagePart = function () {
    "user strict";

    var self = this;

    self.part = null;
    self.page = null;
    self.measures = [];
    self.notes = [];
    self.endings = [];
    self.beams = [];
    self.slurs = [];
};