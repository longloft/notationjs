﻿var NotationJS = NotationJS || {};

NotationJS.Clef = function () {
    "user strict";

    var self = this;

    self.sign = "C";
    self.line = 0;
    self.number = 1;
    self.octaveChange = 0;
};