﻿var NotationJS = NotationJS || {};

NotationJS.Page = function() {
    "user strict";
    var self = this;

    self.credits = [];
    self.parts = [];
    self.number = 1;
    self.piece = null;
    self.measures = [];
    self.measureGroups = [];

    self.width = 0;
    self.height = 0;
    self.leftMargin = 0;
    self.rightMargin = 0;
    self.topMargin = 0;
    self.bottomMargin = 0;
    self.topSystemDistance = 0;
    self.systemDistance = 0;
    self.staffDistance = 0;

    self.systemLeftMargin = 0;
};