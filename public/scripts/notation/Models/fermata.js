﻿var NotationJS = NotationJS || {};

NotationJS.Fermata = function () {
    "user strict";

    var self = this;

    self.defaultX = null;
    self.defaultY = null;
    self.type = null;
    self.note = null;
};