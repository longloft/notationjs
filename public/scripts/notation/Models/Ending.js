﻿var NotationJS = NotationJS || {};

NotationJS.Ending = function () {
    "user strict";

    var self = this;

    self.piece = null;
    self.part = null;

    self.defaultX = null;
    self.defaultY = null;
    self.jogLength = 0;
    self.fontSize = "7.5";
    self.number = "";

    self.startMeasure = null;
    self.endMeasure = null;
    self.isDiscontinued = false;
    self.drawEnding = true;
};