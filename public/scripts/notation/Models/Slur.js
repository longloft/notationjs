﻿var NotationJS = NotationJS || {};

NotationJS.Slur = function () {
    "user strict";

    var self = this;

    self.piece = null;

    self.defaultX = null;
    self.defaultY = null;
    self.endDefaultX = null;
    self.endDefaultY = null;

    self.bezierX = null;
    self.bezierY = null;
    self.endBezierX = null;
    self.endBezierY = null;

    self.number = "";
    self.placement = "";

    self.startNote = null;
    self.endNote = null;

    self.curves = [];

    self.measures = [];
};