﻿var NotationJS = NotationJS || {};

NotationJS.Curve = function () {
    "user strict";

    var self = this;

    self.x0 = null;
    self.y0 = null;
    self.cp1x0 = null;
    self.cp1y0 = null;
    self.cp2x0 = null;
    self.cp2y0 = null;

    self.x1 = null;
    self.y1 = null;
    self.cp1x1 = null;
    self.cp1y1 = null;
    self.cp2x1 = null;
    self.cp2y1 = null;
};