﻿var NotationJS = NotationJS || {};

NotationJS.Part = function (canvas) {
    "user strict";

    var self = this;
    self.page = null;
    self.id = "";
    self.numberStaves = 1;
    self.staves = [];
    self.measures = [];
    self.notes = [];
    self.endings = [];
    self.beams = [];
    self.slurs = [];

    self.groups = [];
    self.name = "";
    self.abbreviation = "";
    self.printName = false;

};