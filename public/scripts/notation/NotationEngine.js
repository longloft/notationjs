﻿/// <reference path="MusicXmlEngine2.js" />
/// <reference path="CanvasUtils.js" />

function NotationEngine(container, options) {

    var self = this;

    var canvas = document.createElement('canvas');
    container.appendChild(canvas);

    var ctx = canvas.getContext('2d');
    var currentPageNumber = 1;
    var totalNumberPages = 0;
    var piece = null;
    var musicXml = null;
    var jsonMusicXml = null;
    var canvasUtils = new CanvasUtils();
    var fontChecker = new FontChecker();
    var gestureController = new GestureController(canvas, ctx);

    gestureController.container = container;

    //event funcitions
    self.onLoad = null;

    //getters setters
    self.getMode = function ()      { return gestureController.mode; };
    self.setMode = function (mode)  { gestureController.mode = mode; };
    self.getEnableZoomAndPan = function () { return gestureController.enableZoomAndPan; };
    self.setEnableZoomAndPan = function (enableZoomAndPan) { gestureController.enableZoomAndPan = enableZoomAndPan; };

    self.getTotalNumberPages = function() { return totalNumberPages; }

    self.getPageNumber = function()  {  return currentPageNumber;  }


    //methods
    self.goToPage = function (newPageNumber)
    {
        currentPageNumber = newPageNumber;

        if (piece)
            self.loadPageFromPiece(currentPageNumber);
    }

    fontChecker.onLoad = function () {
        if (jsonMusicXml)
            self.loadDocument(musicXml);
    }

    self.loadDocument = function (xml, pageNumber) {

        musicXml = xml;

        //convert xml to json
        var parser = new NotationParser();
        var doc;
        try {
            doc = parser.parseXmlString(musicXml);
        } catch (e) {
            alert("Unable to parse file data as music xml.");
            return;
        }

        jsonMusicXml = doc;

        self.loadPageFromDocument(jsonMusicXml, pageNumber);
    }

    self.loadPageFromDocument = function (doc, pageNumber) {

        if (typeof (pageNumber) != "undefined")
            currentPageNumber = pageNumber;

        if (!fontChecker.hasEmbeddedFonts) {
            fontChecker.embedMaestroFont();
            return;
        }

        var pieceDes = new NotationJS.MusicXml3.PieceSerializer();
        
        self.currentPageNumber = currentPageNumber;

        piece = pieceDes.deserialize(doc);

        self.loadPageFromPiece(piece, pageNumber);
    };

    self.loadPageFromPiece = function (pageNumber) {
        piece.currentPageNumber = currentPageNumber;
        var page = piece.getCurrentPage();

        //make sure screen is clear
        self.clearCanvas();

        gestureController.adjustCanvasSizeForPage(page);

        //make sure screen is clear
        self.clearCanvas();

        totalNumberPages = piece.totalNumberPages;
        var pieceRenderer = new NotationJS.PieceRenderer(canvas, ctx);
        pieceRenderer.fontCharCodes = self.fontCharCodes;

        pieceRenderer.draw(piece, page);

        if (self.onLoad)
            self.onLoad();
    };

    self.clearCanvas = function () {
        // Store the current transformation matrix
        ctx.save();

        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Restore the transform
        ctx.restore();
    };

    gestureController.onCanvasResize = function (page) {

        self.clearCanvas();

        var pieceRenderer = new NotationJS.PieceRenderer(canvas, ctx);
        pieceRenderer.fontCharCodes = self.fontCharCodes;

        pieceRenderer.draw(piece, page);
    };

   
}
