﻿var MaestroFontCharCodes = {

    flat : 'b',
    sharp: String.fromCharCode(35),
    doubleFlat: String.fromCharCode(8747),
    doubleSharp: String.fromCharCode(8249),
    natural: 'n',

    flatWithParentheses: String.fromCharCode(65),
    doubleFlatWithParentheses: String.fromCharCode(229),
    sharpWithParentheses: String.fromCharCode(97),
    doubleSharpWithParentheses: String.fromCharCode(197),
    nateralWithParentheses: String.fromCharCode(78),

    dot: String.fromCharCode(46),
    noteHead: String.fromCharCode(339),
    noteHeadOpen: String.fromCharCode(729),
    trebleClef: '&',
    trebleClefOctaveHigher: String.fromCharCode(8224),
    trebleClefOctaveLower: String.fromCharCode(86),
    bassClef: String.fromCharCode(63),
    bassClefOctaveHigher: String.fromCharCode(202),
    bassClefOctaveLower: String.fromCharCode(116),
    altoClef: String.fromCharCode(66),
    quarterNoteRest: String.fromCharCode(338),
    quarterNoteStemUp: 'q',
    quarterNoteStemDown: 'Q',
    eighthNoteRest: String.fromCharCode(8240),
    eighthNoteStemUp: 'e',
    eighthNoteStemDown: 'E',
    eighthNoteFlagDown:'J',
    eighthNoteFlagUp: 'j',
    sixteenthNoteRest: String.fromCharCode(8776),
    sixteenthNoteStemUp: 'x',
    sixteenthNoteStemDown: 'X',
    sixteenthNoteFlagDown: 'R',
    sixteenthNoteFlagUp: 'r',
    thirtySecondNoteRest: String.fromCharCode(174),
    thirtySecondNoteStemUp: String.fromCharCode(85),
    thirtySecondNoteStemDown: String.fromCharCode(88),
    thirtySecondNoteFlagDown: 'R',
    thirtySecondNoteFlagUp: 'r',
    halfNoteRest: String.fromCharCode(211),
    halfNoteStemUp: 'H',
    halfNoteStemDown: 'h',
    wholeNoteRest: String.fromCharCode(8721),
    wholeNote: 'w',

    staccato: String.fromCharCode(46),
    strongAccent: String.fromCharCode(94),
    strongAccentInverted: String.fromCharCode(118),
    accent: String.fromCharCode(62),

    fermata: String.fromCharCode(85),
    fermataInverted: String.fromCharCode(117),


    p: String.fromCharCode(112),
    pp: String.fromCharCode(960),
    ppp: String.fromCharCode(8719),
    pppp: String.fromCharCode(216),
    f: String.fromCharCode(102),
    ff: String.fromCharCode(402),
    fff: String.fromCharCode(207),
    ffff: String.fromCharCode(206),
    mp: String.fromCharCode(80),
    mf: String.fromCharCode(70),
    fz: String.fromCharCode(90),
    fp: String.fromCharCode(205),
    sf: String.fromCharCode(83),
    sffz: String.fromCharCode(231),
    sfp: String.fromCharCode(119),
    sfpp: String.fromCharCode(8706),
    sfz: String.fromCharCode(223),

    cutTime: String.fromCharCode(67),

    brace: function(braceX, braceTopY, braceBottomY, ctx, convertToTenths)
    {
        var height = braceBottomY - braceTopY;
        var sizeRatio = height / (452.8 - 15.0);
        var xOffset = ((braceX - (46.1 * sizeRatio) ) / sizeRatio);
        var yOffset = ((braceBottomY - (452.8 * sizeRatio)) / sizeRatio);
        
        ctx.beginPath();
        ctx.moveTo(((46.1 + xOffset) * sizeRatio), ((452.8 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((36.0 + xOffset) * sizeRatio), ((436.1 + yOffset) * sizeRatio), ((29.0 + xOffset) * sizeRatio), ((418.3 + yOffset) * sizeRatio), ((26.2 + xOffset) * sizeRatio), ((398.9 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((22.2 + xOffset) * sizeRatio), ((370.9 + yOffset) * sizeRatio), ((28.1 + xOffset) * sizeRatio), ((344.0 + yOffset) * sizeRatio), ((34.5 + xOffset) * sizeRatio), ((317.0 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((37.7 + xOffset) * sizeRatio), ((304.0 + yOffset) * sizeRatio), ((39.9 + xOffset) * sizeRatio), ((290.6 + yOffset) * sizeRatio), ((41.2 + xOffset) * sizeRatio), ((277.3 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((42.6 + xOffset) * sizeRatio), ((261.9 + yOffset) * sizeRatio), ((36.9 + xOffset) * sizeRatio), ((248.6 + yOffset) * sizeRatio), ((26.2 + xOffset) * sizeRatio), ((237.4 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((25.2 + xOffset) * sizeRatio), ((236.3 + yOffset) * sizeRatio), ((24.2 + xOffset) * sizeRatio), ((235.2 + yOffset) * sizeRatio), ((23.0 + xOffset) * sizeRatio), ((233.9 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((23.9 + xOffset) * sizeRatio), ((232.7 + yOffset) * sizeRatio), ((24.6 + xOffset) * sizeRatio), ((231.6 + yOffset) * sizeRatio), ((25.5 + xOffset) * sizeRatio), ((230.7 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((40.0 + xOffset) * sizeRatio), ((216.5 + yOffset) * sizeRatio), ((43.8 + xOffset) * sizeRatio), ((198.8 + yOffset) * sizeRatio), ((40.5 + xOffset) * sizeRatio), ((179.7 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((37.4 + xOffset) * sizeRatio), ((161.2 + yOffset) * sizeRatio), ((32.7 + xOffset) * sizeRatio), ((143.0 + yOffset) * sizeRatio), ((28.8 + xOffset) * sizeRatio), ((124.6 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((20.8 + xOffset) * sizeRatio), ((87.1 + yOffset) * sizeRatio), ((24.5 + xOffset) * sizeRatio), ((51.1 + yOffset) * sizeRatio), ((44.3 + xOffset) * sizeRatio), ((17.6 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((44.9 + xOffset) * sizeRatio), ((16.6 + yOffset) * sizeRatio), ((45.6 + xOffset) * sizeRatio), ((15.7 + yOffset) * sizeRatio), ((47.4 + xOffset) * sizeRatio), ((15.0 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((47.1 + xOffset) * sizeRatio), ((17.1 + yOffset) * sizeRatio), ((47.3 + xOffset) * sizeRatio), ((19.4 + yOffset) * sizeRatio), ((46.4 + xOffset) * sizeRatio), ((21.2 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((34.9 + xOffset) * sizeRatio), ((43.6 + yOffset) * sizeRatio), ((34.0 + xOffset) * sizeRatio), ((67.7 + yOffset) * sizeRatio), ((36.4 + xOffset) * sizeRatio), ((91.9 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((39.0 + xOffset) * sizeRatio), ((118.4 + yOffset) * sizeRatio), ((43.6 + xOffset) * sizeRatio), ((144.6 + yOffset) * sizeRatio), ((46.2 + xOffset) * sizeRatio), ((171.1 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((47.3 + xOffset) * sizeRatio), ((182.0 + yOffset) * sizeRatio), ((46.1 + xOffset) * sizeRatio), ((193.4 + yOffset) * sizeRatio), ((44.5 + xOffset) * sizeRatio), ((204.3 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((42.8 + xOffset) * sizeRatio), ((216.0 + yOffset) * sizeRatio), ((37.9 + xOffset) * sizeRatio), ((222.6 + yOffset) * sizeRatio), ((26.3 + xOffset) * sizeRatio), ((233.5 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((26.6 + xOffset) * sizeRatio), ((234.0 + yOffset) * sizeRatio), ((26.8 + xOffset) * sizeRatio), ((234.6 + yOffset) * sizeRatio), ((27.3 + xOffset) * sizeRatio), ((235.0 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((41.4 + xOffset) * sizeRatio), ((246.1 + yOffset) * sizeRatio), ((46.1 + xOffset) * sizeRatio), ((261.6 + yOffset) * sizeRatio), ((46.7 + xOffset) * sizeRatio), ((278.6 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((47.6 + xOffset) * sizeRatio), ((300.3 + yOffset) * sizeRatio), ((43.5 + xOffset) * sizeRatio), ((321.4 + yOffset) * sizeRatio), ((40.3 + xOffset) * sizeRatio), ((342.6 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((37.1 + xOffset) * sizeRatio), ((363.9 + yOffset) * sizeRatio), ((34.5 + xOffset) * sizeRatio), ((385.2 + yOffset) * sizeRatio), ((35.9 + xOffset) * sizeRatio), ((406.8 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((36.8 + xOffset) * sizeRatio), ((420.1 + yOffset) * sizeRatio), ((39.9 + xOffset) * sizeRatio), ((433.0 + yOffset) * sizeRatio), ((45.8 + xOffset) * sizeRatio), ((445.2 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((46.9 + xOffset) * sizeRatio), ((447.4 + yOffset) * sizeRatio), ((47.2 + xOffset) * sizeRatio), ((449.9 + yOffset) * sizeRatio), ((47.8 + xOffset) * sizeRatio), ((452.3 + yOffset) * sizeRatio));
        ctx.bezierCurveTo(((47.2 + xOffset) * sizeRatio), ((452.5 + yOffset) * sizeRatio), ((46.6 + xOffset) * sizeRatio), ((452.6 + yOffset) * sizeRatio), ((46.1 + xOffset) * sizeRatio), ((452.8 + yOffset) * sizeRatio));
        
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.fill();

        ctx.closePath();
    }
};

var MaestroPercussionFontCharCodes = {
    natural: String.fromCharCode(110),
    threeQuartersFlat: String.fromCharCode(73)
};