﻿/// <reference path="MusicXml/Renders/PieceRenderer.js" />
/// <reference path="MusicXml/Piece.js" />
/// <reference path="CanvasUtils.js" />
/// <reference path="MaestroFontCharCodes.js" />
/// <reference path="FontChecker.js" />


function MusicXmlEngine(canvas, options) {

    var self = this;
    var document = null;
    var canvas = canvas;
    var ctx = canvas.getContext('2d');

    var canvasUtils = new CanvasUtils();
    var fontChecker = new FontChecker();

    var currentPage = 1;
    var totalNumberPages = 0;
    var noteStaffPoint = {};
    var musicFontPoint = {};
    var wordFontPoint = {};

    self.fontCharCodes = MaestroFontCharCodes;

    //event funcitions
    self.onLoad = null;

    //page layout data
    var pageLayoutDetails = {};

    fontChecker.onLoad = function () {
        if (document)
            self.loadDocument(document);
    }

    self.autoAdjustCanvasWidthHeight = true;

    self.getPageLayoutData = function () {
        return pageLayoutDetails;
    }

    self.getTotalNumberPages = function () {
        return totalNumberPages;
    }

    self.getPageNumber = function () {
        return currentPage;
    }

    self.goToPage = function (newPageNumber) {
        currentPage = newPageNumber;

        if (document)
            self.loadDocument(document);
    }

    var convertTenthsToPixals = function (tenths) {
        //return parseInt(tenths * noteStaffPoint.tenthsPixal);
        return tenths * noteStaffPoint.tenthsPixal;
    }

    self.loadDocument = function (doc, pageNumber) {
        document = doc;

        if (typeof (pageNumber) != "undefined")
            currentPage = pageNumber;

        //we need to make sure that the font is loaded
        if (!fontChecker.hasEmbeddedFonts) {
            fontChecker.embedMaestroFont();
            return;
        }

        if (canvas.getContext) {

            //clear whatever was previously drawn
            self.clearCanvas();

            var defaults = doc["score-partwise"][1].defaults;
            var partList = doc["score-partwise"][1]["part-list"];

            //get layout data
            //pageLayoutDetails = self.parsePageLayout(defaults);

           


            var piece = new Piece();
            var pieceRenderer = new NotationJS.PieceRenderer(canvas, ctx);
            pieceRenderer.fontCharCodes = self.fontCharCodes;

            piece.currentPageNumber = currentPage;

            //set music font settings
            musicFontPoint = piece.getMusicFontSettings(defaults);
            wordFontPoint = piece.getWordFontSettings(defaults);

            //setup measurements based off of defaults
            noteStaffPoint = self.setupMeasurements(piece, musicFontPoint);

            piece.load(doc);

            //adjust width height of canvas
            if (self.autoAdjustCanvasWidthHeight) {
                self.adjustCavasWidthHeight(piece);
            }

            var page = piece.getCurrentPage();
            
            var parts = piece.parts; 
          

            //clear slurs
            slurBeginPoinstArray = [];
            slurEndPoinstArray = [];

            //parse out only current page data
            //var partMeasureDetails = self.getMeasurePrintLayouts(parts);
            totalNumberPages = piece.totalNumberPages;

            pieceRenderer.draw(piece, page);

            //set music font
            self.setMusicFont();

            //self.drawParts(parts, partMeasureDetails, pageLayoutDetails, partList);
        }

        if (self.onLoad)
            self.onLoad();
    };


    self.drawParts = function (parts, partMeasureDetails, pageLayoutDetails, partList) {

        var numberMeasures = parts[0].measures.length;
        var partMeasureX = pageLayoutDetails.pageLeftMargin;
        var partMeasureY = pageLayoutDetails.pageTopSystemDistance + pageLayoutDetails.pageTopMargin;
        var partMeasureBottomY = 0;

        //persist the staff distance for whole page
        var staffDistance = pageLayoutDetails.pageStaffDistance;

        var printBrackets = true;

        //print off measures
        for (var m = 0; m < numberMeasures; m++) {

            var measure = parts[0].measures[m];

            //get measure details
            var measureWidth = convertTenthsToPixals(parseInt(measure["@attributes"].width));
            var measureSystemDistance = pageLayoutDetails.pageSystemDistance;
            var firstMeasureOfPiece = (measure["@attributes"] && measure["@attributes"].number == "1");
            var newLine = false;
            if (measure.print) {
                if (measure.print["@attributes"])
                    newLine = (measure.print["@attributes"]["new-system"]);
                if (measure.print["system-layout"])
                    measureSystemDistance = (measure.print["system-layout"]["system-distance"]) ? convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-distance"]["#text"])) : pageLayoutDetails.pageSystemDistance;
                if (measure.print["staff-layout"]) {
                    pageLayoutDetails.staffDistance = (measure.print["staff-layout"] && measure.print["staff-layout"]["staff-distance"]) ? convertTenthsToPixals(parseInt(measure.print["staff-layout"]["staff-distance"]["#text"])) : 0;
                }
            }


            if (newLine) {
                //want to print the brackets for the new line
                printBrackets = true;
                //reset to page margin
                partMeasureX = pageLayoutDetails.pageLeftMargin;

                //add to bottom y of last measure
                partMeasureY = (partMeasureBottomY + measureSystemDistance);
            }

            var measureY = partMeasureY;
            var firstPart = true;
            var partMeasureDrawDetails = [];

            //print all parts for this measure
            for (var i = 0; i < parts.length; i++) {
                var part = parts[i];
                var partMeasure = part.measures[m];
                var details = partMeasureDetails[i][m];

                if (details.topDistance) {
                    partMeasureY = measureY = details.topDistance + pageLayoutDetails.pageTopMargin;
                    partMeasureX = details.leftMargin + pageLayoutDetails.pageLeftMargin;
                }

                if (details.staffDistance > 0)
                    staffDistance = details.staffDistance;

                //get the staff distance print override in xml for part on next measure
                //so we have to calculate the y before we print
                if (!firstPart)
                    measureY = partMeasureBottomY + staffDistance;

                //draw measure
                var measureDrawDetails = self.drawMeasure(partMeasure, details, partMeasureX, measureY, measureWidth, part.attributes, staffDistance, noteStaffPoint.staffHeight, noteStaffPoint.noteHeadHeight);
                partMeasureBottomY = measureDrawDetails.bottomY;

                if (newLine || firstMeasureOfPiece || (m == 0))
                    self.drawMeasureAttributes(measureDrawDetails, part, noteStaffPoint.noteHeadHeight, firstMeasureOfPiece);

                self.drawMeasureNotes(measureDrawDetails, noteStaffPoint, part);

                //draw all dynamics for the measure
                if (measure.direction)
                    self.drawMeasureDirections(measureDrawDetails);

                //keep track of the part measure draw details
                partMeasureDrawDetails.push({ numberStaves: measureDrawDetails.numberStaves, measure: measure, part: part, partMeasureBottomY: partMeasureBottomY, measureX: partMeasureX, measureY: measureY, measureWidth: measureWidth, staffDistance: staffDistance });

                firstPart = false;
            }

            if (printBrackets) {
                printBrackets = false;
                self.drawBrackets(partMeasureDrawDetails, partList);
                self.displayScoreAndGroupNames(partMeasureDrawDetails, partList);
            }

            self.drawMeasureLine(partMeasureDrawDetails, partList);

            partMeasureX += measureWidth;
        }

    };

    //includes dynamics and comments
    self.drawMeasureDirections = function (measureDrawDetails) {
        var directions = measureDrawDetails.measure.direction;

        if (typeof (directions) == "undefined")
            return;

        if (!(directions instanceof Array)) {
            directions = [directions];
        }

        for (var i = 0; i < directions.length; i++) {
            var direction = directions[i];

            if (!(direction["direction-type"] instanceof Array))
                direction["direction-type"] = [direction["direction-type"]];

            for (var x = 0; x < direction["direction-type"].length; x++) {
                var type = direction["direction-type"][x];

                if (typeof (type.dynamics) != "undefined") {
                    self.drawDynamics(type.dynamics, direction, measureDrawDetails);
                }
                else if (typeof (type.words) != "undefined") {
                    self.drawDirectionWords(type.words, direction, measureDrawDetails, wordFontPoint);
                }
            }

        }
    };

    //draws any comments for a given measure
    self.drawDirectionWords = function (words, direction, measureDrawDetails, fontSettings) {
        if (!words || !words["@attributes"])
            return;

        var defaultY = (typeof (words["@attributes"]["default-y"]) != "undefined") ? convertTenthsToPixals(parseInt(words["@attributes"]["default-y"], 10)) : 0;
        var fontSize = words["@attributes"]["font-size"];
        var placement = direction["@attributes"].placement;
        var word = words["#text"];

        var defaultX = measureDrawDetails.measureX;


        if (placement == "above") {
            defaultY = measureDrawDetails.measureY - defaultY;
        }
        else {
            defaultY = measureDrawDetails.bottomY + defaultY;
        }

        var originalFont = ctx.font;
        ctx.font = fontSize + "pt " + fontSettings.fontFamily;

        ctx.fillText(word, defaultX, defaultY);

        ctx.font = originalFont;
    };

    self.drawDynamics = function (dynamics, direction, measureDrawDetails) {
        var placement = direction["@attributes"].placement;
        var defaultX = convertTenthsToPixals(parseInt(dynamics["@attributes"]["default-x"], 10));
        var defaultY = convertTenthsToPixals(parseInt(dynamics["@attributes"]["default-y"], 10));
        var halign = dynamics["@attributes"]["halign"];
        var staff = (direction.staff) ? parseInt(direction.staff["#text"]) : 1;

        //if (placement == "above")
        defaultY *= -1;

        defaultX = measureDrawDetails.measureX + defaultX;
        defaultY += measureDrawDetails.staffTopYArray[(staff - 1)];

        var dynamicChar = "";

        for (var key in dynamics) {
            var dynamicValue = key;
            if (typeof (self.fontCharCodes[key]) != "undefined") {
                dynamicChar = self.fontCharCodes[key];
                break;
            }
        }

        if (dynamicChar != "") {
            if (halign == "center")
                defaultX -= (ctx.measureText(dynamicChar).width * 0.5);
            ctx.fillText(dynamicChar, defaultX, defaultY);

        }

    };

    //draws the labels for groups and instruments
    self.displayScoreAndGroupNames = function (partMeasureDrawDetails, partList) {
        var numberGroupsScoreIsIn = 0;
        var previousScorePart = null;
        var nextScoreIsFirstGroupPart = false;
        var activePartGroups = [];
        for (var i = 0; i < partList.length; i++) {
            var part = partList[i];

            if (part.type == "part-group") {
                if (part["@attributes"].type == "start") {
                    numberGroupsScoreIsIn += 1;
                    activePartGroups["part" + part["@attributes"].number] = { partGroup: part, scorePart: null };
                    nextScoreIsFirstGroupPart = true;
                }
                else if (part["@attributes"].type == "stop") {
                    numberGroupsScoreIsIn -= 1;

                    //print group label
                    var startingPartGroup = activePartGroups["part" + part["@attributes"].number].partGroup;
                    var startingScorePart = activePartGroups["part" + part["@attributes"].number].scorePart;
                    var endPart = part;

                    if (startingPartGroup["group-name"] != null) {

                        var defaultX = convertTenthsToPixals(parseInt(startingPartGroup["group-symbol"]["@attributes"]["default-x"], 10));

                        //draw line
                        var firstMeasureDetails = null;
                        var lastMeasureDetails = null;

                        //finde part measure details
                        for (var x = 0; x < partMeasureDrawDetails.length; x++) {
                            if (partMeasureDrawDetails[x].part.part["@attributes"].id == startingScorePart["@attributes"].id)
                                firstMeasureDetails = partMeasureDrawDetails[x];
                            else if (partMeasureDrawDetails[x].part.part["@attributes"].id == previousScorePart["@attributes"].id)
                                lastMeasureDetails = partMeasureDrawDetails[x];
                        }

                        delete activePartGroups["part" + part["@attributes"].number];

                        var label = (currentPage == 1) ? startingPartGroup["group-name"]["#text"] : startingPartGroup["group-abbreviation"]["#text"];
                        var originalTextBaseline = ctx.textBaseline;
                        var originalTextAlign = ctx.textAlign;
                        ctx.textAlign = "right";
                        ctx.textBaseline = "middle";
                        ctx.font = 'normal ' + wordFontPoint.fontSize + 'pt Times New Roman';
                        ctx.fillText(label, (firstMeasureDetails.measureX - convertTenthsToPixals(30)), (firstMeasureDetails.measureY + ((lastMeasureDetails.partMeasureBottomY - firstMeasureDetails.measureY) * 0.5)));
                        ctx.textAlign = originalTextAlign;
                        ctx.textBaseline = originalTextBaseline;
                    }
                }
            }
            else if (part.type == "score-part") {
                //set score part for all active groups that do not have one
                if (nextScoreIsFirstGroupPart) {
                    firstScorePartInGroupPartId = part["@attributes"].number;
                    for (var activePartGroupId in activePartGroups) {
                        if (activePartGroups[activePartGroupId].scorePart == null)
                            activePartGroups[activePartGroupId].scorePart = part;
                    }
                    nextScoreIsFirstGroupPart = false;
                }

                //remember for part group end logic
                previousScorePart = part;

                //print single part label
                var isInGroup = (numberGroupsScoreIsIn > 0);
                var measureDetails = null;
                //finde part measure details
                for (var x = 0; x < partMeasureDrawDetails.length; x++) {
                    if (partMeasureDrawDetails[x].part.part["@attributes"].id == part["@attributes"].id) {
                        measureDetails = partMeasureDrawDetails[x];
                        break;
                    }
                }

                if (measureDetails != null) {
                    var labelText = (isInGroup && part["part-abbreviation"]) ? part["part-abbreviation"]["#text"] : part["part-name"]["#text"];
                    var canPrintLabel = (!isInGroup && (part["part-name"]["@attributes"] != null && part["part-name"]["@attributes"]["print-object"] == "no")) ? false : true;

                    if (canPrintLabel) {
                        var originalTextBaseline = ctx.textBaseline;
                        var originalTextAlign = ctx.textAlign;
                        ctx.textAlign = "right";
                        ctx.textBaseline = "middle";
                        ctx.font = 'normal ' + wordFontPoint.fontSize + 'pt Times New Roman';
                        ctx.fillText(labelText, (measureDetails.measureX - convertTenthsToPixals(30)), (measureDetails.measureY + ((measureDetails.partMeasureBottomY - measureDetails.measureY) * 0.5)));
                        ctx.textAlign = originalTextAlign;
                        ctx.textBaseline = originalTextBaseline;
                    }
                }
            }
        }
    }

    //figures out where to draw all the brackets for the left side of the music
    self.drawBrackets = function (partMeasureDrawDetails, partList) {
        if (partMeasureDrawDetails == null && partMeasureDrawDetails.length <= 0)
            return;
        var firstMeasureDetails = partMeasureDrawDetails[0];
        var lastMeasureDetails = partMeasureDrawDetails[(partMeasureDrawDetails.length - 1)];

        //draw first line to connect all staffs
        ctx.beginPath();
        ctx.moveTo(firstMeasureDetails.measureX, firstMeasureDetails.measureY);
        ctx.lineTo(lastMeasureDetails.measureX, lastMeasureDetails.partMeasureBottomY);
        ctx.lineCap = 'square';
        ctx.stroke();
        ctx.closePath();

        var previousScorePart = null;
        var nextScoreIsFirstGroupPart = false;
        var activePartGroups = [];
        for (var i = 0; i < partList.length; i++) {
            var part = partList[i];
            var partMeasureDrawDetail = null;

            //get draw details
            for (var b = 0; b < partMeasureDrawDetails.length; b++) {
                if (partMeasureDrawDetails[b].part.part["@attributes"].id == part["@attributes"].id) {
                    partMeasureDrawDetail = partMeasureDrawDetails[b];
                    break;
                }
            }

            if (part["@attributes"].type == "start" && part["group-symbol"]) {
                activePartGroups["part" + part["@attributes"].number] = { partGroup: part, scorePart: null };
                nextScoreIsFirstGroupPart = true;
            }
            else if (part["@attributes"].type == "stop") {
                var startPart = activePartGroups["part" + part["@attributes"].number];

                if (startPart) {

                    var startingPartGroup = activePartGroups["part" + part["@attributes"].number].partGroup;
                    var startingScorePart = activePartGroups["part" + part["@attributes"].number].scorePart;
                    var endPart = part;

                    var defaultX = convertTenthsToPixals(parseInt(startingPartGroup["group-symbol"]["@attributes"]["default-x"], 10));

                    //draw line
                    var firstMeasureDetails = null;
                    var lastMeasureDetails = null;

                    //finde part measure details
                    for (var x = 0; x < partMeasureDrawDetails.length; x++) {
                        if (partMeasureDrawDetails[x].part.part["@attributes"].id == startingScorePart["@attributes"].id)
                            firstMeasureDetails = partMeasureDrawDetails[x];
                        else if (partMeasureDrawDetails[x].part.part["@attributes"].id == previousScorePart["@attributes"].id)
                            lastMeasureDetails = partMeasureDrawDetails[x];
                    }

                    self.drawBracket((firstMeasureDetails.measureX + defaultX), firstMeasureDetails.measureY, lastMeasureDetails.partMeasureBottomY, startingPartGroup["group-symbol"]["#text"]);

                    delete activePartGroups["part" + part["@attributes"].number];
                }
            }

            if (partMeasureDrawDetail != null && partMeasureDrawDetail.numberStaves > 1) {
                self.fontCharCodes.brace((partMeasureDrawDetail.measureX - 4), partMeasureDrawDetail.measureY, partMeasureDrawDetail.partMeasureBottomY, ctx, convertTenthsToPixals);
            }

            if (part.type == "score-part") {
                //set score part for all active groups that do not have one
                if (nextScoreIsFirstGroupPart) {
                    firstScorePartInGroupPartId = part["@attributes"].number;
                    for (var activePartGroupId in activePartGroups) {
                        if (activePartGroups[activePartGroupId].scorePart == null)
                            activePartGroups[activePartGroupId].scorePart = part;
                    }
                    nextScoreIsFirstGroupPart = false;
                }

                //remember for part group end logic
                previousScorePart = part;
            }
        }
    };

    //draws on the canvas the left bracket by type
    self.drawBracket = function (barx, barTopY, barBottomY, type) {

        switch (type) {
            case "square":
                var indentX = convertTenthsToPixals(10);
                ctx.beginPath();
                ctx.moveTo(barx + indentX, barTopY);
                ctx.lineTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineTo(barx + indentX, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();
                break;

            case "bracket":
                var thickness = convertTenthsToPixals(4);
                var swigleInsetCurveWidth = convertTenthsToPixals(14);
                var swigleInsetWidth = convertTenthsToPixals(18);
                var leftX = barx - convertTenthsToPixals(2);
                var rightX = barx + convertTenthsToPixals(2)
                ctx.beginPath();
                ctx.moveTo(leftX, (barTopY - 2));
                ctx.lineTo(leftX, (barBottomY + 3));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barBottomY + 2), (barx + swigleInsetWidth), (barBottomY + 5));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barBottomY + 2), leftX, (barBottomY + 2));
                ctx.lineTo(rightX, (barBottomY + 2));
                ctx.lineTo(rightX, (barTopY - 2));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barTopY - 2), (barx + swigleInsetWidth), (barTopY - 5));
                ctx.quadraticCurveTo((barx + swigleInsetCurveWidth), (barTopY - 2), leftX, (barTopY - 3));
                ctx.lineTo(leftX, (barTopY - 2));
                ctx.fillStyle = '#000000';
                ctx.fill();
                ctx.closePath();
                break;

            default:
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineCap = 'square';
                ctx.stroke();
                ctx.closePath();
                break;
        }

    };

    self.drawMeasureLine = function (partMeasureDrawDetails, partList) {
        if (partMeasureDrawDetails == null && partMeasureDrawDetails.length <= 0)
            return;

        var currentGroupNumber = "";
        var previousScorePartId = "";
        var firstScorePartInGroupPartId = null;
        var nextScoreIsFirstGroupPart = false;
        var inGroup = false;
        for (var i = 0; i < partList.length; i++) {
            var part = partList[i];
            //get first group number for barline
            if (part["group-barline"] && part["group-barline"]["#text"] == "yes" && part["@attributes"].type == "start") {
                currentGroupNumber = part["@attributes"].number;
                nextScoreIsFirstGroupPart = true;
                inGroup = true;
            }
                //get the last part number
            else if (part["@attributes"].type == "stop" && part["@attributes"].number == currentGroupNumber) {

                //draw line
                var firstMeasureDetails = null;
                var lastMeasureDetails = null;

                //finde part measure details
                for (var x = 0; x < partMeasureDrawDetails.length; x++) {
                    if (partMeasureDrawDetails[x].part.part["@attributes"].id == firstScorePartInGroupPartId)
                        firstMeasureDetails = partMeasureDrawDetails[x];
                    else if (partMeasureDrawDetails[x].part.part["@attributes"].id == previousScorePartId)
                        lastMeasureDetails = partMeasureDrawDetails[x];
                }

                //draw line to connect all staffs
                self.drawBarline((firstMeasureDetails.measureX + firstMeasureDetails.measureWidth), firstMeasureDetails.measureY, lastMeasureDetails.partMeasureBottomY, firstMeasureDetails.measure);

                inGroup = false;
            }

            if (part.type == "score-part") {
                if (nextScoreIsFirstGroupPart) {
                    firstScorePartInGroupPartId = part["@attributes"].id;
                    nextScoreIsFirstGroupPart = false;
                }

                //draw the barline if the part is not in a group
                if (!inGroup) {
                    //find part by id in measure detail list
                    var currentMeasureDetails = null;
                    for (var x = 0; x < partMeasureDrawDetails.length; x++) {
                        if (partMeasureDrawDetails[x].part.part["@attributes"].id == part["@attributes"].id)
                            currentMeasureDetails = partMeasureDrawDetails[x];
                    }

                    //draw line
                    self.drawBarline((currentMeasureDetails.measureX + currentMeasureDetails.measureWidth), currentMeasureDetails.measureY, currentMeasureDetails.partMeasureBottomY, currentMeasureDetails.measure);

                }

                previousScorePartId = part["@attributes"].id;
            }
        }
    };

    //draws the barline by type
    self.drawBarline = function (barx, barTopY, barBottomY, measure) {
        var type = "normal";

        if (measure != null && measure.barline != null && measure.barline["bar-style"]) {
            type = measure.barline["bar-style"]["#text"];
        }

        switch (type) {
            case "light-heavy":
                ctx.beginPath();
                ctx.moveTo(barx - 5, barTopY);
                ctx.lineTo(barx - 5, barBottomY);
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - 2, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy-light":
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - 5, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy-heavy":
                ctx.beginPath();
                ctx.rect(barx - 2, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(barx - 8, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            case "light-light":
                ctx.beginPath();
                ctx.moveTo(barx - 5, barTopY);
                ctx.lineTo(barx - 5, barBottomY);
                ctx.stroke();
                ctx.closePath();

                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.stroke();
                ctx.closePath();
                break;
            case "heavy":
                ctx.beginPath();
                ctx.rect(barx - 2, barTopY, 2, (barBottomY - barTopY));
                ctx.fillStyle = 'black';
                ctx.fill();
                ctx.stroke();
                ctx.closePath();
                break;
            default:
                ctx.beginPath();
                ctx.moveTo(barx, barTopY);
                ctx.lineTo(barx, barBottomY);
                ctx.lineJoin = 'miter';
                ctx.stroke();
                ctx.closePath();
                break;
        }

    };

    //draws the staff lines and returns the point coordinates for the measure
    self.drawMeasure = function (measure, measureDetails, measureX, measureY, measureWidth, partAttributes, staffDistance, staffHeight, noteHeadHeight) {
        var numberStaves = (partAttributes.staves) ? parseInt(partAttributes.staves["#text"]) : 1;

        var firstStaff = true;
        var topStaffDistance = measureY;
        var bottomY = 0;
        var topYArray = [];
        var bottomYArray = [];
        for (var i = 0; i < numberStaves; i++) {
            if (!firstStaff) {
                topStaffDistance += (staffHeight + staffDistance);
            }

            firstStaff = false;
            bottomY = self.drawStaffLines(measureWidth, measureX, topStaffDistance, noteHeadHeight, staffHeight);
            bottomYArray.push(bottomY);
            topYArray.push(topStaffDistance);
        }

        return { bottomY: bottomY, staffBottomYArray: bottomYArray, staffTopYArray: topYArray, numberStaves: numberStaves, measureX: measureX, measureY: measureY, measure: measure, measureDetails: measureDetails };
    };

    //gets the y coordinate for c 0 
    self.getCYForClef = function (clef, topY, bottomY, noteHeadHeight) {
        var c5Y = 0;
        var c4Y = 0;
        var c0Y = 0;
        var octaveHeight = ((noteHeadHeight * 0.5) * 7);

        switch (clef.sign["#text"]) {
            //treble
            case "G":
                c5Y = (topY + (noteHeadHeight * 2));
                c4Y = ((topY + (noteHeadHeight * 5)) + (noteHeadHeight * 0.5));
                c0Y = (((topY + (noteHeadHeight * 4.5)) + (noteHeadHeight * 0.5)) + (octaveHeight * 4));
                break;
                //bass
            case "F":
                c5Y = (topY - ((noteHeadHeight * 2) - (noteHeadHeight * 5)));
                c4Y = (topY - noteHeadHeight);
                c0Y = ((topY - noteHeadHeight) + (octaveHeight * 4));
                break;
                //Alto
            case "C":

                break;
            default:

        }

        return { c5Y: c5Y, c4Y: c4Y, c0Y: c0Y };
    };

    var slurBeginPoinstArray = [];
    var slurEndPoinstArray = [];

    self.drawMeasureNotes = function (measureDrawDetails, notePoints, part) {
        self.setMusicFont();

        var notes = measureDrawDetails.measure.note;
        var topDistance = measureDrawDetails.measureY;
        var noteHeadHeight = notePoints.noteHeadHeight;
        var noteHeadWidth = notePoints.noteHeadWidth;
        var staffHeight = notePoints.staffHeight;
        var centerStaffX = measureDrawDetails.measureX + (measureDrawDetails.measureDetails.width * 0.5);
        var clefs = part.attributes.clef;
        if (typeof (clefs.length) == "undefined")
            clefs = [clefs];

        //figure middle c for all staves
        var clefCY = [];
        for (var i = 0; i < measureDrawDetails.numberStaves; i++) {

            var bottomY = measureDrawDetails.staffBottomYArray[i];
            var topY = measureDrawDetails.staffTopYArray[i];
            var clef = clefs[i];

            clefCY.push(self.getCYForClef(clef, topY, bottomY, noteHeadHeight));
        }

        //put singe item in array
        if (typeof (notes.length) == "undefined")
            notes = [notes];

        var beamBeginPointsArray = [];
        
        var previousBeamFlagNotePoint = null;

        for (var t = 0; t < notes.length; t++) {
            var note = notes[t];
            var previousNote = notes[(t - 1)];
            var defaultX = (note["@attributes"]) ? convertTenthsToPixals(parseInt(note["@attributes"]["default-x"])) : 0;
            var previousNoteDefaultX = (previousNote && previousNote["@attributes"]) ? convertTenthsToPixals(parseInt(previousNote["@attributes"]["default-x"])) : 0;
            var staff = (note.staff) ? parseInt(note.staff["#text"], 10) : 1;
            var clefId = (staff - 1);
            var clef = clefs[clefId];

            //get c for specific staff
            var c5Y = clefCY[clefId].c5Y;
            var c4Y = clefCY[clefId].c4Y;
            var c0Y = clefCY[clefId].c0Y;
            var staffTopY = measureDrawDetails.staffTopYArray[clefId];
            var staffBottomY = measureDrawDetails.staffBottomYArray[clefId];
            var centerStaffY = staffBottomY - (staffHeight * 0.5);

            var octaveHeight = ((noteHeadHeight * 0.5) * 7);
            var previousNoteShowsFlagBeam = ((defaultX == previousNoteDefaultX) && (defaultX != 0));

            //get pitch char
            var noteData = self.getPitchTypeChar(note, previousNoteShowsFlagBeam);

            //pitch char
            var char = noteData.char;

            //do we need to print flag
            var needsFlag = noteData.needsFlag;

            //Figure Y
            var myY = 0;
            var stepid = self.noteIdsAscending[noteData.pitchStep];

            if (!noteData.isRest) {
                myY = (c0Y - (octaveHeight * noteData.pitchOctave)) - ((noteHeadHeight * 0.5) * (self.noteIdsAscending[noteData.pitchStep] - 1));
            }
            else {
                myY = centerStaffY;
            }

            //save true center y
            var noteCenterY = myY;

            //figure X
            var myX = defaultX + measureDrawDetails.measureX;

            if (noteData.isMeasureRest) {
                myX = centerStaffX;
                myY = myY - (noteHeadHeight * 0.5);
            }

            //PRINT NOTE HEAD************************
            ctx.fillText(char, myX, myY);

            //figure it need added line through note head
            self.drawLineThroughNote(myX, myY, staffTopY, staffBottomY, noteHeadWidth, noteHeadHeight);

            //print dot if needed
            if (noteData.isDot) {
                self.drawDotForNote(myX, myY, noteData, clefs[(staff - 1)], noteHeadWidth, noteHeadHeight);
            }

            if (!previousNoteShowsFlagBeam) {
                previousBeamFlagNotePoint = { x: (defaultX + measureDrawDetails.measureX), y: myY }
            }

            var stemX = 0;
            var defaultStemY = 0;
            //create stems if needed
            if (noteData.needsCustomStem) {
                //get default y
                stemX = (!noteData.isStemDown) ? ((defaultX + measureDrawDetails.measureX) + (noteHeadWidth - 0.5)) : ((defaultX + measureDrawDetails.measureX));
                stemY = 0;
                if (previousNoteShowsFlagBeam) {
                    stemY = (previousBeamFlagNotePoint) ? previousBeamFlagNotePoint.y : 0;
                }
                else {
                    defaultStemY = (note.stem && note.stem["@attributes"]) ? convertTenthsToPixals(parseInt(note.stem["@attributes"]["default-y"], 10)) : 0;
                    stemY = (staffTopY - defaultStemY);
                }

                //nudge stem x to look better with note head
                if (!noteData.isStemDown) {
                    if (noteData.pitchType != "half")
                        stemX -= convertTenthsToPixals(0.5);
                    myY -= convertTenthsToPixals(2.2);
                }
                else {
                    if (noteData.pitchType != "half")
                        stemX += convertTenthsToPixals(0.5);
                    myY += convertTenthsToPixals(2.2);
                }

                ctx.beginPath();
                ctx.moveTo(stemX, myY);
                ctx.lineTo(stemX, stemY);
                ctx.lineCap = 'butt';
                ctx.stroke();
                ctx.closePath();
            }

            if (needsFlag && !previousNoteShowsFlagBeam) {
                var flagChar = noteData.flagChar;

                if (stemX > 0) {
                    var flagY = (noteData.isStemDown) ? (staffTopY - defaultStemY - noteHeadHeight) : (staffTopY - defaultStemY + noteHeadHeight);
                    ctx.fillText(flagChar, stemX, flagY)
                }
            }

            if (noteData.isBeamBeginArray.length > 0) {
                for (var b = 0; b < noteData.isBeamBeginArray.length; b++) {
                    if (noteData.isBeamBeginArray[b])
                        beamBeginPointsArray[b] = { x: stemX, y: (staffTopY - defaultStemY) };
                }
            }

            for (var b = 0; b < noteData.isBeamEndArray.length; b++) {
                if (noteData.isBeamEndArray[b]) {
                    ctx.save();

                    ctx.beginPath();
                    ctx.fillStyle = 'black';
                    var beamHeight = convertTenthsToPixals(6);
                    var beamPadding = convertTenthsToPixals(8);
                    if (noteData.isStemDown) {
                        ctx.moveTo(beamBeginPointsArray[b].x, (beamBeginPointsArray[b].y - (beamPadding * b)));
                        ctx.lineTo(stemX, ((staffTopY - defaultStemY) - (beamPadding * b)));
                        ctx.lineTo(stemX, (((staffTopY - defaultStemY) - beamHeight) - (beamPadding * b)));
                        ctx.lineTo(beamBeginPointsArray[b].x, ((beamBeginPointsArray[b].y - beamHeight)) - (beamPadding * b));

                    }
                    else {
                        ctx.moveTo(beamBeginPointsArray[b].x, (beamBeginPointsArray[b].y + (beamPadding * b)));
                        ctx.lineTo(stemX, ((staffTopY - defaultStemY) + (beamPadding * b)));
                        ctx.lineTo(stemX, (((staffTopY - defaultStemY) + beamHeight) + (beamPadding * b)));
                        ctx.lineTo(beamBeginPointsArray[b].x, ((beamBeginPointsArray[b].y + beamHeight)) + (beamPadding * b));

                    }
                    ctx.fill();
                    ctx.closePath();
                    ctx.restore();
                    noteData.isBeamEndArray[b] = false;
                    //beamBeginPoints[b] = null;
                }
            }

            //SLURs *********************
            if (noteData.isSlurBeginArray.length > 0) {
                for (var b = 0; b < noteData.isSlurBeginArray.length; b++) {
                    if (noteData.isSlurBeginArray[b]) {
                        var newSlurStartPoint = noteData.isSlurBeginArray[b];

                        var slurdefaultx = (newSlurStartPoint["@attributes"]["default-x"]) ? parseInt(newSlurStartPoint["@attributes"]["default-x"]) : 0;
                        var slurdefaulty = (newSlurStartPoint["@attributes"]["default-y"]) ? (staffTopY - convertTenthsToPixals(parseInt(newSlurStartPoint["@attributes"]["default-y"]))) : null;

                        if (!slurdefaulty)
                            slurdefaulty = myY;

                        newSlurStartPoint.x = myX + convertTenthsToPixals(slurdefaultx);
                        newSlurStartPoint.y = slurdefaulty;
                        newSlurStartPoint.xb = myX + convertTenthsToPixals(parseInt(newSlurStartPoint["@attributes"]["bezier-x"]));
                        newSlurStartPoint.yb = staffTopY - convertTenthsToPixals(parseInt(newSlurStartPoint["@attributes"]["bezier-y"]));

                        slurBeginPoinstArray.push(newSlurStartPoint);
                    }
                }
            }

            if (noteData.isSlurEndArray.length > 0) {
                for (var b = 0; b < noteData.isSlurEndArray.length; b++) {
                    if (noteData.isSlurEndArray[b]) {
                        var newSlurEndPoint = noteData.isSlurEndArray[b];

                        var slurdefaultx = (newSlurEndPoint["@attributes"]["default-x"]) ? parseInt(newSlurEndPoint["@attributes"]["default-x"]) : 0;
                        var slurdefaulty = (newSlurEndPoint["@attributes"]["default-y"]) ? (staffTopY - convertTenthsToPixals(parseInt(newSlurEndPoint["@attributes"]["default-y"]))) : null;

                        if (!slurdefaulty)
                            slurdefaulty = myY;

                        newSlurEndPoint.x = myX + convertTenthsToPixals(slurdefaultx);
                        newSlurEndPoint.y = slurdefaulty;
                        newSlurEndPoint.xb = myX + convertTenthsToPixals(parseInt(newSlurEndPoint["@attributes"]["bezier-x"]));
                        newSlurEndPoint.yb = staffTopY - convertTenthsToPixals(parseInt(newSlurEndPoint["@attributes"]["bezier-y"]));

                        slurEndPoinstArray.push(newSlurEndPoint);
                    }
                }
            }

            while ((slurBeginPoinstArray.length > 0 && slurEndPoinstArray.length > 0)) {

                var startSlur = slurBeginPoinstArray[(slurBeginPoinstArray.length - 1)];
                var endSlur = slurEndPoinstArray[(slurEndPoinstArray.length - 1)];

                self.drawSlur(ctx,
                        startSlur.x,
                        startSlur.y,
                        endSlur.x,
                        endSlur.y,
                        startSlur.xb,
                        startSlur.yb,
                        endSlur.xb,
                        endSlur.yb,
                        (startSlur["@attributes"]["placement"] != "above"),
                        2.5 * 10
                        );
                slurBeginPoinstArray.pop();
                slurEndPoinstArray.pop();
            }


            beamPreviousNoteX = (defaultX + measureDrawDetails.measureX);

            if (noteData.accidental) {
                var char = null;
                var offset = 0;

                switch (noteData.accidental) {
                    case "flat":
                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.flat : self.fontCharCodes.flatWithParentheses;
                        offset = (!noteData.accidentalInParentheses) ? convertTenthsToPixals(10) : convertTenthsToPixals(20);
                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));
                        break;
                    case "sharp":
                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.sharp : self.fontCharCodes.sharpWithParentheses;
                        offset = (!noteData.accidentalInParentheses) ? convertTenthsToPixals(10) : convertTenthsToPixals(21);
                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));
                        break;
                    case "natural":
                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.natural : self.fontCharCodes.nateralWithParentheses;
                        offset = (!noteData.accidentalInParentheses) ? convertTenthsToPixals(10) : convertTenthsToPixals(20);
                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));
                        break;
                    case "double-sharp":
                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.doubleSharp : self.fontCharCodes.doubleSharpWithParentheses;
                        offset = convertTenthsToPixals(12);
                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));
                        break;
                    case "flat-flat":
                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.doubleFlat : self.fontCharCodes.doubleFlatWithParentheses;
                        offset = (!noteData.accidentalInParentheses) ? convertTenthsToPixals(18) : convertTenthsToPixals(24);
                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));
                        break;
                    case "triple-sharp":

                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.doubleSharp : self.fontCharCodes.doubleSharpWithParentheses;
                        offset = (!noteData.accidentalInParentheses) ? convertTenthsToPixals(12) : convertTenthsToPixals(20);

                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));

                        var charWidth = ctx.measureText(char).width;

                        ctx.fillText((!noteData.accidentalInParentheses) ? self.fontCharCodes.sharp : self.fontCharCodes.sharpWithParentheses, ((defaultX + measureDrawDetails.measureX) - (offset + charWidth + convertTenthsToPixals(2))), (noteCenterY));
                        break;
                    case "triple-flat":

                        char = (!noteData.accidentalInParentheses) ? self.fontCharCodes.doubleFlat : self.fontCharCodes.doubleFlatWithParentheses;
                        offset = (!noteData.accidentalInParentheses) ? convertTenthsToPixals(18) : convertTenthsToPixals(24);

                        ctx.fillText(char, ((defaultX + measureDrawDetails.measureX) - offset), (noteCenterY));

                        var charWidth = ctx.measureText(char).width;

                        ctx.fillText((!noteData.accidentalInParentheses) ? self.fontCharCodes.flat : self.fontCharCodes.flatWithParentheses, ((defaultX + measureDrawDetails.measureX) - (offset + charWidth + convertTenthsToPixals(-5))), (noteCenterY));
                        break;
                }
            }

        }
    };

    self.drawLineThroughNote = function (noteX, noteY, staffTopY, staffBottomY, noteHeadWidth, noteHeadHeight) {

        var numberLines = 0;
        var goingUp = true;
        var lineY = staffTopY;

        if (noteY < staffTopY) {
            var topHeight = (staffTopY - noteY);
            var numberLines = Math.floor(((staffTopY - noteY) / noteHeadHeight) + 0.1);
        }
        else if (noteY > staffBottomY) {
            var numberLines = Math.floor(((noteY - staffBottomY) / noteHeadHeight) + 0.1);
            goingUp = false;
            lineY = staffBottomY;
        }


        for (var i = 0; i < numberLines; i++) {
            if (goingUp)
                lineY -= noteHeadHeight;
            else
                lineY += noteHeadHeight;
            ctx.beginPath();
            ctx.moveTo((noteX - convertTenthsToPixals(3)), lineY);
            ctx.lineTo((noteX + noteHeadWidth + convertTenthsToPixals(3)), lineY);
            ctx.lineCap = 'butt';
            ctx.stroke();
            ctx.closePath();
        }
    };

    //figures out where to put the dot for notes on staff line
    self.drawDotForNote = function (noteX, noteY, noteData, clef, noteHeadWidth, noteHeadHeight) {

        var dotX = noteX + (noteHeadWidth + convertTenthsToPixals(6));
        var dotY = noteY;
        switch (clef.sign["#text"]) {
            //treble cleff
            case "G":
                if ((noteData.pitchOctave == 5 && noteData.pitchStep == "F") ||
                    (noteData.pitchOctave == 5 && noteData.pitchStep == "D") ||
                    (noteData.pitchOctave == 4 && noteData.pitchStep == "B") ||
                    (noteData.pitchOctave == 4 && noteData.pitchStep == "G") ||
                    (noteData.pitchOctave == 4 && noteData.pitchStep == "E") ||
                    noteData.isRest
                    ) {
                    dotY -= (noteHeadHeight * 0.5);
                }
                break;
                //bass cleff
            case "F":
                if ((noteData.pitchOctave == 2 && noteData.pitchStep == "B") ||
                    (noteData.pitchOctave == 2 && noteData.pitchStep == "G") ||
                    (noteData.pitchOctave == 3 && noteData.pitchStep == "F") ||
                    (noteData.pitchOctave == 3 && noteData.pitchStep == "D") ||
                    (noteData.pitchOctave == 3 && noteData.pitchStep == "A") ||
                    noteData.isRest
                    ) {
                    dotY -= (noteHeadHeight * 0.5);
                }
                break;
        }
        ctx.fillText(self.fontCharCodes.dot, dotX, dotY);
    };

    //used for adjusting the y axis
    self.noteIdsAscending = { C: 1, D: 2, E: 3, F: 4, G: 5, A: 6, B: 7 };
    self.noteIdsDescending = { C: 7, D: 6, E: 5, F: 4, G: 3, A: 2, B: 1 };

    //get the char for pitch type
    self.getPitchTypeChar = function (note, previousNoteShowsFlagBeam) {
        var pitchStep = (note.pitch) ? note.pitch.step["#text"] : "C";
        var pitchOctave = (note.pitch) ? parseInt(note.pitch.octave["#text"]) : 5;
        var pitchType = (note.type) ? note.type["#text"] : "";
        var isRest = (note.rest) ? true : false;
        var isMeasureRest = (note.rest && note.rest["@attributes"] && note.rest["@attributes"].measure == "yes") ? true : false;
        var isStemDown = (note.stem && note.stem["#text"] == "down");
        var accidental = (note.accidental && note.accidental["#text"]) ? note.accidental["#text"] : null;
        var accidentalInParentheses = (note.accidental && note.accidental["#text"] && note.accidental["@attributes"] && note.accidental["@attributes"].parentheses == "yes");
        var isDot = (note.dot);
        var needsCustomStem = ((note.stem && note.stem["@attributes"]) || previousNoteShowsFlagBeam);

        //get beam data
        var isBeamBeginArray = [];
        var isBeamEndArray = [];
        if (note.beam) {
            //see if only one beam node
            if (typeof (note.beam.length) == "undefined") {
                isBeamBeginArray[0] = (note.beam["#text"] == "begin") ? true : false;
                isBeamEndArray[0] = (note.beam["#text"] == "end") ? true : false;
            }
            else {
                for (var b = 0; b < note.beam.length; b++) {
                    isBeamBeginArray[b] = (note.beam[b]["#text"] == "begin") ? true : false;
                    isBeamEndArray[b] = (note.beam[b]["#text"] == "end") ? true : false;
                }
            }
        }

        //get slur data
        var isSlurBeginArray = [];
        var isSlurEndArray = [];
        if (note.notations) {
            if (note.notations.slur && typeof(note.notations.slur.length) == "undefined") {
                if (note.notations.slur["@attributes"]["type"] == "start")
                    isSlurBeginArray[0] = note.notations.slur;
                else
                    isSlurEndArray[0] = note.notations.slur;
            }
        }

        //default char to notehead
        var char = self.fontCharCodes.noteHead;
        var needsFlag = false;
        var flagChar = null;

        switch (pitchType) {
            case "quarter":
                if (isRest)
                    char = self.fontCharCodes.quarterNoteRest;
                else {
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.quarterNoteStemDown : self.fontCharCodes.quarterNoteStemUp;
                }

                break;
            case "eighth":
                if (isRest)
                    char = self.fontCharCodes.eighthNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.eighthNoteStemDown : self.fontCharCodes.eighthNoteStemUp;

                if (!note.beam) {
                    flagChar = (isStemDown) ? self.fontCharCodes.eighthNoteFlagDown : self.fontCharCodes.eighthNoteFlagUp;
                    needsFlag = true;
                }

                break;
            case "16th":
                if (isRest)
                    char = self.fontCharCodes.sixteenthNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.sixteenthNoteStemDown : self.fontCharCodes.sixteenthNoteStemUp;
                if (!note.beam) {
                    needsFlag = true;
                    flagChar = (isStemDown) ? self.fontCharCodes.sixteenthNoteFlagDown : self.fontCharCodes.sixteenthNoteFlagUp;
                }

                break;

            case "32nd":
                if (isRest)
                    char = self.fontCharCodes.thirtySecondNoteRest;
                else
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.sixteenthNoteStemDown : self.fontCharCodes.sixteenthNoteStemUp;
                if (!note.beam) {
                    needsFlag = true;
                    flagChar = (isStemDown) ? self.fontCharCodes.sixteenthNoteFlagDown : self.fontCharCodes.sixteenthNoteFlagUp;
                }

                break;
            case "half":
                if (isRest)
                    char = self.fontCharCodes.halfNoteRest;

                else {
                    char = self.fontCharCodes.noteHeadOpen;
                    if (!needsCustomStem)
                        char = (isStemDown) ? self.fontCharCodes.halfNoteStemDown : self.fontCharCodes.halfNoteStemUp;
                }

                break;
            case "whole":
                if (isRest)
                    char = self.fontCharCodes.wholeNoteRest;
                else
                    char = self.fontCharCodes.wholeNote;

                offByHalf = true;

                break;
            default:
                char = self.fontCharCodes.wholeNoteRest;
                break;
        }

        return {
            char: char,
            needsFlag: needsFlag,
            flagChar: flagChar,
            isDot: isDot,
            pitchStep: pitchStep,
            pitchOctave: pitchOctave,
            pitchType: pitchType,
            isRest: isRest,
            isStemDown: isStemDown,
            accidentalInParentheses: accidentalInParentheses,
            accidental: accidental,
            isMeasureRest: isMeasureRest,
            needsCustomStem: needsCustomStem,
            isBeamBeginArray: isBeamBeginArray,
            isBeamEndArray: isBeamEndArray,
            isSlurBeginArray: isSlurBeginArray,
            isSlurEndArray: isSlurEndArray
        };
    };

    //draws the clef and key signature
    self.drawMeasureAttributes = function (measureDrawDetails, part, noteHeadHeight, firstMeasure) {

        for (var i = 0; i < measureDrawDetails.numberStaves; i++) {

            //draw clef************************************************
            var clef = (typeof (part.attributes.clef) != "undefined" && typeof (part.attributes.clef[i]) != "undefined") ? part.attributes.clef[i] : part.attributes.clef;
            var topY = measureDrawDetails.staffTopYArray[i];
            var bottomY = measureDrawDetails.staffBottomYArray[i];
            var centerStaffY = bottomY - ((bottomY - topY) * 0.5);
            var clefX = measureDrawDetails.measureX + convertTenthsToPixals(12);
            var signWidth = 0;

            switch (clef.sign["#text"]) {
                case "G":
                    ctx.fillText(self.fontCharCodes.trebleClef, clefX, (topY + (noteHeadHeight * 3)));
                    signWidth = ctx.measureText(self.fontCharCodes.trebleClef).width;
                    break;
                case "F":
                    ctx.fillText(self.fontCharCodes.bassClef, clefX, (topY + noteHeadHeight));
                    signWidth = ctx.measureText(self.fontCharCodes.bassClef).width;
                    break;
                case "C":
                    ctx.fillText(self.fontCharCodes.altoClef, clefX, centerStaffY);
                    signWidth = ctx.measureText(self.fontCharCodes.altoClef).width;
                    break;
            }

            //draw key *************************************************
            var keyFifths = parseInt(part.attributes.key.fifths["#text"]);
            var keyFinalX = clefX + signWidth;
            //sharp key
            if (keyFifths > 0) {
                var numberOfSharps = keyFifths;
                var keyX = keyFinalX;
                var keyWidth = ctx.measureText(self.fontCharCodes.sharp).width;
                var topSharpY = topY;

                if (clef.sign["#text"] == "F")
                    topSharpY += noteHeadHeight;
                else if (clef.sign["#text"] == "C")
                    topSharpY += (noteHeadHeight * 0.5);

                for (var j = 0; j < numberOfSharps; j++) {
                    var sharpY = topSharpY;

                    switch (j) {
                        case 1:
                            sharpY += (noteHeadHeight * 1.5);
                            break;
                        case 2:
                            sharpY -= (noteHeadHeight * 0.5);
                            break;
                        case 3:
                            sharpY += noteHeadHeight;
                            break;
                        case 4:
                            sharpY += (noteHeadHeight * 2.5);
                            break;
                        case 5:
                            sharpY += (noteHeadHeight * 0.5);
                            break;
                        case 6:
                            sharpY += (noteHeadHeight * 2);
                            break;
                    }

                    keyFinalX = (keyX + (keyWidth * j) + convertTenthsToPixals(12));

                    ctx.fillText(self.fontCharCodes.sharp, keyFinalX, sharpY);
                }

                keyFinalX += keyWidth;
            }
                //flat key
            else if (keyFifths < 0) {
                var numberOfFlats = keyFifths * -1;
                var keyX = keyFinalX;
                var keyWidth = ctx.measureText(self.fontCharCodes.flat).width;
                var topFlatY = centerStaffY;

                if (clef.sign["#text"] == "F")
                    topFlatY += noteHeadHeight;
                else if (clef.sign["#text"] == "C")
                    topFlatY += (noteHeadHeight * 0.5);

                for (var j = 0; j < numberOfFlats; j++) {
                    var flatY = topFlatY;

                    if (j % 2 != 0) {
                        flatY -= (noteHeadHeight * 1.5);
                        flatY += ((noteHeadHeight * 0.5) * (j * 0.5));
                    }
                    else {
                        flatY += ((noteHeadHeight * 0.5) * (j * 0.5));
                    }

                    keyFinalX = (keyX + (keyWidth * j) + convertTenthsToPixals(12));

                    ctx.fillText(self.fontCharCodes.flat, keyFinalX, flatY);
                }

                keyFinalX += keyWidth;
            }

            //draw time signature
            if (firstMeasure) {
                var beats = part.attributes.time.beats["#text"];
                var beatType = part.attributes.time["beat-type"]["#text"];
                var beatX = keyFinalX + convertTenthsToPixals(10);

                ctx.fillText(beats, beatX, (measureDrawDetails.staffTopYArray[i] + (noteHeadHeight * 1)));
                ctx.fillText(beatType, beatX, (measureDrawDetails.staffTopYArray[i] + (noteHeadHeight * 3)));
            }
        }
    };

    //draw staff lines for measure.
    self.drawStaffLines = function (measureWidth, leftMargin, topStaffDistance, noteHeadHeight, staffHeight) {

        for (var s = 0; s < 5; s++) {
            ctx.beginPath();
            ctx.moveTo(leftMargin, topStaffDistance + (noteHeadHeight * s));
            ctx.lineTo(leftMargin + measureWidth, topStaffDistance + (noteHeadHeight * s));
            ctx.stroke();
            ctx.closePath();
        }

        return topStaffDistance + staffHeight;
    };

    self.drawSlur = function (context, x0, y0, x1, y1, x0b, y0b, x1b, y1b, downward, bow) {

        if (!isNaN(x0b))
        {
            var sign = (downward ? 1 : -1);
            var thickness = sign * 2;

            context.save();
            context.beginPath();
            context.moveTo(x0, y0);
            
            if (!downward)
            {
                context.bezierCurveTo(x0b, y0b, x1b, y1b, x1, y1);

                y0b += thickness;
                y1b += thickness

                context.bezierCurveTo(x1b, y1b, x0b, y0b, x0, y0);
            }
            else
            {
                context.bezierCurveTo(x0b, y0b + ((y0 - y0b) * 2), x1b, y1b + ((y1 - y1b) * 2), x1, y1);

                y0b += thickness;
                y1b += thickness

                context.bezierCurveTo(x1b, y1b + ((y1 - y1b) * 2), x0b, y0b + ((y0 - y0b) * 2), x0, y0);
            }
           
            context.lineTo(x0, y0);
            context.fill();
            context.closePath();
        }
        else
        {
            var sign = (downward ? 1 : -1);

            var thickness = sign * 2;

            var delta_x = x1 - x0;
            var delta_y = y1 - y0;

            var ratio = 0.25;

            var max_bow = delta_x * 0.4;

            if (bow > max_bow && delta_y < 2 * 10) {

                bow = max_bow;
            }

            bow *= sign;

            var slope = delta_y / delta_x;

            var cp1x = x0 + delta_x * ratio;
            var cp1y = y0 + (cp1x - x0) * slope + bow;

            var cp2x = x1 - delta_x * ratio;
            var cp2y = y0 + (cp2x - x0) * slope + bow;
            context.save();
            context.beginPath();
            context.moveTo(x0, y0);
            context.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x1, y1);

            cp1y += thickness;
            cp2y += thickness;
            context.bezierCurveTo(cp2x, cp2y, cp1x, cp1y, x0, y0);
            context.lineTo(x0, y0);
            context.fill();
            context.closePath();
        }
       
        
        //context.bezierCurveTo(cp2x, cp2y, cp1x, cp1y, x0, y0);
    };

    //gets any system layout info for all measures for a current page parts
    self.getMeasurePrintLayouts = function (parts) {
        var measureLayouts = [];
        for (var x = 0; x < parts.length; x++) {

            var part = parts[x];
            var partMeasureDetails = [];

            for (var i = 0; i < part.measures.length; i++) {
                var measure = part.measures[i];
                var measureDetails = {};
                measureDetails.width = convertTenthsToPixals(parseInt(measure["@attributes"].width));
                var systemDistance = 0
                if (measure.print && measure.print["system-layout"]) {
                    measureDetails.leftMargin = (measure.print["system-layout"]["system-margins"] && measure.print["system-layout"]["system-margins"]["left-margin"]) ? convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-margins"]["left-margin"]["#text"])) : 0;
                    measureDetails.rightMargin = (measure.print["system-layout"]["system-margins"] && measure.print["system-layout"]["system-margins"]["right-margin"]) ? convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-margins"]["right-margin"]["#text"])) : 0;
                    measureDetails.topDistance = (measure.print["system-layout"]["top-system-distance"]) ? convertTenthsToPixals(parseInt(measure.print["system-layout"]["top-system-distance"]["#text"])) : 0;


                }

                if (measure.print && measure.print["staff-layout"]) {
                    measureDetails.staffDistance = (measure.print["staff-layout"] && measure.print["staff-layout"]["staff-distance"]) ? convertTenthsToPixals(parseInt(measure.print["staff-layout"]["staff-distance"]["#text"])) : 0;
                }

                if (measure.print && measure.print["system-layout"]) {
                    measureDetails.systemDistance = (measure.print["system-layout"]["system-distance"]) ? convertTenthsToPixals(parseInt(measure.print["system-layout"]["system-distance"]["#text"])) : 0;
                }

                partMeasureDetails.push(measureDetails);
            }

            measureLayouts.push(partMeasureDetails);
        }

        return measureLayouts;
    }

    //sets the music font for the canvas
    self.setMusicFont = function () {
        //musicFontPoint.fontSize = 21;
        var noteFont = "normal " + musicFontPoint.fontSize + "pt MaestroNotes";

        //set note font for sizing
        ctx.font = noteFont;
    };

    //sets up all necessary measurements
    self.setupMeasurements = function (piece, musicFontPoint) {
        //musicFontPoint.fontSize = 21;
        var noteFont = "normal " + musicFontPoint.fontSize + "pt MaestroNotes";

        //set note font for sizing
        ctx.font = noteFont;

        var noteStaffPoint = {};

        noteStaffPoint.noteHeadWidth = ctx.measureText(self.fontCharCodes.noteHead).width;
        noteStaffPoint.noteHeadHeight = canvasUtils.getNoteHeadHeight(self.fontCharCodes.noteHead, noteFont);
        noteStaffPoint.noteHeadHeight = noteStaffPoint.noteHeadWidth * 0.82;//noteStaffPoint.noteHeadWidth - 1;
        noteStaffPoint.staffHeight = (noteStaffPoint.noteHeadHeight * 4);

        noteStaffPoint.tenthsPixal = noteStaffPoint.noteHeadHeight / 10;

        piece.noteHeadWidth = noteStaffPoint.noteHeadWidth;
        piece.noteHeadHeight = noteStaffPoint.noteHeadHeight;
        piece.staffHeight = noteStaffPoint.staffHeight;
        piece.tenthsPixal = noteStaffPoint.tenthsPixal;

        return noteStaffPoint;
    };

    //get the page layout data
    self.parsePageLayout = function (defaults) {
        if (!defaults)
            return;

        if (defaults["page-layout"] && defaults["page-layout"]["page-margins"] instanceof Array) {
            defaults["page-layout"]["page-margins"] = defaults["page-layout"]["page-margins"][0];
        }

        var pageDetails = {};

        pageDetails.pageWidth = (defaults["page-layout"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-width"]["#text"])) : 0;
        pageDetails.pageHeight = (defaults["page-layout"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-height"]["#text"])) : 0;
        pageDetails.pageLeftMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"] && defaults["page-layout"]["page-margins"]["left-margin"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["left-margin"]["#text"])) : 0;
        pageDetails.pageRightMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"] && defaults["page-layout"]["page-margins"]["right-margin"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["right-margin"]["#text"])) : 0;
        pageDetails.pageTopMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["top-margin"]["#text"])) : 0;
        pageDetails.pageBottomMargin = (defaults["page-layout"] && defaults["page-layout"]["page-margins"]) ? convertTenthsToPixals(parseInt(defaults["page-layout"]["page-margins"]["bottom-margin"]["#text"])) : 0;
        pageDetails.pageTopSystemDistance = (defaults["system-layout"] && defaults["system-layout"]["top-system-distance"]) ? convertTenthsToPixals(parseInt(defaults["system-layout"]["top-system-distance"]["#text"])) : 0;
        pageDetails.pageSystemDistance = (defaults["system-layout"] && defaults["system-layout"]["system-distance"]) ? convertTenthsToPixals(parseInt(defaults["system-layout"]["system-distance"]["#text"])) : 0;
        pageDetails.pageStaffDistance = (defaults["staff-layout"] && defaults["staff-layout"]["staff-distance"]) ? convertTenthsToPixals(parseInt(defaults["staff-layout"]["staff-distance"]["#text"])) : 0;

        return pageDetails;
    };

    
   

    //remove all drawing from canvas
    self.clearCanvas = function () {
        // Store the current transformation matrix
        ctx.save();

        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Restore the transform
        ctx.restore();
    }

    //resize the canvas to the musicxml page specs
    self.adjustCavasWidthHeight = function (piece) {
        var retina = window.devicePixelRatio > 1;
        var pageWidth = (piece.pageWidth > 0) ? piece.pageWidth : canvas.width;
        var pageHeight = (piece.pageHeight > 0) ? piece.pageHeight : canvas.height;
        var dim = 1;

        if (retina)
            dim = 2;

        var totalSquareFootage = (pageWidth * dim) * (pageHeight * dim);

        if (dim > 1 && totalSquareFootage >= (2200 * 2200)) {
            dim = 1;
        }

        canvas.width = piece.pageWidth * dim;
        canvas.height = piece.pageHeight * dim;

        if (dim > 1) {
            //set the style width and hight only for retina support
            //causing aliasing if changed for browsers
            canvas.style.width = piece.pageWidth + "px";
            canvas.style.height = piece.pageHeight + "px";
            ctx.scale(2, 2);
        }

    };

}