﻿function AnnotationsEngine(canvas)
{
    var self = this;
    var canvas = canvas;
    var ctx = canvas.getContext('2d');

    self.lineWidth = 2;
    self.opacity = 1.0;
    var eraserWidth = 50;

    var flag = false;
    var prevX = 0;
    var currX = 0;
    var prevY = 0;
    var currY = 0;
    var dot_flag = false;

    var textInput = null;
    var textX = 0;
    var textY = 0;

    self.enableDrawing = false;

    self.color = "red"
    self.mode = "pen" //pen, eraser, text

    canvas.addEventListener("mousemove", function (e) {
        self.findxy('move', e.pageX, e.pageY)
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        self.findxy('down', e.pageX, e.pageY)
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        self.findxy('up', e.pageX, e.pageY)
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        self.findxy('out', e.pageX, e.pageY)
    }, false);

    canvas.addEventListener("touchstart", function (e) {
        self.findxy('down', e.targetTouches[0].pageX, e.targetTouches[0].pageY)
    }, false);
    canvas.addEventListener("touchmove", function (e) {
        self.findxy('move', e.targetTouches[0].pageX, e.targetTouches[0].pageY); 
        if (self.enableDrawing)
            e.preventDefault();
    }, false);
    canvas.addEventListener("touchend", function (e) {
        self.findxy('up', e.targetTouches[0].pageX, e.targetTouches[0].pageY)
    }, false);



    self.findxy = function(res, x, y) {

        if (!self.enableDrawing)
            return;

        x = x - canvas.offsetLeft - canvas.offsetParent.offsetLeft;
        y = y - canvas.offsetTop - canvas.offsetParent.offsetTop;

        

        if (self.mode == "text")
        {
            if (res == 'up')
            {
                if (textInput == null) {
                    self.showTextInput(x, y);
                }
                else {
                    self.drawTextInput();
                }
            }
           
            return;
        }

        var retina = window.devicePixelRatio > 1;
        if (retina) {
            x *= 2;
            y *= 2;
        }

        if (res == 'down') {
            prevX = currX;
            prevY = currY;
            currX = x;
            currY = y;

            flag = true;
            dot_flag = true;
            if (dot_flag) {
                ctx.beginPath();
                ctx.globalCompositeOperation = (self.mode == "pen") ? "source-over" : "destination-out";
                ctx.fillStyle = self.color;
                ctx.fillRect(currX, currY, 2, 2);
                ctx.closePath();
                dot_flag = false;
            }
        }
        if (res == 'up' || res == "out") {
            flag = false;
        }
        if (res == 'move') {
            if (flag) {
                prevX = currX;
                prevY = currY;
                currX = x;
                currY = y;
                self.draw();
            }
        }
    }

    self.draw = function() {
        var width = (self.mode == "pen") ? self.lineWidth : eraserWidth
        ctx.beginPath();
        ctx.globalCompositeOperation = (self.mode == "pen") ? "source-over" : "destination-out";

        if(width < 5)
        {
            ctx.moveTo(prevX, prevY);
            ctx.lineTo(currX, currY);
            ctx.strokeStyle = self.color;
            ctx.stroke();
        }
        else
        {
            ctx.arc(currX, currY, (width), 0, Math.PI * 2, false);
        }
        
       
        ctx.fill();
        ctx.globalAlpha = self.opacity;
        ctx.lineWidth = (self.mode == "pen") ? self.lineWidth : eraserWidth;
        
        ctx.closePath();
    }

    self.getImage = function ()
    {
        return canvas.toDataURL("image/png");
    }

    self.drawImage = function (image)
    {
        var imageObj = new Image();
        imageObj.onload = function () {
            ctx.drawImage(this, 0, 0);
        };

        imageObj.src = image;
    }

    self.showTextInput = function (x, y) {
        var newInput = document.createElement("INPUT");
        newInput.type = "text";
        newInput.style.position = "absolute";
        newInput.style.top = (y + canvas.offsetParent.offsetTop) + "px";
        newInput.style.left = (x + canvas.offsetParent.offsetLeft) + "px";
        newInput.style.fontFamily = "comic sans MS";
        newInput.style.color = self.color;

        newInput.onkeypress = function (event) {
            if (event.keyCode == 13)
                self.drawTextInput();
        };

        document.body.appendChild(newInput);

        newInput.focus();

        textX = x;
        textY = y;

        textInput = newInput
    };

    self.drawTextInput = function ()
    {
        var text = textInput.value;
        document.body.removeChild(textInput);
        textInput = null;
        var font = 12;

        var retina = window.devicePixelRatio > 1;
        if (retina) {
            textX *= 2;
            textY *= 2;
            font *= 2;
        }

        ctx.globalCompositeOperation = "source-over";
        ctx.textBaseline = "top";
        ctx.font = font + 'pt comic sans MS';
        ctx.fillStyle = self.color;
        ctx.fillText(text, textX, textY);
    }

    self.adjustCavasWidthHeight = function (pageDetails) {
        var retina = window.devicePixelRatio > 1;
        var pageWidth = (pageDetails.pageWidth > 0) ? pageDetails.pageWidth : canvas.width;
        var pageHeight = (pageDetails.pageHeight > 0) ? pageDetails.pageHeight : canvas.height;
        var dim = 1;

        if (retina)
            dim = 2;

        var totalSquareFootage = (pageWidth * dim) * (pageHeight * dim);

        if (dim > 1 && totalSquareFootage >= (2200 * 2200)) {
            dim = 1;
        }

        canvas.width = pageDetails.pageWidth * dim;
        canvas.height = pageDetails.pageHeight * dim;

        if (dim > 1) {
            //set the style width and hight only for retina support
            //causing aliasing if changed for browsers
            canvas.style.width = pageDetails.pageWidth + "px";
            canvas.style.height = pageDetails.pageHeight + "px";
            ctx.scale(2, 2);
        }

    };
}