var express = require("express");

var app = express();
app.use(express.logger());

// Configuration

app.configure(function() {
    app.set('views', __dirname + '/public');
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.static(__dirname + '/public'));
    app.use(app.router);
    app.engine('html', require('ejs').renderFile);
});

app.get('/', function(request, response) {
    response.render('index.html');
});
app.get('/responsive', function(request, response) {
    response.render('responsive.html');
});
var port = process.env.PORT || 8080;
app.listen(port, function() {
    console.log("Listening on " + port);
});