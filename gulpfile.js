var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('gulp-bower');
var inject = require('gulp-inject');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat-util');
var es = require('stream-series');
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var rename = require("gulp-rename");
var change = require('gulp-change');
var less = require('gulp-less');
var path = require('path');
var jshint = require('gulp-jshint');
var karma = require('karma');
var karmaParseConfig = require('karma/lib/config').parseConfig;

var buildNumber = '5.0.' + new Date().getTime();

var vendorJs = [
    './node_modules/jquery/dist/jquery.js',
    './node_modules/jquery-ui-dist/jquery-ui.js',
    './node_modules/bootstrap/dist/js/bootstrap.js',
    './node_modules/knockout/build/output/knockout-latest.js'
];

var vendorCss = [
    './node_modules/jquery-ui/jquery-ui.css',
];

gulp.task('local-build', ['less', 'views'], function() {

    //copy required files
    var vendorJsStream = gulp.src(vendorJs)
        .pipe(gulp.dest('./public/vendor'));

    var vendorCssStream = gulp.src(vendorCss)
        .pipe(gulp.dest('./public/vendor'));

    var configStream = gulp.src('configs/dev.js')
        .pipe(rename('config.js'))
        .pipe(change(performChange))
        .pipe(gulp.dest('./public'));

    var bootstrapFontStream = gulp.src('./bower_components/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('./public/fonts'));

    var fontAwesomeFontStream = gulp.src('./node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('./public/fonts'));

    var target = gulp.src(['./public/index.html', './public/responsive.html']);
    var sources = gulp.src(['./public/scripts/**/*.js', './public/styles/**/*.css'], { read: false });

    return target.pipe(inject(es(vendorJsStream, vendorCssStream, sources, configStream, bootstrapFontStream, fontAwesomeFontStream), { relative: true }))
        .pipe(gulp.dest('./public'));
});

gulp.task('dev-build', ['less', 'views'], function() {

    // var configStream = gulp.src('configs/dev.js')
    //     .pipe(rename('config_' + buildNumber + '.js'))
    //     .pipe(change(performChange))
    //     .pipe(gulp.dest('./public'));

    var vendorJsStream = gulp.src(vendorJs)
        .pipe(concat('vendor_' + buildNumber + '.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/bin'));

    var vendorCssStream = gulp.src(vendorCss)
        .pipe(concat('vendor_' + buildNumber + '.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/bin'));

    var appJsStream = gulp.src(['./public/scripts/**/*.js'])
        .pipe(concat('scripts_' + buildNumber + '.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/bin'));

    var appCssStream = gulp.src('./public/styles/**/*.css')
        .pipe(concat('site_' + buildNumber + '.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/bin'));

    var bootstrapFontStream = gulp.src('./bower_components/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('./public/fonts'));

    var fontAwesomeFontStream = gulp.src('./node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('./public/fonts'));

    return gulp.src('./public/index.html')
        .pipe(inject(es(vendorJsStream, vendorCssStream, appJsStream, appCssStream, configStream, bootstrapFontStream, fontAwesomeFontStream), { relative: true }))
        .pipe(gulp.dest('./public'));

});

gulp.task('prod-build', ['less', 'views'], function() {

    var configStream = gulp.src('configs/prod.js')
        .pipe(rename('config_' + buildNumber + '.js'))
        .pipe(change(performChange))
        .pipe(gulp.dest('./public'));

    var vendorJsStream = gulp.src(vendorJs)
        .pipe(concat('vendor_' + buildNumber + '.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/bin'));

    var vendorCssStream = gulp.src(vendorCss)
        .pipe(concat('vendor_' + buildNumber + '.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/bin'));

    var appJsStream = gulp.src(['./public/platform/**/*.js'])
        .pipe(concat('scripts_' + buildNumber + '.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/bin'));

    var appCssStream = gulp.src('./public/styles/**/*.css')
        .pipe(concat('site_' + buildNumber + '.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/bin'));

    var bootstrapFontStream = gulp.src('./bower_components/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('./public/fonts'));

    var fontAwesomeFontStream = gulp.src('./node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('./public/fonts'));

    return gulp.src('./public/index.html')
        .pipe(inject(es(vendorJsStream, vendorCssStream, appJsStream, appCssStream, configStream, bootstrapFontStream, fontAwesomeFontStream), { relative: true }))
        .pipe(gulp.dest('./public'));

});

gulp.task('views', function() {
    return gulp.src(['views/index.html', 'views/responsive.html'])
        .pipe(gulp.dest('./public'));
});

gulp.task('bower', ['cleanAll'], function() {
    return bower({ cmd: 'install' });
});

gulp.task('clean', function() {
    return gulp.src(['./public/bin', './public/vendor', './public/styles'], { read: false })
        .pipe(clean());
});

gulp.task('cleanAll', function() {
    return gulp.src(['./public/bin', './public/vendor', './public/styles', './bower_components', './test-reports'], { read: false })
        .pipe(clean());
});

gulp.task('less', function() {
    return gulp.src('./styles/**/*.less')
        .pipe(less({
            paths: [path.join(__dirname, 'includes')]
        }))
        .pipe(gulp.dest('./public/styles'));
});

gulp.task('lint', function() {
    return gulp.src('./public/platform/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});



function runKarma(configFilePath, options, cb) {

    configFilePath = path.resolve(configFilePath);

    var server = karma.server;
    var log = gutil.log,
        colors = gutil.colors;
    var config = karmaParseConfig(configFilePath, {});

    Object.keys(options).forEach(function(key) {
        config[key] = options[key];
    });

    server.start(config, function(exitCode) {
        log('Karma has exited with ' + colors.red(exitCode));
        cb();
        process.exit(exitCode);
    });
}

/** actual tasks */

/** single run */
gulp.task('test', function(cb) {
    runKarma('karma.conf.js', {
        autoWatch: false,
        singleRun: true
    }, cb);
});

function performChange(content) {
    return content.replace(/{{buildnumber}}/g, buildNumber);
}