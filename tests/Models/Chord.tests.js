describe('Chord', function() {

    it('should set notes to an array', function(done) {
        var model = new NotationJS.Chord();
        expect(model.notes).toEqual(jasmine.arrayContaining([]));
        done();
    });

    it('should add a note while setting itself as the notes chord', function(done) {
        var model = new NotationJS.Chord();
        var note = { chord: null };
        model.addNote(note);
        expect(model.notes).toEqual(jasmine.arrayContaining([note]));
        expect(note.chord).toEqual(model);
        done();
    });

});