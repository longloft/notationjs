describe('Barline', function() {

    it('should set number to 1', function(done) {
        var model = new NotationJS.Beam();
        expect(model.number).toBe(1);
        done();
    });

    it('should set type to "regular"', function(done) {
        var model = new NotationJS.Beam();
        expect(model.type).toBe("regular");
        done();
    });

    it('should set isHook to false', function(done) {
        var model = new NotationJS.Beam();
        expect(model.isHook).toBe(false);
        done();
    });
});