describe('Articulation', function() {

    it('should set x to 0', function(done) {
        var model = new NotationJS.Articulation();
        expect(model.x).toBe(0);
        done();
    });

    it('should set y to 0', function(done) {
        var model = new NotationJS.Articulation();
        expect(model.x).toBe(0);
        done();
    });

    it('should set defaultX to 0', function(done) {
        var model = new NotationJS.Articulation();
        expect(model.defaultX).toBe(0);
        done();
    });

    it('should set defaultY to 0', function(done) {
        var model = new NotationJS.Articulation();
        expect(model.defaultY).toBe(0);
        done();
    });

});