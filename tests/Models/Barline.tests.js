describe('Barline', function() {

    it('should set location to blank string', function(done) {
        var model = new NotationJS.Barline();
        expect(model.location).toBe("");
        done();
    });

    it('should set style to blank string', function(done) {
        var model = new NotationJS.Barline();
        expect(model.style).toBe("");
        done();
    });

});